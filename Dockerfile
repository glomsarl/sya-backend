#download the node image
FROM node:16.14.0-alpine3.14

# #set the working directory
WORKDIR /app

# # install app dependencies
COPY package*.json ./

# #clean npm cache
# RUN npm cache clean --force && npm install

# #clean install dependecies
RUN npm install

# # add app
COPY . ./

#add uploads file
RUN mkdir ./uploads

# # expose port 3011 to outer environment
EXPOSE 4000

# # start app
CMD ["node", "index"]
