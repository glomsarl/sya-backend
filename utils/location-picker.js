exports.getPossibleLocation = (latitude, longitude, radius) => {
  let locations = []
  for (let angle = 0; angle < 360; angle++) {
    for (let r = 0; r < radius; r++) {
      const lat = r * Math.sin(angle) + latitude
      const long = r * Math.cos(angle) + longitude
      locations.push({ lat, long })
    }
  }
  return locations
}
