exports.generateRedisString = (object) => {
  return Object.keys(object).reduce((accumulator, key) => {
    accumulator.push(key)
    accumulator.push(object[key] ?? '')
    return accumulator
  }, [])
}

exports.generateCronJobString = (date_string) => {
  const date = new Date(date_string)
  return `${date.getSeconds()} ${date.getMinutes()} ${
    date.getHours
  } ${date.getDate()} ${date.getMonth()} ${date.getFullYear()}`
}
