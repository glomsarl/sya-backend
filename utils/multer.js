const multer = require('multer')
const uuidV4 = require('uuid').v4()
const path = require('path')
// Multer config
module.exports = multer({
  // storage: multer.diskStorage({}),
  dest: '/uploads',
  storage: multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
      let nameFile = file.originalname.split(' ')
      nameFile = nameFile.join('_')
      nameFile = nameFile.split('.')
      nameFile.pop()
      nameFile = nameFile.join('_')
      cb(
        null,
        `${file.fieldname}_${uuidV4}_${nameFile}.${file.mimetype.split('/')[1]}`
      )
    },
  }),
  fileFilter: (req, file, cb) => {
    let ext = path.extname(file.originalname).toLowerCase()
    if (
      ext !== '.jpg' &&
      ext !== '.jpeg' &&
      ext !== '.png' &&
      ext !== '.jfif' &&
      ext !== '.pdf' &&
      ext !== '.webp'
    ) {
      req.error = new Error('File type is not supported')
      cb(req.error.message, false)
      return
    }
    cb(null, true)
  },
})
