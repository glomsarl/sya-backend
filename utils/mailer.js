const fs = require('fs')
const path = require('path')
const Handlebars = require('handlebars')
const nodemailer = require('nodemailer')

let transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: process.env.EMAIL,
    pass: process.env.EMAIL_PASSWORD,
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false,
  },
})

/**
 * @param (array_of_email. subject, template_path, message)
 * @array_of_email : an array of email you want to send the message to. In case there's only one email give
 * an @aray_of_email of one element.
 * @subject (string) : the subject of email message
 * @template_path (string): path to the template file youo want to use for mailing
 * @message (object) : it's an object of keys-vakues use to replace template preconfigured values
 * Please open template file to kmow what keys need to be provided for a particular email template for emxaple
 * for a @template_path with index.hbs we need to provide a link_message wich is not realy for order @template_path
 */

exports.mailer = (array_of_email, subject, template_path, message) => {
  const source = fs.readFileSync(path.join(__dirname, template_path), 'utf8')
  const template = Handlebars.compile(source)

  let mailOptions = {
    from: process.env.EMAIL,
    to: array_of_email.join(', '),
    subject: subject,
    html: template({ ...message }),
  }
  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) reject(error)
      else resolve(info)
    })
  })
}
