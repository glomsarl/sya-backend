const shell = require('shelljs')
const { Client } = require('elasticsearch')

// //starting elasticsearch server
// shell.exec(`start ${process.env.ELASTICSEARCH_DB_PATH}`)
// shell.exec('curl http://127.0.0.1:9200')

const elasticsearchClient = new Client({
  host: 'http://127.0.0.1:9200',
})

exports.createDataIndex = (doc_name, data) => {
  try {
    if (!data || !doc_name)
      throw new Error('doc_name and document data is required')
    elasticsearchClient.index({
      index: doc_name,
      body: data,
    })
    return { is_data_indexed: true }
  } catch (error) {
    console.log({
      message: error.message || 'Sorry, index not created',
    })
  }
}

exports.searchDataIndex = async (doc_name, query) => {
  try {
    const exist = await elasticsearchClient.indices.exists({ index: doc_name })
    if (exist) {
      const response = await elasticsearchClient.search({
        index: doc_name,
        body: {
          query,
        },
      })
      if (response) {
        const elasticsearch_data = response.hits.hits.map(({ _source }) => {
          return _source
        })
        return elasticsearch_data
      }
    }
    return []
  } catch (error) {
    console.error({
      message: error.message || 'Sorry, an error occured',
    })
  }
}

exports.searchAllIndex = async (doc_name) => {
  try {
    const exist = await elasticsearchClient.indices.exists({ index: doc_name })
    if (exist) {
      const response = await elasticsearchClient.search({
        index: doc_name,
        body: {
          query: {
            match_all: {},
          },
        },
      })
      if (response) {
        const elasticsearch_data = response.hits.hits.map(({ _source }) => {
          return _source
        })
        return elasticsearch_data
      }
    }
    return []
  } catch (error) {
    console.error({
      message: error.message || 'Sorry, an error occured',
    })
  }
}

exports.deleteDocument = async (doc_name) => {
  try {
    const response = await elasticsearchClient.search({
      index: doc_name,
      body: {
        query: {
          match_all: {},
        },
      },
    })
    response.hits.hits.forEach(({ _id: id }) => {
      elasticsearchClient.delete({ id, index: doc_name })
    })
    return { is_document_deleted: true }
  } catch (error) {
    console.error({
      message: error.message || 'Sorry, an error occured',
    })
  }
}
