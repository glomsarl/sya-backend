'use strict'
const mung = require('express-mung')

/* add new_token to json response body. */
exports.responseInterceptor = mung.json((body, req, res) => {
  try {
    const { new_token } = res
    if (new_token) {
      return { ...body, new_token, is_modified: 'OK' }
    }
    if(req.error) throw new Error(req.error.message) 
    return body
  } catch (error) {
    return res.status(500).send({
      message: error.message,
    })
  }
})
