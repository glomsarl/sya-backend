const jwt = require('jsonwebtoken')
const client = require('../utils/redis')

const generateToken = (session_id) =>
  jwt.sign(
    {
      session_id,
    },
    process.env.JWT_SECRET,
    { expiresIn: process.env.TOKEN_DURATION },
  )
  
  const verifyToken = (token, secret) => {
  let access_token = { verified_token: {}, error: null }
  jwt.verify(token, secret, (error, verified_token) => {
    access_token = { error, verified_token }
  })
  try {
    if (access_token.error)
      throw new Error(
        access_token.error.message || 'Token has expired, Please sign in again',
      )
    else {
      const {
        verified_token: { session_id, iat, exp },
      } = access_token
      const now = new Date() / 1000
      if (now - iat > 0.75 * (exp - iat)) {
        const new_token = generateToken(session_id)
        return { session_id, new_token }
      } else return { session_id }
    }
  } catch (error) {
    // const { session_id } = jwt.decode(token)
    // if (error.message === 'jwt expired') client.del(session_id)
    return { error }
  }
}

module.exports = {
  generateToken,
  verifyToken,
}
