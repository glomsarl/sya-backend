'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StructureAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      StructureAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "audited_by",
        onDelete: "CASCADE",
      });

      StructureAudit.belongsTo(models.Structure, {
        foreignKey: "structure_id",
        onDelete: "CASCADE",
      });
    }
  };
  StructureAudit.init(
    {
      structure_audit_id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      logo_ref: {
        type: DataTypes.STRING,
      },
      description: {
        type: DataTypes.STRING,
      },
      longitude: {
        type: DataTypes.REAL,
      },
      latitude: {
        type: DataTypes.REAL,
      },
      specific_indications: {
        type: DataTypes.STRING,
      },
      is_deleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      structure_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      audited_by: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      audited_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: "StructureAudit",
      freezeTableName: true,
      createdAt: false,
      updatedAt: false,
    }
  );
  StructureAudit.removeAttribute('updatedAt')
  return StructureAudit;
};