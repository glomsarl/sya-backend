'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogDetail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      CatalogDetail.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "created_by",
        onDelete: "CASCADE"
      })
      CatalogDetail.belongsTo(models.Service, {
        foreignKey: "service_id",
        onDelete: "CASCADE"
      })
      CatalogDetail.belongsTo(models.Catalog, {
        foreignKey: "catalog_id",
        onDelete: "CASCADE"
      })
      CatalogDetail.hasMany(models.CatalogDetailAudit, {
        foreignKey: "catalog_detail_id",
      })
    }
  };
  CatalogDetail.init({
    catalog_detail_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    is_reservable: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    is_take_away: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    is_home_delivery: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    is_payable: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    amount: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    quantity: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    catalog_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    service_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    cancellation_delay: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'CatalogDetail',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return CatalogDetail;
};