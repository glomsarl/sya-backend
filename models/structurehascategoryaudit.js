'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StructureHasCategoryAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      StructureHasCategoryAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "audited_by",
        onDelete: "CASCADE",
      });
      StructureHasCategoryAudit.belongsTo(models.StructureHasCategory, {
        foreignKey: "structure_has_category_id",
        onDelete: "CASCADE",
      });

    }
  };
  StructureHasCategoryAudit.init(
    {
      structure_has_category_audit_id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      is_category_active: {
        type: DataTypes.BOOLEAN,
      },
      structure_has_category_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      audited_by: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: "StructureHasCategoryAudit",
      freezeTableName: true,
      updatedAt: false,
      createdAt: false,
    }
  );
  return StructureHasCategoryAudit;
};