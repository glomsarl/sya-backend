'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SubscriptionPayment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SubscriptionPayment.belongsTo(models.StructureHasSubscription, {
        foreignKey: "structure_has_subscription_id",
        onDelete: "CASCADE"
      })
    }
  };
  SubscriptionPayment.init({
    subscription_payment_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    structure_has_subscription_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    commission: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    commission_rate: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    paid_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    is_paid: {
      type: DataTypes.UUID,
      allowNull: false
    },
    reference: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    payment_method: {
      type: DataTypes.ENUM('CASH', 'MoMo', 'OM'),
      allowNull: false,
    },
    total_period_revenue: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
  }, {
    sequelize,
    modelName: 'SubscriptionPayment',
    freezeTableName: true,
    timestamps: false
  });
  return SubscriptionPayment;
};