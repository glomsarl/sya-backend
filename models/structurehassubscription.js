'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StructureHasSubscription extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      StructureHasSubscription.belongsTo(models.Structure, {
        foreignKey: "structure_id",
        onDelete: "CASCADE"
      })
      StructureHasSubscription.belongsTo(models.Subscription, {
        foreignKey: "subscription_id",
        onDelete: "CASCADE"
      })

      StructureHasSubscription.hasMany(models.SubscriptionPayment, {
        foreignKey: "structure_has_subscription_id",
      })
    }
  };
  StructureHasSubscription.init({
    structure_has_subscription_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    structure_id: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: "Structure",
        key: "structure_id",
        as: "structure_id"
      }
    },
    subscription_id: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: "Subscription",
        key: "subscription_id",
        as: "subscription_id"
      }
    },
    started_at: {
      type: DataTypes.DATE,
    },
    expired_at: {
      type: DataTypes.DATE,
    },
    created_at: {
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE,
      allowNull: false,
    },
}, {
    sequelize,
    modelName: 'StructureHasSubscription',
    freezeTableName: true,
    timestamps: false,
  });
  return StructureHasSubscription;
};