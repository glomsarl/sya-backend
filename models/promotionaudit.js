'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PromotionAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PromotionAudit.belongsTo(models.Promotion, {
        foreignKey: "promotion_id",
        onDelete: "CASDCADE",
      })
      PromotionAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "audited_by",
        onDelete: "CASCADE",
      })
    }
  };
  PromotionAudit.init({
    promotion_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    promotion_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    audited_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    audited_by: {
      allowNull: false,
      type: DataTypes.UUID,
    },
  }, {
    sequelize,
    modelName: 'PromotionAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
  });
  return PromotionAudit;
};