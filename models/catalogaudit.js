'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      CatalogAudit.belongsTo(models.Catalog, {
        foreignKey: "catalog_id",
        onDelete: "CASCADE"
      })
      CatalogAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "audited_by",
        onDelete: "CASCADE"
      })
    }
  };
  CatalogAudit.init({
    catalog_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    ends_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    starts_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    audited_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    },
    audited_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'CatalogAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return CatalogAudit;
};