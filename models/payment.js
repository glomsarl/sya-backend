'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Payment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Payment.belongsTo(models.Order, {
        foreignKey: "order_id",
        onDelete: "CASCADE"
      })
      Payment.belongsTo(models.PromotionCode, {
        foreignKey: "promotion_code_id",
        onDelete: "CASCADE"
      })
      Payment.belongsTo(models.PersonHasRole, {
        foreignKey: "paid_by",
        onDelete: "CASCADE"
      })
    }
  };
  Payment.init({
    payment_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    order_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    is_paid: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    payment_method: {
      type: DataTypes.ENUM('CASH', 'SY'),
      allowNull: false,
    },
    payment_service: {
      type: DataTypes.ENUM('OM', 'MoMo', 'SYA'),
      allowNull: false,
    },
    initial_due: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    amount_paid: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    promotion_code_id: {
      type: DataTypes.UUID,
    },
    paid_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    },
    paid_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'Payment',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return Payment;
};