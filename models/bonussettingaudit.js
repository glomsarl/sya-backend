'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BonusSettingAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BonusSettingAudit.belongsTo(models.PersonHasRole, {
        foreignKey: "audited_by",
        onDelete: "CASCADE"
      })
      BonusSettingAudit.belongsTo(models.BonusSetting, {
        foreignKey: "bonus_setting_id",
        onDelete: "CASCADE"
      })
    }
  };
  BonusSettingAudit.init({
    bonus_setting_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    sy_value: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    audited_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    audited_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    // bonus_bank: {
    //   type: DataTypes.INTEGER,
    //   defaultValue: 0,
    // },
    sign_up_bonus: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    structure_validation_bonus: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    is_sign_up_bonus_on: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false,
    },
    bonus_setting_id: {
      type: DataTypes.UUID,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'BonusSettingAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return BonusSettingAudit;
};