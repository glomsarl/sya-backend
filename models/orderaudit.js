'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrderAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      OrderAudit.belongsTo(models.PersonHasRole, {
        foreignKey: "audited_by",
        onDelete: "CASCADE"
      })
      OrderAudit.belongsTo(models.Order, {
        foreignKey: "order_id",
        onDelete: "CASCADE"
      })
    }
  };
  OrderAudit.init({
    order_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    audited_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    audited_by: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: "Person",
        key: "person_id",
        as: "audited_by"
      }
    },
    order_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    order_type: {
      type: DataTypes.ENUM('R', 'T', 'H'),
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'OrderAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return OrderAudit;
};