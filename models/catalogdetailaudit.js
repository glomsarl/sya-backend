'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CatalogDetailAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      CatalogDetailAudit.belongsTo(models.CatalogDetail, {
        foreignKey: "catalog_detail_id",
        onDelete: "CASCADE"
      })
      CatalogDetailAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "audited_by",
        onDelete: "CASCADE"
      })
    }
  };
  CatalogDetailAudit.init({
    catalog_detail_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    catalog_detail_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    is_reservable: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    is_take_away: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    is_home_delivery: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    is_payable: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    amount: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    quantity: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    cancellation_delay: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    audited_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    },
    audited_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'CatalogDetailAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return CatalogDetailAudit;
};