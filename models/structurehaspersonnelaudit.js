'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StructureHasPersonnelAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      StructureHasPersonnelAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "audited_by",
        onDelete: "CASCADE",
      });
      StructureHasPersonnelAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "structure_has_personnel_id",
        onDelete: "CASCADE",
      });
    }
  };
  StructureHasPersonnelAudit.init(
    {
      structure_has_personnel_audit_id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      is_active: {
        type: DataTypes.BOOLEAN,
      },
      structure_has_personnel_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      audited_by: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      audited_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: "StructureHasPersonnelAudit",
      freezeTableName: true,
      updatedAt: false,
      createdAt: false,
    }
  );
  return StructureHasPersonnelAudit;
};