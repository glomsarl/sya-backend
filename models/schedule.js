'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Schedule extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Schedule.belongsTo(models.Structure, {
        foreignKey: "structure_id",
        onDelete: "CASCADE"
      })
      Schedule.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "created_by",
        onDelete: "CASCADE"
      })
      Schedule.hasMany(models.ScheduleAudit, {
        foreignKey: "schedule_id",
      })
    }
  };
  Schedule.init({
    schedule_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    day: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    open_at: {
      type: DataTypes.TIME,
      allowNull: false,
    },
    close_at: {
      type: DataTypes.TIME,
      allowNull: false,
    },
    date: {
      type: DataTypes.DATE
    },
    structure_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    created_by: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    is_deleted: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    }
  }, {
    sequelize,
    modelName: 'Schedule',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
  });
  return Schedule;
};