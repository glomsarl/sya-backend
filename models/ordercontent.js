'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class OrderContent extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      OrderContent.belongsTo(models.Service, {
        foreignKey: 'service_id',
        onDelete: 'CASCADE',
      })
      OrderContent.belongsTo(models.PersonHasRole, {
        foreignKey: 'ordered_by',
        onDelete: 'CASCADE',
      })
      OrderContent.belongsTo(models.Order, {
        foreignKey: 'order_id',
        onDelete: 'CASCADE',
      })

      OrderContent.hasMany(models.OrderContent, {
        foreignKey: 'order_id',
      })
    }
  }
  OrderContent.init(
    {
      order_content_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      service_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      order_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      unit_price: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      ordered_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
      ordered_by: {
        allowNull: false,
        type: DataTypes.UUID,
      },
    },
    {
      sequelize,
      modelName: 'OrderContent',
      freezeTableName: true,
      createdAt: false,
      updatedAt: false,
    },
  )
  return OrderContent
}
