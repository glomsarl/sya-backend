'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class ServiceImage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ServiceImage.belongsTo(models.Service, {
        foreignKey: 'service_id',
        onDelete: 'CASCADE',
      })
      ServiceImage.belongsTo(models.StructureHasPersonnel, {
        foreignKey: 'added_by',
        onDelete: 'CASCADE',
      })
    }
  }

  ServiceImage.init(
    {
      service_image_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      service_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      service_image_ref: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      is_deleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      added_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
      added_by: {
        allowNull: false,
        type: DataTypes.UUID,
      },
    },
    {
      sequelize,
      modelName: 'ServiceImage',
      freezeTableName: true,
      timestamps: false,
    },
  )
  return ServiceImage
}
