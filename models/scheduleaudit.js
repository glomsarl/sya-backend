'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ScheduleAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ScheduleAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "audited_by",
        onDelete: "CASCADE",
      })
      ScheduleAudit.belongsTo(models.Schedule, {
        foreignKey: "schedule_id",
        onDelete: "CASCADE",
      })
    }
  };
  ScheduleAudit.init({
    schedule_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    audited_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    audited_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    open_at: {
      type: DataTypes.TIME
    },
    close_at: {
      type: DataTypes.TIME
    },
    date: {
      type: DataTypes.DATE
    },
    is_deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
    }
  }, {
    sequelize,
    modelName: 'ScheduleAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
    createdAt: false,
  });
  return ScheduleAudit;
};