'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PersonHasRole extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PersonHasRole.belongsTo(models.Person, {
        foreignKey: "person_id",
        onDelete: "CASCADE"
      })
      PersonHasRole.belongsTo(models.Role, {
        foreignKey: "role_id",
        onDelete: "CASCADE"
      })

      PersonHasRole.hasMany(models.Demand, {
        foreignKey: 'person_has_role_id'
      })
      PersonHasRole.hasMany(models.PromotionCode, {
        foreignKey: 'person_has_role_id'
      })
      PersonHasRole.hasMany(models.OrderContent, {
        foreignKey: 'ordered_by'
      })
      PersonHasRole.hasMany(models.FeedbackAudit, {
        foreignKey: 'audited_by'
      })
      PersonHasRole.hasMany(models.OrderAudit, {
        foreignKey: 'audited_by'
      })
      PersonHasRole.hasMany(models.OrderBonus, {
        foreignKey: 'audited_by'
      })
      PersonHasRole.hasMany(models.Feedback, {
        foreignKey: 'created_by'
      })
      PersonHasRole.hasMany(models.Order, {
        foreignKey: 'ordered_by'
      })
      PersonHasRole.hasMany(models.OrderContent, {
        foreignKey: 'ordered_by'
      })
      PersonHasRole.hasMany(models.Payment, {
        foreignKey: 'paid_by'
      })

      
      PersonHasRole.hasMany(models.Promotion, {
        foreignKey: "created_by",
      });
      PersonHasRole.hasMany(models.BonusSubscription, {
        foreignKey: "created_by",
      });
      PersonHasRole.hasMany(models.PersonHasRoleAudit, {
        foreignKey: "person_has_role_id",
      })
      PersonHasRole.hasMany(models.Demand, {
        foreignKey: "validated_by",
      })
      PersonHasRole.hasMany(models.BonusSetting, {
        foreignKey: "created_by",
      })
      PersonHasRole.hasMany(models.Structure, {
        foreignKey: "created_by",
      })
      PersonHasRole.hasMany(models.BonusSettingAudit, {
        foreignKey: "audited_by",
      })
      PersonHasRole.hasMany(models.Mail, {
        foreignKey: "sent_by",
      })
      PersonHasRole.hasMany(models.Log, {
        foreignKey: "person_has_role_id",
      })
      PersonHasRole.hasMany(models.PersonBonus, {
        foreignKey: 'person_id'
      })
      PersonHasRole.hasMany(models.StructureHasPersonnel, {
        foreignKey: "person_has_role_id",
      })
      PersonHasRole.hasMany(models.ResetPassword, {
        foreignKey: "person_has_role_id",
      })
      PersonHasRole.hasMany(models.StructureCategory, {
        foreignKey: "created_by",
      })
      PersonHasRole.hasMany(models.Subscription, {
        foreignKey: "created_by",
      })
      PersonHasRole.hasMany(models.SubscriptionAudit, {
        foreignKey: "audited_by",
      })
      PersonHasRole.hasMany(models.BonusSubscriptionAudit, {
        foreignKey: "audited_by",
      })
    }
  };
  PersonHasRole.init({
    person_has_role_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    person_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    role_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    is_deleted: {
      allowNull: false, 
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    }
  }, {
    sequelize,
    modelName: 'PersonHasRole',
    freezeTableName: true,
    updatedAt: false,
    createdAt: false,
  });
  return PersonHasRole;
};