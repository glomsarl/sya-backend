'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PromotionHasServiceAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PromotionHasServiceAudit.belongsTo(models.PromotionHasService, {
        foreignKey: "promotion_has_service_id",
        onDelete: "CASCADE",
      })
      PromotionHasServiceAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "audited_by",
        onDelete: "CASCADE",
      })
    }
  };
  PromotionHasServiceAudit.init({
    promotion_has_services_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    promotion_has_service_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    consequent_price: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    audited_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    audited_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {
    sequelize,
    modelName: 'PromotionHasServiceAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return PromotionHasServiceAudit;
};