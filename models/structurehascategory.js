'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StructureHasCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      StructureHasCategory.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "added_by",
        onDelete: "CASCADE",
      });
      StructureHasCategory.belongsTo(models.Structure, {
        foreignKey: "structure_id",
        onDelete: "CASCADE",
      });
      StructureHasCategory.belongsTo(models.StructureCategory, {
        foreignKey: "structure_category_id",
        onDelete: "CASCADE",
      });

    }
  };
  StructureHasCategory.init(
    {
      structure_has_category_id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      is_category_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      structure_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      structure_category_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      added_by: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
      is_deleted: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      }
    },
    {
      sequelize,
      modelName: "StructureHasCategory",
      freezeTableName: true,
      updatedAt: false,
      createdAt: false,
    }
  );
  return StructureHasCategory;
};