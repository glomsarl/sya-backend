'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class FeedbackAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      FeedbackAudit.belongsTo(models.Feedback, {
        foreignKey: "feedback_id",
        onDelete: "CASCADE",
      })
      FeedbackAudit.belongsTo(models.PersonHasRole, {
        foreignKey: "audited_by",
        onDelete: "CASCADE",
      })
    }
  };
  FeedbackAudit.init({
    feedback_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    feedback_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    audited_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    },
    audited_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {
    sequelize,
    modelName: 'FeedbackAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return FeedbackAudit;
};