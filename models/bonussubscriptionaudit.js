'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BonusSubscriptionAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BonusSubscriptionAudit.belongsTo(models.BonusSubscription, {
        foreignKey: "bonus_subscription_id",
        onDelete: "CASCADE",
      })
      BonusSubscriptionAudit.belongsTo(models.PersonHasRole, {
        foreignKey: "audited_by",
        onDelete: "CASCADE",
      })
    }
  };
  BonusSubscriptionAudit.init({
    bonus_subscription_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    quantity_cashed_out: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    quantity_cashed_in: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    quantity_remaining: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    bonus_subscription_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    audited_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    audited_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'BonusSubscriptionAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return BonusSubscriptionAudit;
};