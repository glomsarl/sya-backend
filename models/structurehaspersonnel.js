'use strict';
const {
  Model, Sequelize
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StructureHasPersonnel extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      StructureHasPersonnel.belongsTo(models.PersonHasRole, {
        foreignKey: "personnel_role_id",
        onDelete: "CASCADE"
      });
      StructureHasPersonnel.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "created_by",
        onDelete: "CASCADE",
      });
      StructureHasPersonnel.belongsTo(models.Structure, {
        foreignKey: "structure_id",
        onDelete: "CASCADE",
      });
      StructureHasPersonnel.hasMany(models.PromotionHasService, {
        foreignKey: "created_by",
      });
      StructureHasPersonnel.hasMany(models.ServiceAudit, {
        foreignKey: "audited_by",
        onDelete: "CASCADE",
      });
      StructureHasPersonnel.hasMany(models.Catalog, {
        foreignKey: "created_by",
      });
      StructureHasPersonnel.hasMany(models.CatalogAudit, {
        foreignKey: "audited_by",
      });
      StructureHasPersonnel.hasMany(models.PromotionHasServiceAudit, {
        foreignKey: "audited_by",
      });
      StructureHasPersonnel.hasMany(models.PromotionAudit, {
        foreignKey: "audited_by",
        onDelete: "CASCADE",
      });
      StructureHasPersonnel.hasMany(models.Service, {
        foreignKey: "created_by",
      });
      StructureHasPersonnel.hasMany(models.CatalogDetail, {
        foreignKey: "created_by",
      });
      StructureHasPersonnel.hasMany(models.CatalogDetailAudit, {
        foreignKey: "audited_by",
      });
    }
  };
  StructureHasPersonnel.init(
    {
      structure_has_personnel_id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      person_has_role_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      created_by: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      structure_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: "StructureHasPersonnel",
      freezeTableName: true,
      updatedAt: false,
      createdAt: false,
    }
  );
  return StructureHasPersonnel;
};