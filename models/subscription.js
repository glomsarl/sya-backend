'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Subscription extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Subscription.belongsTo(models.PersonHasRole, {
        foreignKey: "created_by",
        onDelete: "CASCADE",
      })
      Subscription.hasMany(models.StructureHasSubscription, {
        foreignKey: "subscription_id",
        onDelete: "CASCADE",
      })
      Subscription.hasMany(models.SubscriptionAudit, {
        foreignKey: "subscription_id",
        onDelete: "CASCADE",
      })
    }
  };
  Subscription.init({
    subscription_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    subscription_type: {
      type: DataTypes.ENUM('MONTHLY', 'TERMLY', 'YEARLY'),
      defaultValue: "MONTHLY",
      allowNull: false,
    },
    commission_rate: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    created_by: {
      allowNull: false,
      type: DataTypes.UUID,
    }
  }, {
    sequelize,
    modelName: 'Subscription',
    freezeTableName: true,
    timestamps: false,
  });
  return Subscription;
};