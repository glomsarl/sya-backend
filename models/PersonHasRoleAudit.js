'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PersonHasRoleAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PersonHasRoleAudit.belongsTo(models.PersonHasRole, {
        foreignKey: "person_has_role_id",
        onDelete: "CASCADE"
      })
    }
  };
  PersonHasRoleAudit.init({
    person_has_role_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    role_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    person_has_role_id: {
      allowNull: false,
      type: DataTypes.UUID
    },
    audited_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {
    sequelize,
    modelName: 'PersonHasRoleAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return PersonHasRoleAudit;
};