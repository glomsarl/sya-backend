'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StructureCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      StructureCategory.belongsTo(models.PersonHasRole, {
        foreignKey: 'created_by',
        onDelete: 'CASCADE'
      })
      
      StructureCategory.hasMany(models.PersonBonus, {
        foreignKey: 'structure_category_id'
      })
      StructureCategory.hasMany(models.StructureHasCategory, {
        foreignKey: 'structure_category_id'
      })
    }
  };
  StructureCategory.init(
    {
      structure_category_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        unique: true,
      },
      category_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      is_deleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
      },
      created_by: {
        type: DataTypes.UUID,
        allowNull: false
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: "StructureCategory",
      freezeTableName: true,
      updatedAt: false,
      createdAt: false,
    }
  );
  return StructureCategory;
};