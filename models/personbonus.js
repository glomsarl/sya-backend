'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PersonBonus extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PersonBonus.belongsTo(models.PersonHasRole, {
        foreignKey: 'person__role_id',
        onDelete: "CASCADE"
      })
      PersonBonus.belongsTo(models.StructureCategory, {
        foreignKey: 'structure_category_id',
        onDelete: "CASCADE"
      })
    }
  };
  PersonBonus.init({
    person_bonus_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    value: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    person_has_role_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    structure_category_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  }, {
    sequelize,
    modelName: 'PersonBonus',
    timestamps: false,
    freezeTableName: true,
  });
  return PersonBonus;
};