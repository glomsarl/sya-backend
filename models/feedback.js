'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Feedback extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Feedback.hasMany(models.FeedbackAudit, {
        foreignKey: 'audited_by',
      })
      Feedback.belongsTo(models.PersonHasRole, {
        foreignKey: 'created_by',
        onDelete: 'CASCADE',
      })
      Feedback.belongsTo(models.OrderContent, {
        foreignKey: 'order_content_id',
        onDelete: 'CASCADE',
      })
    }
  }
  Feedback.init(
    {
      feedback_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      order_content_id: {
        type: DataTypes.UUID,
      },
      feedback: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      rating: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: false,
      },
      created_by: {
        type: DataTypes.UUID,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'Feedback',
      freezeTableName: true,
      createdAt: false,
      updatedAt: false,
    },
  )
  return Feedback
}
