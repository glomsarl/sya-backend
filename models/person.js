'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Person extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Person.hasMany(models.PersonAudit, {
        foreignKey: 'person_id'
      })
      Person.hasOne(models.PersonHasRole, {
        foreignKey: 'person_id'
      })
      Person.belongsTo(models.Person, {
        foreignKey: 'referral_id'
      })
      Person.hasMany(models.Person, {
        foreignKey: 'referral_id'
      })
    }
  };
  Person.init(
    {
      person_id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      first_name: {
        type: DataTypes.STRING,
      },
      last_name: {
        type: DataTypes.STRING,
      },
      national_id_number: {
        type: DataTypes.STRING,
        unique: true,
      },
      date_of_birth: {
        type: DataTypes.DATEONLY,
      },
      gender: {
        type: DataTypes.ENUM("M", "F"),
      },
      phone: {
        type: DataTypes.INTEGER,
        unique: true,
      },
      email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
      bonus_acquired: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      referral_id: {
        type: DataTypes.UUID,
      },
      person_image_ref: {
        type: DataTypes.STRING,
        unique: true,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      }
    },
    {
      sequelize,
      modelName: "Person",
      freezeTableName: true,
      updatedAt: false,
      createdAt: false,
    }
  );
  return Person;
};
