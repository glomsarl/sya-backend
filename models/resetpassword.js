'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ResetPassword extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ResetPassword.belongsTo(models.PersonHasRole, {
        foreignKey: "person_has_role_id",
        onDelete: "CASCADE",
      })
    }
  };
  ResetPassword.init({
    reset_password_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4, 
    },
    link: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    is_used: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    expires_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    person_has_role_id: {
      type: DataTypes.UUID,
      allowNull: true,
    },
  }, {
    sequelize,
    modelName: 'ResetPassword',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
  });
  return ResetPassword;
};