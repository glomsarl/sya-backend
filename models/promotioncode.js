'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PromotionCode extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PromotionCode.belongsTo(models.PersonHasRole, {
        foreignKey: "person_role_id",
        onDelete: "CASCADE"
      })
      PromotionCode.belongsTo(models.Structure, {
        foreignKey: "structure_id",
        onDelete: "CASCADE"
      })
      PromotionCode.hasMany(models.Payment, {
        foreignKey: "promotion_code_id",
        onDelete: "CASCADE"
      })
    }
  };
  PromotionCode.init({
    promotion_code_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    structure_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    person_has_role_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    code_value: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    is_used: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    percentage: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    used_at: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    created_by: {
      allowNull: false,
      type: DataTypes.DATE,
    },
  }, {
    sequelize,
    modelName: 'PromotionCode',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return PromotionCode;
};