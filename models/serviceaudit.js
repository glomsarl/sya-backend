'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ServiceAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ServiceAudit.belongsTo(models.Service, {
        foreignKey: "service_id",
        onDelete: "CASCADE"
      })
      ServiceAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "audited_by",
        onDelete: "CASCADE"
      })
    }
  };
  ServiceAudit.init({
    service_audit_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    service_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    audited_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    audited_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {
    sequelize,
    modelName: 'ServiceAudit',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return ServiceAudit;
};