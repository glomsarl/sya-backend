'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PromotionHasService extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PromotionHasService.belongsTo(models.Service, {
        foreignKey: "service_id",
        onDelete: "CASCADE"
      })
      PromotionHasService.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "created_by",
        onDelete: "CASCADE"
      })
      PromotionHasService.belongsTo(models.Promotion, {
        foreignKey: "promotion_id",
        onDelete: "CASCADE"
      })
      PromotionHasService.hasMany(models.PromotionHasServiceAudit, {
        foreignKey: "promotion_has_service_id",
      })
    }
  };
  PromotionHasService.init({
    promotion_has_service_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    promotion_id: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: "Promotion",
        key: "promotion_id",
        as: "promotion_id",
      }
    },
    service_id: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: "Service",
        key: "service_id",
        as: "service_id",
      }
    },
    consequent_price: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: "StructureHasPersonnel",
        key: "structure_has_personnel_id",
        as: "created_by",
      }
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    sequelize,
    modelName: 'PromotionHasService',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return PromotionHasService;
};