'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PersonAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PersonAudit.belongsTo(models.Person, {
        foreignKey: 'person_id',
        onDelete: 'CASCADE'
      })
    }
  };
  PersonAudit.init(
    {
      person_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        unique: true,
      },
      first_name: {
        type: DataTypes.STRING,
      },
      last_name: {
        type: DataTypes.STRING,
      },
      national_id_number: {
        type: DataTypes.STRING,
      },
      date_of_birth: {
        type: DataTypes.DATEONLY,
      },
      gender: {
        type: DataTypes.ENUM("M", "F"),
      },
      phone: {
        type: DataTypes.INTEGER,
      },
      email: {
        type: DataTypes.STRING,
      },
      person_image_ref: {
        type: DataTypes.STRING,
      },
      person_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      bonus_acquired: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: "PersonAudit",
      freezeTableName: true,
      allowNull: false,
      createdAt: false,
      updatedAt: false,
    }
  );
  return PersonAudit;
};