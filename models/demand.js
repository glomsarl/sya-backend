'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Demand extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Demand.belongsTo(models.PersonHasRole, {
        foreignKey: "requested_by",
        onDelete: "CASCADE",
      });
      Demand.belongsTo(models.PersonHasRole, {
        foreignKey: "validated_by",
        onDelete: "CASCADE",
      });
      Demand.belongsTo(models.Structure, {
        foreignKey: "structure_id",
        onDelete: "CASCADE",
      });
    }
  };
  Demand.init(
    {
      demand_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        unique: true,
      },
      validated_at: {
        type: DataTypes.DATE,
      },
      is_valid: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      rejected_at: {
        type: DataTypes.DATE,
      },
      rejection_reason: {
        type:  DataTypes.STRING,
      },
      requested_by: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      validated_by: {
        type: DataTypes.UUID,
      },
      structure_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
      is_deleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      deleted_at: {
        type: DataTypes.DATE,
      },
      is_viewed: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      viewed_at: {
        type: DataTypes.DATE,
      }
    },
    {
      sequelize,
      modelName: "Demand",
      freezeTableName: true,
      updatedAt: false,
      createdAt: false,
    }
  );
  return Demand;
};