'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class OrderConfirmation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      OrderConfirmation.belongsTo(models.Order, {
        foreignKey: "order_id",
        onDelete: "CASCADE",
      })
    }
  }
  OrderConfirmation.init(
    {
      order_confirmation_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      order_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      status: {
        type: DataTypes.ENUM('CONFIRMED', 'UNCONFIRMED', 'CANCELLED', 'SYSTEM-CANCELLED', 'SERVED'),
        allowNull: false,
        defaultValue: 'UNCONFIRMED',
      },
      confirmed_at: {
        type: DataTypes.DATE,
      },
      cancelled_at: {
        type: DataTypes.DATE,
      },
      served_at: {
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: 'OrderConfirmation',
      freezeTableName: true,
      timestamps: false,
    },
  )
  return OrderConfirmation
}
