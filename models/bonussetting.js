'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BonusSetting extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BonusSetting.belongsTo(models.PersonHasRole, {
        foreignKey: "created_by",
        onDelete: "CASCADE"
      })
      BonusSetting.hasMany(models.BonusSettingAudit, {
        foreignKey: "bonus_setting_id",
      })
    }
  };
  BonusSetting.init({
    bonus_setting_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    sy_value: {
      type: DataTypes.INTEGER,
    },
    sign_up_bonus: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    referral_bonus: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    structure_validation_bonus: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    is_sign_up_bonus_on: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'BonusSetting',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return BonusSetting;
};