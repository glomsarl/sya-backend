'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Publication extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Publication.belongsTo(models.Structure, {
        foreignKey: "structure_id",
        onDelete: "CASCADE",
      })
      Publication.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "created_by",
        onDelete: "CASCADE",
      })

    }
  };
  Publication.init({
    publication_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    publication_ref: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    structure_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    created_by: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    deleted_at: {
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'Publication',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return Publication;
};