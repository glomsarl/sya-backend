'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Structure extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Structure.hasMany(models.StructureHasPersonnel, {
        foreignKey: "structure_id",
      });
      Structure.hasMany(models.StructureHasCategory, {
        foreignKey: "structure_id",
      });
      Structure.hasMany(models.Demand, {
        foreignKey: "structure_id",
      });
      Structure.hasMany(models.Publication, {
        foreignKey: "structure_id",
      });
      Structure.hasMany(models.Demand, {
        foreignKey: "structure_id",
      });
      Structure.hasMany(models.Promotion, {
        foreignKey: "structure_id",
      });
      Structure.belongsTo(models.PersonHasRole, {
        foreignKey: "created_by",
      });
      Structure.hasMany(models.BonusSubscription, {
        foreignKey: "structure_id",
      });
      Structure.hasMany(models.PromotionCode, {
        foreignKey: "structure_id",
      });
      Structure.hasMany(models.Catalog, {
        foreignKey: "structure_id",
      });
      Structure.hasMany(models.Service, {
        foreignKey: "structure_id",
      });
      Structure.hasMany(models.StructureHasSubscription, {
        foreignKey: "structure_id",
      });
    }
  };
  Structure.init(
    {
      structure_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        unique: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      logo_ref: {
        type: DataTypes.STRING,
      },
      description: {
        type: DataTypes.TEXT,
      },
      longitude: {
        type: DataTypes.REAL,
        allowNull: false,
      },
      latitude: {
        type: DataTypes.REAL,
        allowNull: false,
      },
      specific_indications: {
        type: DataTypes.TEXT,
      },
      number_of_points: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      number_of_votes: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      is_valid: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      is_disabled: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      is_deleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
      created_by: {
        allowNull: false,
        type: DataTypes.UUID,
      }
    },
    {
      sequelize,
      modelName: "Structure",
      freezeTableName: true,
      updatedAt: false,
      createdAt: false,
    }
  );
  return Structure;
};