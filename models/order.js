'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Order.hasMany(models.OrderBonus, {
        foreignKey: "order_id",
      })
      Order.belongsTo(models.PersonHasRole, {
        foreignKey: "ordered_by",
      })
      Order.hasMany(models.Payment, {
        foreignKey: "order_id",
      })
      Order.hasMany(models.OrderConfirmation, {
        foreignKey: "order_id",
      })
      Order.hasMany(models.OrderContent, {
        foreignKey: "order_id",
      })
    }
  };
  Order.init({
    order_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    ordered_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    ordered_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    selected_date: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    order_type: {
      type: DataTypes.ENUM('R', 'T', 'H'),
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'Order',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return Order;
};