'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BonusSubscription extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BonusSubscription.belongsTo(models.PersonHasRole, {
        foreignKey: "created_by",
        onDelete: "CASCADE",
      })
      BonusSubscription.belongsTo(models.Promotion, {
        foreignKey: "promotion_id",
        onDelete: "CASCADE",
      })
      BonusSubscription.hasMany(models.BonusSubscriptionAudit, {
        foreignKey: "bonus_subscription_id",
      })
    }
  };
  BonusSubscription.init({
    bonus_subscription_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    promotion_id: {
      unique: true,
      allowNull: false,   
      type: DataTypes.UUID,
    },
    quantity_cashed_out: {
      defaultValue: 0,
      type: DataTypes.INTEGER,
    },
    quantity_cashed_in: {
      defaultValue: 0,
      type: DataTypes.INTEGER,
    },
    quantity_remaining: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,   
    },
    created_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {
    sequelize,
    modelName: 'BonusSubscription',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return BonusSubscription;
};