'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Promotion extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Promotion.belongsTo(models.Structure, {
        foreignKey: "structure_id",
        onDelete: "CASCADE"
      })
      Promotion.belongsTo(models.PersonHasRole, {
        foreignKey: "created_by",
        onDelete: "CASCADE"
      })
      Promotion.hasMany(models.OrderBonus, {
        foreignKey: "promotion_id",
      })
      Promotion.hasMany(models.PromotionAudit, {
        foreignKey: "promotion_id",
      })
      Promotion.hasMany(models.PromotionHasService, {
        foreignKey: "promotion_id",
      })
      Promotion.hasMany(models.BonusSubscription, {
        foreignKey: "promotion_id",
      })
    }
  };
  Promotion.init({
    promotion_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    structure_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    starts_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    ends_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    is_expired: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    promotion_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    promotion_type: {
      type: DataTypes.ENUM("Bonus", "Reduction"),
      allowNull: false,
    },
    value: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    purchasing_bonus: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false,
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    created_at: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'Promotion',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return Promotion;
};