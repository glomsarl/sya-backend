'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Mail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Mail.belongsTo(models.PersonHasRole, {
        foreignKey: "sent_by",
        onDelete: "CASCADE"
      })
    }
  };
  Mail.init({
    mail_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    sent_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    sent_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'Mail',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return Mail;
};