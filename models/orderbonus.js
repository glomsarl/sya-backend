'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrderBonus extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      OrderBonus.belongsTo(models.Order, {
        foreignKey: "order_id",
        onDelete: "CASCADE",
      })
      OrderBonus.belongsTo(models.PersonHasRole, {
        foreignKey: "created_by",
        onDelete: "CASCADE",
      })
      OrderBonus.belongsTo(models.Promotion, {
        foreignKey: "promotion_id",
        onDelete: "CASCADE",
      })
    }
  };
  OrderBonus.init({
    order_bonus_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    order_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    promotion_id : {
      type: DataTypes.UUID,
      allowNull: false,
    },
    bonus_acquired: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    is_acquired: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'OrderBonus',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return OrderBonus;
};