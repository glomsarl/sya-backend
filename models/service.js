'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Service extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Service.belongsTo(models.Structure, {
        foreignKey: 'structure_id',
        onDelete: 'CASCADE',
      })
      Service.belongsTo(models.StructureHasPersonnel, {
        foreignKey: 'created_by',
        onDelete: 'CASCADE',
      })

      Service.hasMany(models.PromotionHasService, {
        foreignKey: 'service_id',
      })
      Service.hasMany(models.ServiceAudit, {
        foreignKey: 'service_id',
      })
      Service.hasMany(models.ServiceImage, {
        foreignKey: 'service_id',
      })
      Service.hasMany(models.OrderContent, {
        foreignKey: 'service_id',
      })
      Service.hasMany(models.CatalogDetail, {
        foreignKey: 'service_id',
      })
    }
  }
  Service.init(
    {
      service_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      structure_id: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
      },
      unit_price: {
        type: DataTypes.FLOAT,
        defaultValue: 0,
        allowNull: false,
      },
      rating: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      votes: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      cancellation_delay: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
      created_by: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      is_deleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      main_image_ref: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'Service',
      freezeTableName: true,
      createdAt: false,
      updatedAt: false,
    },
  )
  return Service
}
