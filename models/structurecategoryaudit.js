'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StructureCategoryAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      StructureCategoryAudit.belongsTo(models.StructureHasPersonnel, {
        foreignKey: 'audited_by',
        onDelete: 'CASCADE'
      })

      StructureCategoryAudit.belongsTo(models.StructureCategory, {
        foreignKey: 'structure_category_id',
        onDelete: 'CASCADE'
      })
    }
  };
  StructureCategoryAudit.init(
    {
      structure_category_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        unique: true,
      },
      category_name: {
        type: DataTypes.STRING,
      },
      is_deleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      structure_category_id: {
        type: DataTypes.UUID,
        allowNull: false
      },
      audited_by: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: "StructureCategoryAudit",
      freezeTableName: true,
      updatedAt: false,
      createdAt: false,
    }
  );
  return StructureCategoryAudit;
};