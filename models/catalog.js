'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Catalog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Catalog.belongsTo(models.Structure, {
        foreignKey: "structure_id",
        onDelete: "CASCADE",
      })
      Catalog.belongsTo(models.StructureHasPersonnel, {
        foreignKey: "created_by",
        onDelete: "CASCADE",
      })
      Catalog.hasMany(models.CatalogAudit, {
        foreignKey: "catalog_id",
      })
      Catalog.hasMany(models.CatalogDetail, {
        foreignKey: "catalog_id",
      })
    }
  };
  Catalog.init({
    catalog_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    structure_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    ends_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    starts_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: false,
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false, 
      defaultValue: false
    }
  }, {
    sequelize,
    modelName: 'Catalog',
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
  });
  return Catalog;
};