'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SubscriptionAudit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SubscriptionAudit.belongsTo(models.Subscription, {
        foreignKey: 'subscription_id',
        onDelete: "CASCADE"
      })
      SubscriptionAudit.belongsTo(models.PersonHasRole, {
        foreignKey: 'audited_by',
        onDelete: "CASCADE"
      })
    }
  };
  SubscriptionAudit.init({
    subscription_audit_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    subscription_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    subscription_type: {
      type: DataTypes.ENUM('MONTHLY', 'TERMLY', 'YEARLY'),
      defaultValue: "MONTHLY",
      allowNull: false,
    },
    commission_rate: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    audited_at: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    audited_by: {
      allowNull: false,
      type: DataTypes.UUID,
    }
  }, {
    sequelize,
    modelName: 'SubscriptionAudit',
    freezeTableName: true,
    timestamps: false,
  });
  return SubscriptionAudit;
};