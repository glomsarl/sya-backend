'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Log extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Log.belongsTo(models.PersonHasRole, {
        foreignKey: 'person_has_role_id',
        onDelete: 'CASCADE',
      })
    }
  }
  Log.init(
    {
      log_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        unique: true,
      },
      logged_out_at: {
        type: DataTypes.DATE,
      },
      logged_in_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
      person_has_role_id: {
        type: DataTypes.UUID,
        onDelete: 'CASCADE',
      },
    },
    {
      sequelize,
      modelName: 'Log',
      freezeTableName: true,
      updatedAt: false,
      createdAt: false,
    },
  )
  return Log
}
