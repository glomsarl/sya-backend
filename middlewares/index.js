const client = require('../utils/redis')
const { verifyToken } = require('../utils/token-handler')

exports.clientAccess = (req, res, next) => {
  try {
    const sya_token = req.headers.sya_token
    if (!sya_token)
      return res.json({
        person: {
          sya_amount: 0,
        },
      })
    const { new_token, session_id, error } = verifyToken(
      sya_token,
      process.env.JWT_SECRET
    )
    if (error) return res.status(500).send({ message: error.message })
    client.hgetall(session_id, (err, session) => {
      if (session) {
        res.session = session
        res.new_token = new_token
        next()
      } else {
        if (!err) return res.status(500).send({ message: 'Session not found' })
        return res.status(500).send({
          message: err?.message || `Access denied for current user`,
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message:
        error.message ||
        'Sorry an error occured, please make sure you set you access information before the api call',
    })
  }
}

exports.ownerAccess = (req, res, next) => {
  try {
    const { session_id, new_token, error } = verifyToken(
      req.headers.sya_token,
      process.env.JWT_SECRET
    )
    if (error) throw new Error(error.message)
    client.hgetall(session_id, (err, session) => {
      if (session && session.role_id === process.env.OWNER_ID) {
        res.session = session
        res.new_token = new_token
        const structure_id = session.structure_id
        try {
          if (session.structure_id) {
            req.query.structure_id = structure_id
            next()
          } else throw new Error('A structure needs to be selected')
        } catch (error) {
          return res.status(500).send({
            message: error.message || `Access denied for current user`,
          })
        }
      } else
        return res.status(500).send({
          message: err?.message || `Access denied for current user`,
        })
    })
  } catch (error) {
    return res.status(500).send({
      message:
        error.message ||
        'Sorry an error occured, please make sure you set you access information before the api call',
    })
  }
}

exports.adminAccess = (req, res, next) => {
  try {
    const { session_id, new_token, error } = verifyToken(
      req.headers.sya_token,
      process.env.JWT_SECRET
    )
    if (error) throw new Error(error.message)
    client.hgetall(session_id, (err, session) => {
      if (session && session.role_id === process.env.ADMIN_ID) {
        res.user_id = session.user_id
        res.new_token = new_token
        next()
      } else
        return res.status(500).send({
          message: err?.message || `Access denied for current user`,
        })
    })
  } catch (error) {
    return res.status(500).send({
      message:
        error.message ||
        'Sorry an error occured, please make sure you set you access information before the api call',
    })
  }
}
