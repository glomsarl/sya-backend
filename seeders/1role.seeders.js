'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Role', [
        {
            role_id: process.env.CLIENT_ID, 
            created_at: new Date(), 
            role_name: 'CLIENT',
        },
        {
            role_id: process.env.ADMIN_ID, 
            created_at: new Date(), 
            role_name: 'ADMIN',
        },
        {
            role_id: process.env.OWNER_ID, 
            created_at: new Date(), 
            role_name: 'OWNER',
        },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Role', null, {});
  }
};