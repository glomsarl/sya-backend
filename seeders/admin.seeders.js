'use strict'
const bcrypt = require('bcryptjs')
const { Person, PersonHasRole } = require('../models')
const password_generator = require('generate-password')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const { person_id } = await Person.create({
      national_id_number: '1009523575',
      email: 'merlindjeumesi@gmail.com',
      first_name: 'Merlin',
      last_name: 'Djeumesi',
      created_at: new Date(),
    })
    const password = password_generator.generate({
      length: 8,
      lowercase: true,
      numbers: true,
      excludeSimilarCharacters: true,
    })
    await PersonHasRole.create({
      person_id,
      created_at: new Date(),
      role_id: process.env.ADMIN_ID,
      person_has_role_id: process.env.ADMIN_ACOUNT_ID,
      password: bcrypt.hashSync("sya-password", parseInt(process.env.SALT)),
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Person', null, {})
  },
}
