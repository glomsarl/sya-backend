'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'BonusSetting',
      [
        {
            bonus_setting_id: process.env.BONUS_SETTING_ID,
            sy_value: 0,
            sign_up_bonus: 0,
            referral_bonus: 0,
            structure_validation_bonus: 0,
            is_sign_up_bonus_on: false,
            created_at: new Date(),
            created_by: process.env.ADMIN_ACOUNT_ID,
        }
      ],
      {},
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('BonusSetting', null, {})
  },
}
