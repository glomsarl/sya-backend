'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'StructureCategory',
      [
        {
          structure_category_id: process.env.RESTAURANT_CATEGORY_ID,
          created_by: process.env.ADMIN_ACOUNT_ID,
          category_name: 'restaurant',
          created_at: new Date(),
        },
        {
          structure_category_id: process.env.CINEMA_CATEGORY_ID,
          created_by: process.env.ADMIN_ACOUNT_ID,
          category_name: 'cinema',
          created_at: new Date(),
        },
        {
          structure_category_id: process.env.ARCADE_MANEGE_ID,
          created_by: process.env.ADMIN_ACOUNT_ID,
          category_name: 'manege',
          created_at: new Date(),
        },
        {
          structure_category_id: process.env.FASHION_CATEGORY_ID,
          created_by: process.env.ADMIN_ACOUNT_ID,
          category_name: 'beautySalon',
          created_at: new Date(),
        },
      ],
      {},
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('StructureCategory', null, {})
  },
}
