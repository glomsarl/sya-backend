'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Subscription',
      [
        {
          subscription_id: process.env.SUBSCRIPTION_1,
          subscription_type: "MONTHLY",
          commission_rate: 12,
          created_at: new Date(),
          created_by: process.env.ADMIN_ACOUNT_ID,
        },
        // {
        //     subscription_id: process.env.SUBSCRIPTION_2,
        //     subscription_type: "TERMLY",
        //     commission_rate: 10,
        //     created_at: new Date(),
        //     created_by: process.env.ADMIN_ACOUNT_ID,
        // },
        // {
        //     subscription_id: process.env.SUBSCRIPTION_3,
        //     subscription_type: "YEARLY",
        //     commission_rate: 8,
        //     created_at: new Date(),
        //     created_by: process.env.ADMIN_ACOUNT_ID,
        // },
      ],
      {},
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Subscription', null, {})
  },
}
