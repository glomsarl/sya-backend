const {
  Log,
  Order,
  Person,
  Service,
  Payment,
  Feedback,
  Promotion,
  Structure,
  OrderBonus,
  PersonAudit,
  PersonBonus,
  OrderContent,
  BonusSetting,
  PersonHasRole,
  PromotionCode,
  CatalogDetail,
  OrderConfirmation,
  BonusSubscription,
  StructureCategory,
  StructureHasCategory,
  BonusSubscriptionAudit,
  sequelize,
  Sequelize,
  Sequelize: { Op },
} = require('../models')
const {
  orderRemainder,
  orderCancellationJob,
} = require('../jobs/order-reminder')
const client = require('../utils/redis')
const { mailer } = require('../utils/mailer')
const { generateToken } = require('../utils/token-handler')
const { generateRedisString } = require('../utils/string-generator')

const bcrypt = require('bcryptjs')
const uuidv4 = require('uuid').v4()

const login = async (person, res, transaction) => {
  try {
    const log = await Log.create(
      {
        person_has_role_id: person.user_id,
      },
      { transaction }
    )
    if (log) {
      return client.hmset(
        uuidv4,
        generateRedisString({
          ...person,
          log_id: log.log_id,
        }),
        (err, response) => {
          if (err) throw new Error(err.message)
          else if (response)
            return res.json({
              token: generateToken(uuidv4),
              person,
            })
        }
      )
    } else throw new Error('Sorry unable to login user')
  } catch (error) {
    return res.json({
      message: error.message || 'Sorry an unexpected error occured.',
    })
  }
}

exports.register = async (req, res, next) => {
  const { email, first_name, last_name, password, phone, referral_id } =
    req.body
  if (!email || !password) {
    return res
      .status(500)
      .send({ message: 'User cannot exist without email and password' })
  }
  return sequelize.transaction(async (transaction) => {
    try {
      const setting = await BonusSetting.findOne({
        attributes: ['sign_up_bonus', 'referral_bonus'],
        transaction,
      })
      if (!setting) throw new Error('Bonus setting not set yet')
      const { referral_bonus, sign_up_bonus } = setting
      let is_referral_id_valid = true
      if (referral_id) {
        const referral = await Person.findOne({
          attributes: { exclude: ['created_at'] },
          where: { person_id: referral_id },
        })
        if (referral) {
          await PersonAudit.create({
            ...referral.dataValues,
          })
          await Person.increment(
            { bonus_acquired: referral_bonus },
            { where: { person_id: referral_id }, transaction }
          )
        } else {
          is_referral_id_valid = false
          req.error = 'Invalid referal link'
        }
      }
      const person = await Person.create(
        {
          email,
          phone,
          first_name,
          last_name,
          referal_id: is_referral_id_valid ? referral_id : null,
          bonus_acquired: sign_up_bonus,
        },
        { transaction }
      )
      const hash = bcrypt.hashSync(password, parseInt(process.env.SALT))
      const { person_has_role_id: user_id, role_id } =
        await PersonHasRole.create(
          {
            person_id: person.person_id,
            password: hash,
            role_id: process.env.CLIENT_ID,
          },
          { transaction }
        )
      person.referral_link = `https://seeyouagain.fun/sign-up/${person.person_id}`
      mailer([email], 'SYA SIGN UP', '../views/AccountCreation.hbs', {
        sign_up_bonus: setting?.sign_up_bonus || 0,
        referral_bonus: setting?.referral_bonus || 0,
        referral_link: person.referral_link,
      })
        .then((info) => console.log(info))
        .catch((error) => console.log(error))
      if (req.url === '/sign-up/purchase') {
        res.user_data = { ...person.dataValues, role_id, user_id }
        next()
      } else {
        return await login(
          { ...person.dataValues, role_id, user_id },
          res,
          transaction
        )
      }
    } catch (error) {
      return res.status(500).send({
        message:
          error.message ||
          'Sorry an erro occured. Please check you internet connection and try again',
      })
    }
  })
}

exports.findAllUserOrders = async (req, res) => {
  const { user_id, person_has_role_id } = res.session
  const { status, selected_date } = req.query

  const ordered_by = person_has_role_id ?? user_id
  try {
    const order_data = await Order.findAll({
      attributes: ['order_id', 'selected_date', 'ordered_by', 'order_type'],
      where: selected_date
        ? { ordered_by, ordered_at: { [Op.gte]: selected_date } }
        : { ordered_by },
      include: [
        {
          model: Payment,
          attributes: ['payment_method'],
        },
        {
          model: OrderContent,
          attributes: ['order_content_id', 'unit_price', 'quantity'],
          include: {
            model: Service,
            attributes: ['name', 'service_id', 'main_image_ref'],
            where: { is_deleted: false },
            include: {
              model: Structure,
              where: { is_deleted: false },
              attributes: ['name', 'longitude', 'latitude', 'logo_ref'],
            },
          },
        },
        {
          model: OrderConfirmation,
          attributes: ['status'],
          where:
            status && status !== '*'
              ? {
                  status:
                    status === 'CANCELLED'
                      ? ['CANCELLED', 'SYSTEM-CANCELLED']
                      : status,
                }
              : 1,
        },
      ],
    })
    const order_histories = []
    order_data.map((order) => {
      const {
        order_id,
        selected_date: ordered_at,
        ordered_by,
        order_type,
        Payments,
        OrderContents,
        OrderConfirmations,
      } = order
      const { payment_method } = Payments[0] ?? {}
      const { status } = OrderConfirmations[0] ?? {}
      if (OrderContents && OrderContents.length > 0) {
        const {
          quantity,
          unit_price,
          order_content_id,
          Service: {
            service_id,
            main_image_ref,
            name: service_name,
            Structure: { name: structure_name, longitude, latitude, logo_ref },
          },
        } = OrderContents[0]
        order_histories.push({
          order_id,
          ordered_at,
          ordered_by,
          order_type,
          status,
          payment_method,
          order_content_id,
          service: {
            quantity,
            service_id,
            unit_price,
            service_name,
            main_image_ref: `${process.env.MULTER_FOLDER}/${main_image_ref}`,
          },
          structure: {
            logo_ref: `${process.env.MULTER_FOLDER}/${logo_ref}`,
            structure_name,
            longitude,
            latitude,
          },
        })
      }
    })
    return res.json({ order_histories })
  } catch (error) {
    return res.status(500).send({
      message:
        error.message ||
        'Sorry an unexpected error occured. Please contact your system administrator',
    })
  }
}

exports.findPersonSy = async (req, res) => {
  const { person_id } = req.query
  try {
    if (!person_id)
      throw new Error('person_id is required for getting user bonus points')
    const sy_points = await PersonBonus.findAll({
      attributes: ['person_bonus_id', 'value'],
      include: [
        {
          model: StructureCategory,
          attributes: ['category_name'],
        },
        {
          model: PersonHasRole,
          attributes: ['person_has_role_id'],
          where: { person_id },
        },
      ],
    })
    return res.json({
      sy_points: sy_points.map(
        ({ person_bonus_id, value, StructureCategory: { category_name } }) => ({
          value,
          category_name,
          person_bonus_id,
        })
      ),
    })
  } catch (error) {
    return res.status(500).send({
      message:
        error.message ||
        'Sorry an unexpected error occured. Please contact your system administrator',
    })
  }
}

exports.purchaseService = (req, res) => {
  const {
    client_order: {
      catalog_detail_id,
      promotion_code_id,
      is_sy_point_used,
      selected_date,
      promotion_id,
      order_type,
      service_id,
      quantity,
    },
  } = req.body

  const { user_id, person_has_role_id, person_id, email } = req.url.startsWith(
    '/sign-'
  )
    ? res.user_data
    : res.session
  const ordered_by = person_has_role_id ?? user_id
  return sequelize.transaction(async (transaction) => {
    try {
      if (
        !catalog_detail_id ||
        !selected_date ||
        !order_type ||
        !service_id ||
        !quantity
      )
        throw new Error('Incomplete data supply for purchase a service')

      const service = await Service.findOne({
        where: { is_deleted: false, service_id },
        attributes: ['unit_price', 'name'],
        include: {
          model: Structure,
          where: { is_disabled: false },
          attributes: ['structure_id', 'name'],
        },
        transaction,
      })
      if (!service) throw new Error('Unknown Service')
      const {
        unit_price,
        name: service_name,
        Structure: { name: structure_name, structure_id },
      } = service
      let unit_consequent_price = unit_price

      const catalogDetail = await CatalogDetail.findOne({
        attributes: ['quantity'],
        where: { catalog_detail_id },
        transaction,
      })
      if (!catalogDetail) throw new Error('Unknown Catalog details')
      if (catalogDetail.quantity < quantity)
        throw new Error('Quantity not satisfied')
      const newQte = catalogDetail.quantity - quantity
      await CatalogDetail.update(
        {
          quantity: newQte,
          is_deleted: newQte === 0,
        },
        {
          where: {
            catalog_detail_id,
          },
          transaction,
        }
      )

      const { order_id } = await Order.create(
        {
          ordered_by,
          order_type,
          selected_date,
        },
        transaction
      )

      let payment = null
      if (promotion_id) {
        const promotion = await Promotion.findOne({
          where: { promotion_id },
          attributes: ['value', 'purchasing_bonus'],
          transaction,
        })
        if (!promotion) throw new Error('Invalid promotion id')
        const { value, purchasing_bonus } = promotion

        const bonus_subscription = await BonusSubscription.findOne({
          where: { promotion_id },
          attributes: { exclude: ['created_at', 'created_by'] },
          transaction,
        })
        if (bonus_subscription) {
          const bonus_setting = await BonusSetting.findOne({
            attributes: ['sy_value'],
            transaction,
          })
          if (!bonus_setting) throw new Error('Bonus setting not yet set')
          const { sy_value } = bonus_setting

          const user = await Person.findOne({
            where: { person_id },
            transaction,
          })

          const {
            StructureCategory: { structure_category_id },
          } = await StructureHasCategory.findOne({
            where: { structure_id },
            attributes: ['structure_has_category_id'],
            include: {
              model: StructureCategory,
              attributes: ['structure_category_id'],
            },
            transaction,
          })
          const person_bonus = await PersonBonus.findOne({
            where: { structure_category_id, person_has_role_id: ordered_by },
            transaction,
          })

          if (is_sy_point_used) {
            const amount = (unit_consequent_price * quantity) / sy_value
            if (!person_bonus || amount > person_bonus.value)
              throw new Error(
                "Sorry, you don't have enough sy point to purchase in this category"
              )
            await PersonBonus.increment(
              {
                value: -amount,
              },
              {
                where: { person_has_role_id, structure_category_id },
                transaction,
              }
            )
            await PersonAudit.create(
              {
                ...user.dataValues,
              },
              { transaction }
            )
            await Person.increment(
              {
                bonus_acquired: -amount,
              },
              { where: { person_id }, transaction }
            )

            await BonusSubscriptionAudit.create(
              {
                ...bonus_subscription.dataValues,
                audited_by: ordered_by,
              },
              { transaction }
            )
            await BonusSubscription.increment(
              {
                quantity_cashed_in: amount,
                quantity_remaining: -amount,
              },
              {
                where: { promotion_id },
                transaction,
              }
            )

            payment = await Payment.create(
              {
                order_id,
                is_paid: true,
                payment_method: 'SY',
                initial_due: amount,
                payment_service: 'SYA',
                amount_paid: amount,
                paid_by: ordered_by,
              },
              { transaction }
            )
          } else {
            await OrderBonus.create(
              {
                order_id,
                promotion_id,
                created_by: ordered_by,
                bonus_acquired: purchasing_bonus,
              },
              { transaction }
            )
          }
        } else {
          unit_consequent_price =
            service.unit_price - 0.01 * value * service.unit_price
        }
      } else if (promotion_code_id) {
        const code = await PromotionCode.findOne({
          where: { promotion_code_id, is_used: false },
          attributes: ['percentage'],
          transaction,
        })
        if (!code) throw new Error('Promotion code is not valid')
        unit_consequent_price =
          service.unit_price - 0.01 * code.percentage * service.unit_price
      }
      await OrderContent.create(
        {
          order_id,
          quantity,
          ordered_by,
          service_id,
          unit_price: unit_consequent_price,
        },
        { transaction }
      )
      await OrderConfirmation.create({ order_id }, { transaction })
      mailer([email], 'Order Reminder', '../views/OrderNotice.hbs', {
        quantity,
        service_name,
        structure_name,
        status: 'UNCONFIRMED',
        serving_date: selected_date,
        unit_price: unit_consequent_price,
        payment_status: payment ? 'PAID' : 'NOT PAID',
      })
        .then((info) => console.log(info))
        .catch((error) => console.log(error))
      orderRemainder(24, {
        email,
        quantity,
        order_id,
        service_name,
        structure_name,
        serving_date: selected_date,
      }).start()
      orderCancellationJob(selected_date, {
        unit_price: unit_consequent_price,
        serving_date: selected_date,
        email,
        structure_name,
        service_name,
        quantity,
        order_id,
      }).start()
      return req.url.startsWith('/sign-')
        ? await login(res.user_data, res, transaction)
        : res.json({
            is_purchase_done: true,
          })
    } catch (error) {
      return res.status(500).send({
        message:
          error.message ||
          'Sorry an unexpected error occured. Please contact your system administrator',
      })
    }
  })
}

exports.changeOrderStatus = async (req, res) => {
  const { order_id, status } = req.body

  try {
    if (!order_id || !status) throw new Error('order id is required')
    const order_content = await OrderContent.findOne({
      attributes: ['order_content_id'],
      include: [
        {
          model: Order,
          attributes: ['ordered_at', 'selected_date'],
          where: { order_id },
        },
        {
          model: Service,
          attributes: ['cancellation_delay'],
          where: { is_deleted: false },
        },
      ],
    })
    if (!order_content) throw new Error('Invalid order id')

    const {
      Order: { ordered_at, selected_date },
      Service: { cancellation_delay },
    } = order_content
    const cancellation_date = new Date(
      new Date().setDate(new Date(ordered_at).getDate() + cancellation_delay)
    )
    const updating_data = {
      CONFIRMED: { status, confirmed_at: new Date() },
      CANCELLED: { status, cancelled_at: new Date() },
    }
    if (
      cancellation_date - new Date() > 0 &&
      new Date(selected_date) - new Date() > 0
    )
      await OrderConfirmation.update(updating_data[status], {
        where: { order_id },
      })
    else throw new Error('Action can no more be done on this order')
    return res.json({ is_order_updated: true })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.giveFeedback = async (req, res) => {
  const { feedback, rating, order_content_id } = req.body
  const { user_id, person_has_role_id } = res.session
  const created_by = person_has_role_id ?? user_id
  try {
    if (!feedback || !rating || !order_content_id)
      throw new Error('feedback and rating is required')
    const new_feedback = await Feedback.create({
      rating: rating * 2,
      feedback,
      created_by,
      order_content_id,
    })
    const feedbacks = await Feedback.count(order_content_id)
    const ratings = await Feedback.sum('rating', {
      where: { order_content_id },
    })
    const { service_id } = await OrderContent.findByPk(order_content_id)
    await Service.update(
      {
        votes: feedbacks,
        rating: ratings / feedbacks,
      },
      {
        where: { service_id },
      }
    )
    return res.json({ feedback: new_feedback })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}
