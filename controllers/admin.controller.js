const {
  Log,
  Order,
  Demand,
  Person,
  Feedback,
  Structure,
  Promotion,
  OrderContent,
  BonusSetting,
  Subscription,
  PersonHasRole,
  FeedbackAudit,
  StructureAudit,
  OrderConfirmation,
  StructureCategory,
  SubscriptionAudit,
  BonusSettingAudit,
  BonusSubscription,
  StructureHasCategory,
  StructureHasSubscription,
  sequelize,
  Sequelize,
  Sequelize: { Op },
} = require('../models')
const {
  subscriptionReminder,
  verifyStructuresSubscription,
} = require('../jobs/subscription-remider')
const { mailer } = require('../utils/mailer')
const { generateStatistics } = require('./common')

exports.fetchAllDemands = async (req, res) => {
  const { offsets } = req.query

  try {
    let demands = await Demand.findAll({
      where: { is_deleted: false },
      attributes: [
        'demand_id',
        'is_valid',
        'rejected_at',
        'created_at',
        'is_viewed',
      ],
      ...offsets,
      include: {
        model: Structure,
        where: { is_deleted: false },
        attributes: [
          'structure_id',
          'name',
          'longitude',
          'latitude',
          'specific_indications',
          'description',
        ],
        include: [
          {
            model: StructureHasCategory,
            attributes: ['structure_has_category_id'],
            where: { is_deleted: false },
            include: {
              model: StructureCategory,
              where: { is_deleted: false },
              attributes: ['category_name'],
            },
          },
          {
            model: PersonHasRole,
            where: { is_deleted: false },
            attributes: ['person_has_role_id'],
            include: {
              model: Person,
              attributes: [
                'first_name',
                'last_name',
                'date_of_birth',
                'gender',
                'national_id_number',
                'email',
                'phone',
              ],
            },
          },
        ],
      },
    })
    demands = demands.map((demand) => {
      const {
        is_viewed,
        created_at,
        rejected_at,
        is_valid,
        demand_id,
        Structure: {
          structure_id,
          name,
          latitude,
          longitude,
          description,
          specific_indications,
        },
        Structure: {
          StructureHasCategories,
          PersonHasRole: { Person: person },
        },
      } = demand
      delete demand.Structure
      return {
        is_viewed,
        created_at,
        rejected_at,
        is_valid,
        demand_id,
        structure: {
          structure_id,
          name,
          latitude,
          longitude,
          description,
          specific_indications,
          category_name: StructureHasCategories.map(
            ({ StructureCategory: { category_name } }) => category_name
          ),
        },
        owner: person.dataValues,
      }
    })
    const number_of_demands = await Demand.findOne({
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col('demand_id')),
          'total_number_of_demands',
        ],
      ],
    })
    return res.json({ demands, ...number_of_demands.dataValues })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.fetchAllStructures = async (req, res) => {
  const { offsets } = req.query

  try {
    let structures = await Structure.findAll({
      ...offsets,
      where: { is_deleted: false, is_valid: true },
      attributes: [
        'structure_id',
        'name',
        'longitude',
        'latitude',
        'specific_indications',
        'description',
        'is_disabled',
      ],
      include: [
        {
          model: StructureHasCategory,
          attributes: ['structure_has_category_id'],
          where: { is_deleted: false },
          include: {
            model: StructureCategory,
            where: { is_deleted: false },
            attributes: ['category_name'],
          },
        },
        {
          model: PersonHasRole,
          where: { is_deleted: false },
          attributes: ['person_has_role_id'],
          include: {
            model: Person,
            attributes: [
              'first_name',
              'last_name',
              'date_of_birth',
              'gender',
              'national_id_number',
              'email',
              'phone',
            ],
          },
        },
      ],
    })
    structures = structures.map((_structure) => {
      const {
        description,
        specific_indications,
        latitude,
        longitude,
        structure_id,
        name,
        is_disabled,
        StructureHasCategories,
        PersonHasRole: { Person: person },
      } = _structure
      delete _structure.PersonHasRole
      delete _structure.StructureHasCategories
      return {
        description,
        specific_indications,
        latitude,
        longitude,
        structure_id,
        is_disabled,
        name,
        category_name: StructureHasCategories.map(
          ({ StructureCategory: { category_name } }) => category_name
        ),
        owner: person.dataValues,
      }
    })
    const number_of_structures = Structure.findOne({
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col('structure_id')),
          'total_number_of_structures',
        ],
      ],
    })
    return res.json({ structures, ...number_of_structures.dataValues })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.setStructureAbility = async (req, res) => {
  const { structure_ids, is_disabled } = req.body
  try {
    if (!structure_ids) throw new Error('structure ids is required')
    await Structure.update(
      { is_disabled: !is_disabled },
      {
        where: { is_deleted: false, structure_id: [...structure_ids] },
      }
    )
    return res.json({ is_structure_abilitated: true })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, something went wrong',
    })
  }
}

exports.validateDemand = (req, res) => {
  const { demand_id } = req.params
  const { user_id: validated_by } = res
  const { rejection_reason } = req.body

  return sequelize.transaction(async (transaction) => {
    try {
      await Demand.update(
        rejection_reason
          ? {
              rejection_reason,
              rejected_at: new Date(),
              validated_by,
              is_valid: false,
            }
          : { validated_at: new Date(), validated_by, is_valid: true },
        {
          where: { is_deleted: false, demand_id },
          transaction,
        }
      )
      const { structure_id } = await Demand.findOne({
        where: { is_deleted: false, demand_id },
        attributes: ['structure_id'],
      })
      await Structure.update(
        {
          is_valid: !Boolean(rejection_reason),
        },
        {
          where: { is_deleted: false, structure_id },
          transaction,
        }
      )
      const {
        name: structure_name,
        PersonHasRole: {
          Person: { email },
        },
      } = await Structure.findOne({
        attributes: ['name'],
        where: { structure_id },
        include: {
          model: PersonHasRole,
          attributes: ['person_has_role_id'],
          where: { is_deleted: false },
          include: {
            model: Person,
            attributes: ['email'],
          },
        },
        transaction,
      })
      if (rejection_reason)
        mailer(
          [email],
          'SYA Structure Response',
          '../views/StructureDemandResponse.hbs',
          {
            rejection_reason,
            rejection_date: `${new Date().toDateString()} at ${new Date().toTimeString()}`,
          }
        )
          .then((info) => console.log(info))
          .catch((error) => console.log(error))
      else {
        const bonus_setting = await BonusSetting.findOne({
          attributes: ['structure_validation_bonus'],
        })
        let ends_at = new Date(new Date().setDate(new Date().getDate() + 7))
        const promotion = await Promotion.create(
          {
            value: bonus_setting.structure_validation_bonus,
            ends_at,
            structure_id,
            purchasing_bouns: 5,
            starts_at: new Date(),
            promotion_type: 'Bonus',
            created_by: validated_by,
            promotion_name: 'SYA Promotion',
          },
          { transaction }
        )
        await BonusSubscription.create(
          {
            quantity_remaining: bonus_setting.structure_validation_bonus,
            promotion_id: promotion.promotion_id,
            created_by: validated_by,
          },
          { transaction }
        )

        await StructureHasSubscription.update(
          { started_at: new Date() },
          {
            where: { structure_id, started_at: null },
            transaction,
          }
        )
        const subscription = await StructureHasSubscription.findOne({
          where: { structure_id },
          attributes: ['subscription_id'],
          include: [
            {
              model: Subscription,
              attributes: ['commission_rate', 'subscription_type'],
            },
          ],
          transaction,
        })
        if (!subscription) throw new Error('Subscrition error')
        const {
          Subscription: { commission_rate, subscription_type },
        } = subscription
        const intervals = {
          MONTHLY: { next: 20, lastest: 30 },
          TERMLY: { next: 170, lastest: 180 },
          YEARLY: { next: 350, lastest: 360 },
        }
        const { next, lastest } = intervals[subscription_type]
        const next_date = new Date(
          new Date().setDate(new Date().getDate() + next)
        )
        const lastest_date = new Date(
          new Date().setDate(new Date().getDate() + lastest)
        )
        subscriptionReminder(
          next_date,
          {
            email,
            lastest_date,
            structure_id,
            structure_name,
            commission_rate,
            month: new Date().toDateString(),
          },
          1
        ).start()
        verifyStructuresSubscription(lastest, structure_id).start()
        mailer(
          [email],
          'SYA Structure Response',
          '../views/StructureValidationNotice.hbs',
          {
            validation_date: `${new Date().toDateString()} at ${new Date().toTimeString()}`,
          }
        )
          .then((info) => console.log(info))
          .catch((error) => console.log(error))
      }
      return res.json({
        is_demand_validated: true,
      })
    } catch (error) {
      return res.status(500).send({
        message: error.message || 'Sorry an unknown error occured',
      })
    }
  })
}

exports.deletedDemands = async (req, res) => {
  const { demands } = req.params

  try {
    if (!demands || demands <= 0) throw new Error('demand id is required')
    await Demand.update(
      {
        is_deleted: true,
        deleted_at: new Date(),
      },
      {
        where: { is_deleted: false, demand_id: [...demands] },
        transaction,
      }
    )
    return res.json({ is_demand_deleted: true })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.viewDemands = async (req, res) => {
  const { demands } = req.body
  try {
    if (!demands || demands <= 0) throw new Error('demand id is required')
    await Demand.update(
      {
        is_viewed: true,
        viewed_at: new Date(),
      },
      {
        where: { is_deleted: false, demand_id: [...demands] },
      }
    )
    return res.json({ is_demand_read: true })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.sendEmails = async (req, res) => {
  const { emails, message } = req.body
  try {
    if (!emails || emails.length === 0 || !message)
      throw new Error('An array of emails and a message most be provided')
    const info = await mailer(
      emails,
      'ADMIN NOTICE',
      '../views/AdminNotice.hbs',
      {
        message,
      }
    )
    return res.json({ info, are_emails_send: true })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.structuresMinorFetch = async (req, res) => {
  const { structure_category_id, search } = req.query
  try {
    let structures = await Structure.findAll({
      where: { is_deleted: false, name: { [Op.substring]: search ?? '' } },
      attributes: ['name', 'specific_indications'],
      include: [
        {
          model: StructureHasCategory,
          attributes: ['structure_has_category_id'],
          where: { is_deleted: false },
          include: {
            model: StructureCategory,
            where: structure_category_id
              ? { is_deleted: false, structure_category_id }
              : { is_deleted: false },
            attributes: ['category_name'],
          },
        },
        {
          model: PersonHasRole,
          where: { is_deleted: false },
          attributes: ['person_has_role_id'],
          include: {
            model: Person,
            attributes: ['first_name', 'last_name', 'email'],
          },
        },
      ],
    })
    structures = structures.map((_structure) => {
      const {
        name: structure_name,
        specific_indications,
        StructureHasCategories,
        PersonHasRole: {
          Person: { first_name, last_name, email: structure_email },
        },
      } = _structure
      delete _structure.PersonHasRole
      delete _structure.StructureHasCategories
      return {
        structure_name,
        specific_indications,
        category_name: StructureHasCategories.map(
          ({ StructureCategory: { category_name } }) => category_name
        ),
        structure_manager: `${first_name} ${last_name}`,
        structure_email,
      }
    })
    return res.json({ structures })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.getClientStatistics = async (req, res) => {
  const { interval, ends_at, starts_at } = req.query
  try {
    if (!interval) throw new Error('interval_duration is required')
    const persons = await Person.findAll({
      attributes: ['person_id', 'created_at'],
      order: [['created_at', 'ASC']],
      where:
        starts_at && ends_at && starts_at !== '' && ends_at !== ''
          ? {
              created_at: {
                [Op.and]: {
                  [Op.gte]: parseInt(starts_at),
                  [Op.lte]: parseInt(ends_at),
                },
              },
            }
          : starts_at && starts_at !== ''
          ? { created_at: { [Op.gte]: parseInt(starts_at) } }
          : ends_at !== ''
          ? { created_at: { [Op.lte]: parseInt(ends_at) } }
          : {},
      include: {
        model: PersonHasRole,
        attributes: ['person_has_role_id'],
        where: { role_id: { [Op.ne]: process.env.ADMIN_ID } },
      },
    })
    return res.json({
      client_statistics:
        persons.length > 0
          ? generateStatistics(persons, parseInt(interval), 'client')
          : [],
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.getAdminOverviews = async (req, res) => {
  try {
    const client = await Person.findOne({
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col('Person.person_id')),
          'number_of_clients',
        ],
      ],
      include: {
        model: PersonHasRole,
        attributes: ['person_has_role_id'],
        where: { role_id: { [Op.ne]: process.env.ADMIN_ID } },
      },
    })
    const structure = await Structure.findOne({
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col('structure_id')),
          'number_of_structures',
        ],
      ],
      where: { is_deleted: false, is_disabled: false, is_valid: true },
    })
    const order = await Order.findOne({
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col('order_id')),
          'number_of_served_orders',
        ],
      ],
      include: {
        model: OrderConfirmation,
        where: { status: 'SERVED' },
        attributes: ['order_confirmation_id'],
      },
    })
    const order_contents = await OrderConfirmation.findAll({
      where: { status: 'SERVED' },
      attributes: ['order_confirmation_id'],
      include: {
        model: Order,
        attributes: ['order_id'],
        include: {
          model: OrderContent,
          attributes: ['quantity', 'unit_price'],
        },
      },
    })
    const total_money_generated = order_contents.reduce(
      (total, { Order: { OrderContents: orders } }) => {
        return (
          total +
          orders.reduce(
            (sub_total, { quantity, unit_price }) =>
              sub_total + quantity * unit_price,
            0
          )
        )
      },
      0
    )
    return res.json({
      overviews: {
        total_money_generated,
        number_of_clients: client?.dataValues.number_of_clients,
        number_of_served_orders: order?.dataValues.number_of_served_orders,
        number_of_structures: structure?.dataValues.number_of_structures,
      },
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.writeBonusSetting = async (req, res) => {
  const {
    sy_value,
    sign_up_bonus,
    referral_bonus,
    structure_validation_bonus,
    is_sign_up_bonus_on,
  } = req.body
  const { user_id: created_by } = res

  try {
    if (
      !sy_value &&
      !sign_up_bonus &&
      !referral_bonus &&
      !structure_validation_bonus
    )
      throw new Error('All setting parameters required')

    const bonus_setting = await BonusSetting.create(
      {
        sy_value,
        created_by,
        sign_up_bonus,
        referral_bonus,
        is_sign_up_bonus_on,
        structure_validation_bonus,
        bonus_setting_id: process.env.BONUS_SETTING_ID,
      },
      { onDuplicatedUpdate: ['bonus_setting_id'] }
    )
    return res.json({ bonus_setting })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.updateBonusSetting = async (req, res) => {
  const { user_id: audited_by } = res
  const {
    sy_value,
    sign_up_bonus,
    referral_bonus,
    structure_validation_bonus,
    is_sign_up_bonus_on,
  } = req.body
  return sequelize.transaction(async (transaction) => {
    try {
      if (
        (!sy_value &&
          !sign_up_bonus &&
          !referral_bonus &&
          !structure_validation_bonus &&
          is_sign_up_bonus_on === null) ||
        is_sign_up_bonus_on === undefined
      )
        throw new Error('All setting parameters required')
      const bonus_setting = await BonusSetting.findOne({
        attributes: { exclude: ['created_at', 'created_by'] },
        transaction,
      })
      if (!bonus_setting) {
        const bonus_setting = await BonusSetting.create(
          {
            sy_value,
            sign_up_bonus,
            referral_bonus,
            is_sign_up_bonus_on,
            created_by: audited_by,
            structure_validation_bonus,
            bonus_setting_id: process.env.BONUS_SETTING_ID,
          },
          { onDuplicatedUpdate: ['bonus_setting_id'] }
        )
        return res.json({ bonus_setting })
      }
      await BonusSettingAudit.create(
        {
          ...bonus_setting.dataValues,
          audited_by,
        },
        { transaction }
      )
      await BonusSetting.update(
        {
          sy_value,
          sign_up_bonus,
          referral_bonus,
          structure_validation_bonus,
          is_sign_up_bonus_on,
        },
        {
          where: {},
          transaction,
        }
      )
      return res.json({ is_bonus_setting_updated: true })
    } catch (error) {
      return res.status(500).send({
        message: error.message || 'Sorry, Something went wrong',
      })
    }
  })
}

exports.createSubscription = async (req, res) => {
  const { subscription_type, commission_rate } = req.body

  try {
    if (!subscription_type || !commission_rate)
      throw new Error('subscription_type and commission_rate are required')
    const subscription = await Subscription.create({
      commission_rate,
      subscription_type,
    })
    return res.json({ subscription })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.deleteSubscription = async (req, res) => {
  const { subscription_id } = req.params
  const { user_id: audited_by } = res

  try {
    if (!subscription_id) throw new Error('subscription_id is required')
    const subs = await StructureHasSubscription.findOne({
      where: { subscription_id },
    })
    if (subs) throw new Error('Cannot delete used subscription')
    const audit_subscription = await Subscription.findOne({
      attributes: ['commission_rate', 'subscription_type'],
      where: { is_deleted: false },
    })
    await SubscriptionAudit.create({
      ...audit_subscription.dataValues,
      audited_by,
    })
    await Subscription.update(
      {
        is_deleted: true,
      },
      {
        where: { subscription_id },
      }
    )
    return res.json({ is_subscription_deleted: true })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.updateSubscription = (req, res) => {
  const { commission_rate, subscription_type } = req.body
  const { subscription_id } = req.params
  const { user_id: audited_by } = res

  return sequelize.transaction(async (transaction) => {
    try {
      if (!subscription_id || !commission_rate || !subscription_type)
        throw new Error(
          'subscription_id, commission_rate and subscription_type are required'
        )
      const audit_subscription = await Subscription.findOne({
        attributes: ['commission_rate', 'subscription_type'],
        where: { is_deleted: false },
      })
      if (!audit_subscription) throw new Error('Cannot update unknwon record')
      await SubscriptionAudit.create(
        {
          ...audit_subscription.dataValues,
          subscription_id,
          audited_by,
        },
        { transaction }
      )
      await Subscription.update(
        {
          commission_rate,
          subscription_type,
        },
        {
          where: { subscription_id },
          transaction,
        }
      )
      return res.json({ is_subscription_updated: true })
    } catch (error) {
      return res.status(500).send({
        message: error.message || 'Sorry, Something went wrong',
      })
    }
  })
}

exports.toggleFeedbackVisibility = async (req, res) => {
  const { feedback_id } = req.params
  const { user_id: audited_by } = res
  try {
    if (!feedback_id) throw new Error('feedback_id is required')
    const audit_feedback = await Feedback.findOne({
      attributes: { exclude: ['created_at', 'created_by'] },
    })
    return sequelize.transaction(async (transaction) => {
      await FeedbackAudit.create(
        {
          ...audit_feedback.dataValues,
          audited_by,
        },
        { transaction }
      )
      await Feedback.update(
        {
          is_visible: !audit_feedback.is_visible,
        },
        {
          where: { feedback_id },
          transaction,
        }
      )
      return res.json({ is_visiblility_toggled: true })
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.subscriptionStatistics = async (req, res) => {
  try {
    let active_structures = 0
    let bloked_structures = 0
    let cleared_structures = 0
    const active = await Structure.findOne({
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col('structure_id')),
          'active_structures',
        ],
      ],
      where: { is_disabled: false },
    })
    if (active) active_structures = active.dataValues.active_structures
    const blocked = await Structure.findOne({
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col('structure_id')),
          'bloked_structures',
        ],
      ],
      where: { is_disabled: true },
    })
    if (blocked) bloked_structures = blocked.dataValues.bloked_structures
    const cleared = await Structure.findOne({
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col('structure_id')),
          'cleared_structures',
        ],
      ],
      where: { is_disabled: false },
      include: {
        model: StructureHasSubscription,
        attributes: ['structure_has_subscription_id'],
        where: { expired_at: { [Op.gte]: new Date() } },
      },
    })
    if (cleared) cleared_structures = cleared.dataValues.cleared_structures
    return res.json({
      statistics: { active_structures, bloked_structures, cleared_structures },
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}
