const {
  Log,
  Order,
  Person,
  Demand,
  Service,
  Catalog,
  Schedule,
  Feedback,
  Structure,
  Promotion,
  PersonAudit,
  Publication,
  ServiceImage,
  OrderContent,
  Subscription,
  BonusSetting,
  CatalogDetail,
  PersonHasRole,
  ResetPassword,
  PromotionCode,
  BonusSubscription,
  StructureCategory,
  PersonHasRoleAudit,
  SubscriptionPayment,
  PromotionHasService,
  StructureHasCategory,
  StructureHasPersonnel,
  StructureHasSubscription,
  sequelize,
  Sequelize,
  Sequelize: { Op },
} = require('../models')
const client = require('../utils/redis')
const { mailer } = require('../utils/mailer')
const {
  structureTotalSales,
  structuringServiceData,
  unPaidSubscriptionHandler,
  generateStatistics,
} = require('./common')
const { verifyToken, generateToken } = require('../utils/token-handler')
const { searchDataIndex } = require('../utils/elasticsearch')

const uuid = require('uuid')
const bcrypt = require('bcryptjs')
const password_generator = require('generate-password')
const { generateRedisString } = require('../utils/string-generator')

const startSession = async (person, res) => {
  const uuidv4 = uuid.v4()
  client.hmset(uuidv4, generateRedisString(person), (err, response) => {
    if (err) throw new Error(err.message)
    else if (response)
      return res.json({
        token: generateToken(uuidv4),
        person,
      })
  })
}

exports.signIn = async (req, res, next) => {
  const { email, password } = req.body
  try {
    const user_data = await PersonHasRole.findOne({
      attributes: ['password', 'role_id', 'person_has_role_id'],
      include: {
        model: Person,
        where: { email },
        attributes: {
          exclude: ['created_at', 'bonus_acquired'],
          include: [['bonus_acquired', 'sya_amount']],
        },
      },
    })
    if (user_data && bcrypt.compareSync(password, user_data.password)) {
      const {
        person_has_role_id,
        Person: {
          national_id_number,
          date_of_birth,
          person_id,
          first_name,
          last_name,
          email,
          phone,
          gender,
          dataValues: { sya_amount },
        },
        role_id,
      } = user_data
      const session = {
        email,
        role_id,
        person_id,
        first_name,
        last_name,
        sya_amount,
        phone: phone ?? '',
        gender: gender ?? '',
        date_of_birth: date_of_birth ?? '',
        national_id_number: national_id_number ?? '',
        referral_link: `https://seeyouagain.fun/sign-up/${person_id}`,
      }

      const { log_id } = await Log.create({
        person_has_role_id,
      })
      switch (role_id) {
        case process.env.OWNER_ID: {
          const personnel = await StructureHasPersonnel.findOne({
            attributes: ['structure_has_personnel_id', 'structure_id'],
            where: { person_has_role_id, is_active: true },
          })
          if (personnel) {
            return await startSession(
              {
                log_id,
                ...session,
                is_owner: true,
                is_admin: false,
                person_has_role_id,
                user_id: personnel.structure_has_personnel_id,
              },
              res
            )
          } else throw new Error('Unknown onwer account')
        }
        case process.env.ADMIN_ID:
          return await startSession(
            {
              log_id,
              ...session,
              is_admin: true,
              is_owner: false,
              user_id: person_has_role_id,
            },
            res
          )
        case process.env.CLIENT_ID:
          if (req.url === '/sign-in/purchase') {
            res.user_data = {
              ...session,
              is_admin: false,
              is_owner: false,
              user_id: person_has_role_id,
            }
            return next()
          } else {
            return await startSession(
              {
                log_id,
                ...session,
                is_admin: false,
                is_owner: false,
                user_id: person_has_role_id,
              },
              res
            )
          }
        default:
          throw new Error('Unhandled role_id found attached to this account')
      }
    } else throw new Error('incorrect email or password !')
  } catch (error) {
    return res.status(500).send({
      message:
        error.message ||
        'Something when wrong, please make sure you have not provided  an empty request body',
    })
  }
}

exports.setActiveStructures = (req, res) => {
  const { structure_id } = req.body
  try {
    const { session_id, new_token, error } = verifyToken(
      req.headers.sya_token,
      process.env.JWT_SECRET
    )
    if (error) throw new Error(error.message)
    client.hgetall(session_id, (err, session) => {
      if (session && session.role_id === process.env.OWNER_ID) {
        client.hmset(session_id, ['structure_id', structure_id])
        res.new_token = new_token
        return res.json({
          is_structure_activated: true,
        })
      } else
        return res.status(500).send({
          message: err?.message || `Access denied for current user`,
        })
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.sendResetPasswordLink = async (req, res) => {
  const { email } = req.body
  const uuidv4 = uuid.v4()
  const expires_at = new Date(new Date().getTime() + 2 * 60 * 60 * 1000)

  try {
    if (!email) throw new Error('cannot send email to an unknwon email')
    const person = await PersonHasRole.findOne({
      attributes: ['person_has_role_id'],
      include: {
        model: Person,
        where: { email },
        attributes: ['last_name'],
      },
    })
    if (!person)
      throw new Error('The given email has not been found in our database')
    const {
      person_has_role_id,
      Person: { last_name },
    } = person
    const reset = await ResetPassword.create({
      link: uuidv4,
      expires_at,
      person_has_role_id,
    })
    if (!reset) throw new Error('Unable to start password reset')
    mailer([email], 'SYA SECURE', '../views/PasswordResetRequest.hbs', {
      last_name,
      reset_password_link: `https://seeyouagain.fun/reset-password/${uuidv4}/${email}`,
    })
      .then((info) => console.log(info))
      .catch((error) => console.log(error))
    return res.json({
      is_reset_password_link_send: true,
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Somethime whent wrong',
    })
  }
}

exports.setNewPassword = async (req, res) => {
  const { new_password } = req.body
  const { link, email } = req.params

  try {
    if (!email || !link || !new_password)
      throw new Error(
        'Reset password link informations and new password is required'
      )
    const reset = await ResetPassword.findOne({
      attributes: ['reset_password_id'],
      where: { link, is_used: false },
    })
    if (!reset) throw new Error('Ooops, invalid user link')
    const person_has_role = await PersonHasRole.findOne({
      attributes: ['password', 'person_has_role_id', 'role_id'],
      include: {
        model: Person,
        attributes: ['person_id', 'last_name'],
        where: { email },
      },
    })
    if (!person_has_role) throw new Error('Ooops, Unidentified user email')
    const {
      role_id,
      password,
      person_has_role_id,
      Person: { last_name },
    } = person_has_role
    return sequelize.transaction(async (transaction) => {
      try {
        await PersonHasRoleAudit.create(
          {
            role_id,
            password,
            person_has_role_id,
          },
          { transaction }
        )
        const hash_password = bcrypt.hashSync(
          new_password,
          parseInt(process.env.SALT)
        )
        await ResetPassword.update(
          {
            is_used: true,
          },
          {
            where: { is_used: false, link },
            transaction,
          }
        )
        await PersonHasRole.update(
          {
            password: hash_password,
          },
          {
            where: { person_has_role_id },
            transaction,
          }
        )
        mailer([email], 'SYA SECURE', '../views/PasswordResetNotice.hbs', {
          last_name,
        })
          .then((info) => console.log(info))
          .catch((error) => console.log(error))
        return res.json({ is_password_changed: true })
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry, Somethime whent wrong',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Somethime whent wrong',
    })
  }
}

const createStructure = async (structure_info, created_by, transaction) => {
  try {
    const structure = await Structure.create(
      {
        ...structure_info,
        created_by,
      },
      { transaction }
    )
    const { structure_id } = structure
    const demand = await Demand.create(
      {
        requested_by: created_by,
        structure_id,
      },
      { transaction }
    )
    return { ...demand.dataValues, ...structure.dataValues }
  } catch (error) {
    throw new Error(error.message)
  }
}

const createOwner = async (structure_id, person_has_role_id, transaction) => {
  try {
    const uuidv4 = uuid.v4()
    const owner = await StructureHasPersonnel.create(
      {
        structure_has_personnel_id: uuidv4,
        person_has_role_id,
        created_by: uuidv4,
        structure_id,
      },
      { transaction }
    )
    return owner
  } catch (error) {
    throw new Error(error.message)
  }
}

const createownerProfile = async ({ password, ...owner_info }, transaction) => {
  try {
    const { person_id } = await Person.create(
      {
        ...owner_info,
      },
      { transaction }
    )
    const generated_password =
      password ||
      password_generator.generate({
        length: 8,
        lowercase: true,
        numbers: true,
      })
    const person_has_role = await PersonHasRole.create(
      {
        person_id,
        role_id: process.env.OWNER_ID,
        password: bcrypt.hashSync(
          generated_password,
          parseInt(process.env.SALT)
        ),
      },
      { transaction }
    )
    const setting = await BonusSetting.findOne({
      attributes: ['sign_up_bonus'],
    })
    mailer([owner_info.email], 'SYA SIGN UP', '../views/AccountCreation.hbs', {
      sign_up_bonus: setting?.sign_up_bonus || 0,
      referral_bonus: setting?.referal_bonus || 0,
      referral_link: `https://seeyouagain.fun/sign-up/${person_id}`,
      more_message: `this is your generated password: ${generated_password}.\n Please don't forget to change as soon as possible`,
    })
      .then((info) => console.log(info))
      .catch((error) => console.log(error))
    return person_has_role
  } catch (error) {
    throw new Error(error.message)
  }
}

const createdOnwerStructure = async (
  structure_info,
  created_by,
  transaction
) => {
  try {
    const structure = await createStructure(
      structure_info,
      created_by,
      transaction
    )
    const { structure_id } = structure
    return await createOwner(structure_id, created_by, transaction)
  } catch (error) {
    throw new Error(error.message)
  }
}

exports.demandStructureCreation = (req, res) => {
  const { structure_info, owner_info } = JSON.parse(req.body.data)
  const logo_ref = req.file?.filename
  try {
    if (!structure_info || !owner_info)
      throw new Error(
        'Cannot demand for structure without structure_info and onwer_info'
      )
    const { name, longitude, latitude, specific_indications, category_id } =
      structure_info
    const {
      first_name,
      last_name,
      email,
      phone,
      password,
      date_of_birth,
      gender,
      national_id_number,
    } = owner_info
    if (
      !name ||
      !password ||
      !longitude ||
      !latitude ||
      !specific_indications ||
      !category_id ||
      !logo_ref
    )
      throw new Error(
        'the following informations are required for structure creation: name, longitude, latitude, specific_indications, category_id and struture_image_ref'
      )
    if (
      !first_name ||
      !last_name ||
      !email ||
      !phone ||
      !date_of_birth ||
      !gender ||
      !national_id_number
    )
      throw new Error(
        'the following informations are required for structure creation: first_name, last_name, email, password, phone, date_of_birth, gender, nation_id_number'
      )
    return sequelize.transaction(async (transaction) => {
      try {
        const person = await PersonHasRole.findOne({
          attributes: ['person_has_role_id', 'role_id', 'password'],
          include: {
            model: Person,
            where: { email },
            attributes: { exclude: ['created_at'] },
          },
          transaction,
        })
        let person_has_role_id = null
        if (person) {
          person_has_role_id = person.person_has_role_id
          const {
            Person: { date_of_birth, gender, phone },
            role_id,
            password,
          } = person
          if (role_id === process.env.CLIENT_ID) {
            await PersonHasRoleAudit.create(
              {
                role_id,
                password,
                person_has_role_id,
              },
              { transaction }
            )
            await PersonHasRole.update(
              { role_id: process.env.OWNER_ID },
              { where: { person_has_role_id }, transaction }
            )
          }
          if (
            date_of_birth !== owner_info.date_of_birth ||
            gender !== owner_info.gender ||
            phone !== owner_info.phone
          ) {
            await PersonAudit.create(person.Person.dataValues, { transaction })
            await Person.update(owner_info, { where: { email }, transaction })
          }
        } else {
          const owner_profile = await createownerProfile(
            owner_info,
            transaction
          )
          if (owner_profile)
            person_has_role_id = owner_profile.person_has_role_id
          else throw new Error("API crash Profile wasn't created")
        }
        const { structure_has_personnel_id: added_by, structure_id } =
          await createdOnwerStructure(
            { ...structure_info, logo_ref },
            person_has_role_id,
            transaction
          )
        await StructureHasCategory.create(
          {
            added_by,
            structure_id,
            structure_category_id: category_id,
          },
          { transaction }
        )
        await StructureHasSubscription.create(
          {
            subscription_id: process.env.SUBSCRIPTION_1,
            subscription_amount: 0,
            structure_id,
          },
          { transaction }
        )
        mailer(
          [email],
          'SYA Structure Creation',
          '../views/StructureCreationNotice.hbs',
          {}
        )
          .then((info) => console.log(info))
          .catch((error) => console.log(error))
        return res.json({ is_struture_created: true })
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry, Somethime whent wrong',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Somethime whent wrong',
    })
  }
}

exports.getStructureCategories = async (req, res) => {
  try {
    const structure_categories = await StructureCategory.findAll({
      attributes: [['structure_category_id', 'category_id'], 'category_name'],
      where: { is_deleted: false },
    })
    return res.json({ structure_categories })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Somethime whent wrong',
    })
  }
}

exports.userLogOut = (req, res) => {
  try {
    const { session_id, error } = verifyToken(
      req.headers.sya_token,
      process.env.JWT_SECRET
    )
    if (error) throw new Error(error.message)
    client.hgetall(session_id, async (err, session) => {
      try {
        if (err) throw new Error(err.message)
        client.del(session.log_id)
        await Log.update(
          {
            logged_out_at: new Date(),
          },
          { where: { log_id: session.log_id } }
        )
        return res.json({
          is_user_logged_out: true,
        })
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry, Somethime whent wrong',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Somethime whent wrong',
    })
  }
}

exports.getStructures = async (req, res) => {
  const {
    category_id: structure_category_id,
    longitude,
    latitude,
    rows_per_page,
    page_number,
    meal,
    budget,
    number_of_persons,
    structure_id,
  } = req.query
  const query_condition =
    longitude && latitude
      ? {
          is_valid: true,
          is_deleted: false,
          is_disabled: false,
          [Op.or]: [
            { latitude: { [Op.lte]: latitude + process.env.RADIUS } },
            { latitude: { [Op.gte]: latitude - process.env.RADIUS } },
          ],
          [Op.or]: [
            { longitude: { [Op.lte]: latitude + process.env.RADIUS } },
            { longitude: { [Op.gte]: latitude - process.env.RADIUS } },
          ],
        }
      : { is_deleted: false, is_valid: true, is_disabled: false }
  try {
    const offsets =
      page_number && rows_per_page
        ? {
            offset: rows_per_page * page_number,
            limit: rows_per_page * (page_number + 1),
          }
        : {}
    const structure_data = await Structure.findAll({
      attributes: [
        'structure_id',
        'name',
        'longitude',
        'latitude',
        'logo_ref',
        'specific_indications',
        'is_deleted',
      ],
      ...offsets,
      where: query_condition,
      include: [
        {
          model: StructureHasCategory,
          attributes: ['structure_has_category_id'],
          where: structure_id
            ? {
                is_category_active: true,
                structure_id: { [Op.ne]: structure_id },
              }
            : { is_category_active: true },
          include: {
            model: StructureCategory,
            attributes: ['structure_category_id'],
            where:
              structure_category_id && structure_category_id !== 'sy-structures'
                ? { structure_category_id, is_deleted: false }
                : { is_deleted: false },
          },
        },
        {
          model: Service,
          where: meal
            ? { is_deleted: false, name: { [Op.substring]: meal } }
            : { is_deleted: false },
          attributes: ['rating', 'unit_price', 'service_id'],
          include: [
            {
              model: CatalogDetail,
              attributes: [
                'is_reservable',
                'is_take_away',
                'is_home_delivery',
                'is_payable',
              ],
            },
          ],
        },
      ],
    })
    const structures = []
    for (let k = 0; k < structure_data.length; k++) {
      const {
        name,
        latitude,
        logo_ref,
        Services,
        longitude,
        structure_id,
        specific_indications,
        StructureHasCategories: categories,
      } = structure_data[k]
      let category_id = null
      categories.forEach(({ StructureCategory: { structure_category_id } }) => {
        if (structure_category_id) category_id = structure_category_id
      })
      if (Services && Services.length > 0) {
        const struct = {
          name,
          latitude,
          longitude,
          category_id,
          structure_id,
          specific_indications,
          ...(await structuringServiceData(Services)),
          logo_ref: `${process.env.MULTER_FOLDER}/${logo_ref}`,
        }
        if (!budget) structures.push(struct)
        else if (budget / number_of_persons >= struct.average_price)
          structures.push(struct)
        else if (
          structure_category_id === 'sy-structures' &&
          struct.is_on_promotion
        )
          structures.push(struct)
      } else
        structures.push({
          name,
          latitude,
          longitude,
          rating: 0,
          structure_id,
          average_price: 0,
          specific_indications,
          promotion_percentage: 0,
          is_on_promotion: false,
          is_structure_sy: false,
          is_reservable: false,
          is_take_away: false,
          is_payable: false,
          is_home_delivery: false,
          category_id: structure_category_id,
          logo_ref: `${process.env.MULTER_FOLDER}/${logo_ref}`,
        })
    }
    const count = await Structure.findAll({
      where: { is_deleted: false },
    })
    return res.json({
      structures: structures,
      number_of_structures: count.length,
    })
  } catch (error) {
    return res.status(500).send({
      message:
        error.message ||
        'Sorry an unexpected error occured. Please contact your system administrator',
    })
  }
}

exports.getStructureInfo = async (req, res) => {
  const { structure_id } = req.query

  try {
    if (!structure_id) throw new Error('structure id is required')
    const structure_data = await Structure.findOne({
      attributes: [
        'name',
        'longitude',
        'latitude',
        'logo_ref',
        'description',
        'specific_indications',
      ],
      where: { is_deleted: false, is_valid: true, structure_id },
      include: [
        {
          model: PersonHasRole,
          where: { is_deleted: false },
          attributes: ['person_has_role_id'],
          include: {
            model: Person,
            attributes: ['phone', 'last_name', 'first_name'],
          },
        },
        {
          model: StructureHasCategory,
          attributes: ['structure_has_category_id'],
          where: { is_category_active: true },
          include: {
            model: StructureCategory,
            attributes: ['structure_category_id'],
            where: { is_deleted: false },
          },
        },
        {
          model: Service,
          where: { is_deleted: false },
          attributes: ['service_id', 'rating', 'unit_price'],
          include: [
            {
              model: CatalogDetail,
              attributes: [
                'is_reservable',
                'is_take_away',
                'is_home_delivery',
                'is_payable',
              ],
            },
          ],
        },
      ],
    })
    if (structure_data) {
      const {
        name,
        longitude,
        latitude,
        logo_ref,
        specific_indications,
        description,
        StructureHasCategories: categories,
        Services,
        PersonHasRole: {
          Person: { first_name, last_name, phone },
        },
      } = structure_data
      let category_id = null
      categories.forEach(({ StructureCategory: { structure_category_id } }) => {
        if (structure_category_id) category_id = structure_category_id
      })
      if (Services && Services.length > 0) {
        return res.json({
          structure: {
            name,
            latitude,
            longitude,
            category_id,
            description,
            structure_id,
            owner_number: phone,
            specific_indications,
            owner_name: `${first_name} ${last_name}`,
            ...(await structuringServiceData(Services)),
            logo_ref: `${process.env.MULTER_FOLDER}/${logo_ref}`,
          },
        })
      } else
        return res.json({
          structure: {
            name,
            latitude,
            longitude,
            description,
            category_id,
            structure_id,
            owner_number: phone,
            specific_indications,
            owner_name: `${first_name} ${last_name}`,
            logo_ref: `${process.env.MULTER_FOLDER}/${logo_ref}`,
          },
        })
    } else
      throw new Error('No corresponding structure informations where found')
  } catch (error) {
    return res.status(500).send({
      message:
        error.message ||
        'Sorry an unexpected error occured. Please contact your system administrator',
    })
  }
}

const scheduleService = (structure_id) => {
  return Schedule.findAll({
    attributes: {
      exclude: ['created_at', 'created_by', 'structure_id'],
    },
    where: { structure_id, is_deleted: false },
    order: [['day', 'ASC']],
  })
}
exports.findStructureSchedules = async (req, res) => {
  const { structure_id: selected_structure_id } = req.query
  try {
    const sya_token = req.headers.sya_token
    if (sya_token) {
      const { session_id, new_token, error } = verifyToken(
        sya_token,
        process.env.JWT_SECRET
      )
      if (error) throw new Error(error.message)
      client.hgetall(session_id, async (err, session) => {
        try {
          res.new_token = new_token
          if (err) throw new Error(err.message)
          const structure_id = selected_structure_id ?? session.structure_id
          if (!structure_id) throw new Error('A structure needs to be selected')
          return res.json({
            schedules: await scheduleService(structure_id),
          })
        } catch (error) {
          return res.status(500).send({
            message: error.message || 'Sorry, Something went wrong',
          })
        }
      })
    } else {
      if (!selected_structure_id)
        throw new Error('A structure needs to be selected')
      return res.json({
        schedules: await scheduleService(selected_structure_id),
      })
    }
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

const publicationService = async (structure_id) => {
  const publications = await Publication.findAll({
    attributes: ['publication_id', 'publication_ref'],
    where: { structure_id, is_deleted: false },
  })
  return publications.map(({ publication_ref, publication_id }) => ({
    publication_id,
    publication_ref: `${process.env.MULTER_FOLDER}/${publication_ref}`,
  }))
}
exports.fetchPublications = async (req, res) => {
  const { structure_id: selected_structure_id } = req.query

  try {
    const sya_token = req.headers.sya_token
    if (sya_token) {
      const { session_id, new_token, error } = verifyToken(
        sya_token,
        process.env.JWT_SECRET
      )
      if (error) throw new Error(error.message)
      client.hgetall(session_id, async (err, session) => {
        try {
          res.new_token = new_token
          if (err) throw new Error(err.message)
          if (!selected_structure_id && !session.structure_id)
            throw new Error('A structure needs to be selected')
          const structure_id = selected_structure_id ?? session.structure_id
          return res.json({
            publications: await publicationService(structure_id),
          })
        } catch (error) {
          return res.status(500).send({
            message: error.message || 'Sorry, Something went wrong',
          })
        }
      })
    } else {
      if (!selected_structure_id)
        throw new Error('A structure needs to be selected')
      return res.json({
        publications: await publicationService(selected_structure_id),
      })
    }
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.findCatalogServices = async (req, res) => {
  const { catalog_id, structure_id, date } = req.query

  try {
    // if (!catalog_id && !structure_id)
    //   throw new Error('catalog id or structure_id is required')
    const services = await CatalogDetail.findAll({
      attributes: [
        'is_reservable',
        'is_take_away',
        'is_home_delivery',
        'is_payable',
        'cancellation_delay',
        'quantity',
      ],
      where: { is_deleted: false },
      include: [
        {
          model: Catalog,
          attributes: ['catalog_id', 'starts_at', 'ends_at'],
          where: {
            ...(catalog_id
              ? { catalog_id }
              : structure_id
              ? { structure_id }
              : {}),
            is_deleted: false,
            ends_at: { [Op.gte]: date ? new Date(date) : new Date() },
          },
        },
        {
          model: Service,
          attributes: [
            'name',
            'rating',
            'service_id',
            'unit_price',
            'description',
            'main_image_ref',
          ],
          where: { is_deleted: false },
          include: {
            model: Structure,
            attributes: ['structure_id'],
            where: catalog_id
              ? { is_disabled: false }
              : { is_deleted: false, is_valid: true, is_disabled: false },
            include: {
              model: StructureHasCategory,
              attributes: ['structure_category_id'],
            },
          },
        },
      ],
    })
    const promotions = await PromotionHasService.findAll({
      where: { is_deleted: false },
      attributes: ['service_id'],
      include: {
        model: Promotion,
        attributes: [
          'value',
          'promotion_id',
          'promotion_type',
          'purchasing_bonus',
        ],
        where: { is_deleted: false },
      },
    })
    const service_images = await ServiceImage.findAll({
      where: { is_deleted: false },
      attributes: ['service_image_id', 'service_image_ref', 'service_id'],
    })

    let catalog = {}
    const catalog_services = []
    for (let i = 0; i < services.length; i++) {
      const {
        is_reservable,
        is_take_away,
        is_home_delivery,
        is_payable,
        cancellation_delay,
        quantity,
        Catalog: { catalog_id, starts_at, ends_at },
        Service: {
          rating,
          service_id,
          Structure: { structure_id, StructureHasCategories },
          unit_price,
          description,
          main_image_ref,
          name: service_name,
        },
      } = services[i]

      let service_promotions = []
      promotions.forEach((promotion) => {
        if (promotion.service_id === service_id) {
          const { promotion_id, promotion_type, value, purchasing_bonus } =
            promotion.Promotion
          service_promotions.push({
            value,
            promotion_id,
            promotion_type,
            purchasing_bonus,
          })
        }
      })
      catalog = { catalog_id, starts_at, ends_at }
      if (
        catalog_services.length === 0 ||
        !catalog_services.find(({ service_id: id }) => id === service_id)
      )
        catalog_services.push({
          rating,
          quantity,
          unit_price,
          service_id,
          is_payable,
          description,
          structure_id,
          is_take_away,
          service_name,
          is_reservable,
          is_home_delivery,
          cancellation_delay,
          promotions: service_promotions,
          category_id: StructureHasCategories[0].structure_category_id,
          main_image_ref: `${process.env.MULTER_FOLDER}/${main_image_ref}`,
          service_images: service_images
            .filter((_) => _.service_id === service_id)
            .map(({ service_image_ref, service_image_id }) => ({
              service_image_ref: `${process.env.MULTER_FOLDER}/${service_image_ref}`,
              service_image_id,
            })),
        })
    }
    return res.json({
      catalog_services,
      catalog:
        !catalog.starts_at && catalog_id
          ? await Catalog.findOne({
              attributes: ['starts_at', 'ends_at'],
              where: {
                catalog_id,
                [Op.or]: [
                  {
                    ends_at: { [Op.gte]: new Date() },
                    starts_at: { [Op.lte]: new Date() },
                  },
                  {
                    starts_at: { [Op.gte]: new Date() },
                  },
                ],
              },
            })
          : catalog,
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.findUserProfile = (req, res) => {
  try {
    const sya_token = req.headers.sya_token
    if (!sya_token || sya_token === '') throw new Error('jwt must be provided')
    const { session_id, error } = verifyToken(sya_token, process.env.JWT_SECRET)
    if (error)
      return res.status(500).send({
        message: error.message,
      })
    client.hgetall(session_id, (err, session) => {
      try {
        if (err || !session)
          return res.status(500).send({
            message: err?.message || 'Session not found',
          })
        return res.json({
          profile: {
            ...session,
            is_owner: session.is_owner === 'true',
            is_admin: session.is_admin === 'true',
          },
        })
      } catch (error) {
        return res.status(500).send({
          message: error.message,
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.getCodeValuePercentage = async (req, res) => {
  const { code_value } = req.query
  try {
    if (!code_value) throw new Error('code value is required')
    const promotion_code = await PromotionCode.findOne({
      attributes: ['promotion_code_id', 'percentage'],
      where: { code_value, is_used: false },
    })
    return res.json({ promotion_code })
  } catch (error) {
    return res.status(500).send({
      message:
        error.message ||
        'Sorry an unexpected error occured. Please contact your system administrator',
    })
  }
}

exports.updateProfile = async (req, res) => {
  const { email, phone, first_name, last_name, gender, date_of_birth } =
    JSON.parse(req.body.data)
  const { person_id } = req.params

  try {
    return sequelize.transaction(async (transaction) => {
      try {
        const person_image_ref = req.file
        if (
          !person_id ||
          (!first_name &&
            !last_name &&
            !email &&
            !phone &&
            !date_of_birth &&
            !gender)
        )
          throw new Error(
            'person_id and update data most be provided(phone date_of_birth, gender, and email) are required'
          )
        const person = await Person.findOne({
          where: { person_id },
          attributes: { exclude: ['created_at'] },
          transaction,
        })
        if (person) {
          await PersonAudit.create(
            {
              person_id,
              ...person,
            },
            { transaction }
          )
          await Person.update(
            {
              person_image_ref:
                person_image_ref?.filename ?? person.person_image_ref,
              date_of_birth: date_of_birth ?? person.date_of_birth,
              first_name: first_name ?? person.first_name,
              last_name: last_name ?? person.last_name,
              gender: gender ?? person.gender,
              email: email ?? person.email,
              phone: phone ?? person.phone,
            },
            {
              where: { person_id },
              transaction,
            }
          )
          return res.json({ is_person_updated: true })
        } else throw new Error('Update Failed')
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry an unknown error occured',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.getSalesStatistics = async (req, res) => {
  const { structure_id: struct_id, interval, ends_at, starts_at } = req.query
  try {
    if (!interval) throw new Error('interval_duration is required')
    const { session_id, new_token, error } = verifyToken(
      req.headers.sya_token,
      process.env.JWT_SECRET
    )
    if (error) throw new Error(error.message)
    client.hgetall(session_id, async (err, session) => {
      try {
        if (!err && !session)
          return res.status(500).send({ message: 'Session not found' })
        if (
          session &&
          (session.role_id === process.env.ADMIN_ID ||
            session.role_id === process.env.OWNER_ID)
        ) {
          res.new_token = new_token
          const structure_id = session.structure_id ?? struct_id
          const order_sales = await Order.findAll({
            attributes: ['ordered_at', 'order_id'],
            order: [['ordered_at', 'ASC']],
            where:
              starts_at && ends_at && starts_at !== '' && ends_at !== ''
                ? {
                    ordered_at: {
                      [Op.and]: {
                        [Op.gte]: parseInt(starts_at),
                        [Op.lte]: parseInt(ends_at),
                      },
                    },
                  }
                : starts_at && starts_at !== ''
                ? { ordered_at: { [Op.gte]: parseInt(starts_at) } }
                : ends_at !== ''
                ? { ordered_at: { [Op.lte]: parseInt(ends_at) } }
                : {},
            include: {
              model: OrderContent,
              required: true,
              attributes: ['order_content_id'],
              include: {
                model: Service,
                where: structure_id ? { structure_id } : {},
              },
            },
          })
          return res.json({
            sales_statistics:
              order_sales.length > 0
                ? generateStatistics(order_sales, parseInt(interval))
                : [],
          })
        } else throw new Error(err?.message || 'Access denied for current user')
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry, Something went wrong',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.fetchSubscriptions = async (req, res) => {
  const { subscription_id } = req.query
  try {
    return res.json({
      subscriptions: subscription_id
        ? await Subscription.findOne({
            where: { is_deleted: false },
            attributes: [
              'subscription_id',
              'subscription_type',
              'commission_rate',
            ],
          })
        : await Subscription.findAll({
            where: { is_deleted: false },
            attributes: [
              'subscription_id',
              'subscription_type',
              'commission_rate',
            ],
          }),
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

const catalogService = async (structure_id, service_id, use_today) => {
  const catalogs = await Catalog.findAll({
    where: use_today
      ? {
          structure_id,
          is_deleted: false,
          ends_at: { [Op.gt]: new Date() },
        }
      : {
          structure_id,
          is_deleted: false,
        },
    attributes: ['starts_at', 'ends_at', 'catalog_id', 'created_at'],
    ...(service_id
      ? {
          include: {
            model: CatalogDetail,
            attributes: ['catalog_detail_id'],
            where: { service_id, is_deleted: false },
          },
        }
      : {}),
  })
  return catalogs.map(({ dataValues: { CatalogDetails, ...catalog } }) => {
    return {
      ...catalog,
      catalog_detail_id: CatalogDetails
        ? CatalogDetails[0].catalog_detail_id
        : null,
    }
  })
}
exports.findAllCatalogs = async (req, res) => {
  const {
    use_today,
    service_id,
    structure_id: selected_structure_id,
  } = req.query

  try {
    const sya_token = req.headers.sya_token
    if (sya_token) {
      const { session_id, new_token, error } = verifyToken(
        sya_token,
        process.env.JWT_SECRET
      )
      if (error) throw new Error(error.message)
      client.hgetall(session_id, async (err, session) => {
        try {
          res.new_token = new_token
          if (err) throw new Error(err.message)
          const structure_id = selected_structure_id ?? session.structure_id
          if (!structure_id) throw new Error('A structure needs to be selected')
          return res.json({
            catalogs: await catalogService(structure_id, service_id, use_today),
          })
        } catch (error) {
          return res.status(500).send({
            message: error.message || 'Sorry something went wrong',
          })
        }
      })
    } else {
      if (!selected_structure_id) throw new Error('structure_id is required')
      return res.json({
        catalogs: await catalogService(
          selected_structure_id,
          service_id,
          use_today
        ),
      })
    }
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.getSubscriptionsTracker = async (req, res) => {
  const { selected_date, structure_id: struct_id } = req.query
  try {
    const { session_id, new_token, error } = verifyToken(
      req.headers.sya_token,
      process.env.JWT_SECRET
    )
    if (error) throw new Error(error.message)
    client.hgetall(session_id, async (err, session) => {
      if (
        session &&
        (session.role_id === process.env.ADMIN_ID ||
          session.role_id === process.env.OWNER_ID)
      ) {
        res.new_token = new_token
        const structure_id = session.structure_id ?? struct_id
        const subs = await SubscriptionPayment.findAll({
          attributes: [
            'paid_at',
            'is_paid',
            'reference',
            'commission',
            'total_period_revenue',
            'subscription_payment_id',
          ],
          include: {
            required: true,
            model: StructureHasSubscription,
            attributes: ['started_at', 'expired_at'],
            where:
              structure_id && selected_date
                ? {
                    structure_id,
                    started_at: { [Op.gte]: new Date(selected_date) },
                  }
                : structure_id
                ? { structure_id }
                : selected_date
                ? { started_at: { [Op.gte]: new Date(selected_date) } }
                : {},
            include: {
              model: Structure,
              attributes: ['name', 'structure_id'],
            },
          },
        })
        let total_commission_paid = 0
        let subscriptions = subs.map(
          ({
            is_paid,
            paid_at,
            reference,
            commission,
            payment_method,
            commission_rate,
            total_period_revenue,
            subscription_payment_id,
            StructureHasSubscription: {
              started_at,
              expired_at,
              Structure: { name: structure_name, structure_id },
            },
          }) => {
            total_period_revenue += commission
            total_commission_paid += commission_rate
            return {
              is_paid,
              paid_at,
              reference,
              commission,
              started_at,
              expired_at,
              structure_id,
              structure_name,
              payment_method,
              commission_rate,
              total_period_revenue,
              subscription_payment_id,
            }
          }
        )
        if (structure_id) {
          subscriptions.push({
            ...(await unPaidSubscriptionHandler(structure_id, res)),
          })
        }

        return res.json({
          statistics: {
            total_system_revenue: await structureTotalSales(
              structure_id,
              res,
              true
            ),
            total_commission_paid,
          },
          subscriptions,
        })
      } else
        return res.status(500).send({
          message: err?.message || `Access denied for current user`,
        })
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.getActiveSubscription = async (req, res) => {
  const { subscription_payment_id, structure_id } = req.query
  try {
    if (!subscription_payment_id && !structure_id)
      throw new Error('subscription_payment_id or structure_id is required')
    const { session_id, new_token, error } = verifyToken(
      req.headers.sya_token,
      process.env.JWT_SECRET
    )
    if (error) throw new Error(error.message)
    client.hgetall(session_id, async (err, session) => {
      try {
        if (
          session &&
          (session.role_id === process.env.ADMIN_ID ||
            session.role_id === process.env.OWNER_ID)
        ) {
          res.new_token = new_token
          if (subscription_payment_id) {
            const subscription = await SubscriptionPayment.findAll({
              attributes: [
                'paid_at',
                'is_paid',
                'reference',
                'commission',
                'payment_method',
                'commission_rate',
                'total_period_revenue',
              ],
              where: { subscription_payment_id },
              include: {
                model: StructureHasSubscription,
                attributes: ['started_at', 'expired_at'],
                include: {
                  model: Structure,
                  attributes: ['name', 'structure_id'],
                },
              },
            })
            if (!subscription) throw new Error('Subscription not found')
            const {
              is_paid,
              paid_at,
              reference,
              commission,
              payment_method,
              commission_rate,
              total_period_revenue,
              StructureHasSubscription: {
                started_at,
                expired_at,
                Structure: { name: structure_name, structure_id },
              },
            } = subscription
            return res.json({
              active_subscription: {
                is_paid,
                paid_at,
                reference,
                commission,
                started_at,
                expired_at,
                structure_id,
                structure_name,
                payment_method,
                commission_rate,
                total_period_revenue,
                subscription_payment_id,
              },
            })
          } else if (structure_id) {
            return res.json({
              active_subscription: await unPaidSubscriptionHandler(
                structure_id,
                res
              ),
            })
          }
        } else
          return res.status(500).send({
            message: err?.message || `Access denied for current user`,
          })
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry, Something went wrong',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.getBonusStatistics = async (req, res) => {
  const { promotion_id, structure_id } = req.query

  try {
    const { session_id, new_token, error } = verifyToken(
      req.headers.sya_token,
      process.env.JWT_SECRET
    )
    if (error) throw new Error(error.message)
    client.hgetall(session_id, async (err, session) => {
      try {
        if (!err && !session)
          return res.status(500).send({ message: 'Session not found' })
        if (
          session &&
          (session.role_id === process.env.ADMIN_ID ||
            session.role_id === process.env.OWNER_ID)
        ) {
          res.new_token = new_token
          const { structure_id: selected_structure_id } = session

          if (!structure_id && !selected_structure_id)
            return res.status(500).send({
              message: 'structure_id is required',
            })
          const statistics = await BonusSubscription.findOne({
            attributes: [
              [
                Sequelize.fn('SUM', Sequelize.col('quantity_cashed_in')),
                'total_cashed_in_bonus',
              ],
              [
                Sequelize.fn('SUM', Sequelize.col('quantity_cashed_out')),
                'total_cashed_out_bonus',
              ],
              [
                Sequelize.fn('SUM', Sequelize.col('quantity_remaining')),
                'total_remaining_bonus',
              ],
            ],
            where: promotion_id ? { promotion_id } : {},
            include: {
              model: Promotion,
              attributes: ['structure_id'],
              where: { structure_id: structure_id ?? selected_structure_id },
            },
          })
          return res.json({
            bonus_statistics: statistics
              ? {
                  total_cashed_in_bonus: parseInt(
                    statistics.dataValues.total_cashed_in_bonus
                  ),
                  total_cashed_out_bonus: parseInt(
                    statistics.dataValues.total_cashed_out_bonus
                  ),
                  total_remaining_bonus: parseInt(
                    statistics.dataValues.total_remaining_bonus
                  ),
                }
              : {
                  total_cashed_in_bonus: 0,
                  total_cashed_out_bonus: 0,
                  total_remaining_bonus: 0,
                },
          })
        } else
          return res.status(500).send({
            message: err?.message || `Access denied for current user`,
          })
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry, Something went wrong',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.getFeedbacks = async (req, res) => {
  const { structure_id } = req.query
  try {
    if (!structure_id) throw new Error('structure_id is required')
    const feedbacks = await Feedback.findAll({
      attributes: ['feedback_id', 'rating', 'feedback', 'created_at'],
      where: { is_visible: true },
      order: [['created_at', 'DESC']],
      include: [
        {
          model: OrderContent,
          required: true,
          attributes: ['order_content_id'],
          include: {
            model: Service,
            attributes: ['service_id'],
            where: { structure_id, is_deleted: false },
          },
        },
        {
          model: PersonHasRole,
          where: { is_deleted: false },
          attributes: ['person_has_role_id'],
          include: {
            model: Person,
            attributes: ['first_name', 'last_name'],
          },
        },
      ],
    })
    return res.json({
      feedbacks: feedbacks.map(
        ({
          feedback_id,
          created_at,
          rating,
          feedback,
          PersonHasRole: {
            Person: { first_name, last_name },
          },
        }) => ({
          rating,
          feedback,
          feedback_id,
          given_at: created_at,
          given_by: `${first_name} ${last_name}`,
        })
      ),
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.findOnwerStructures = async (req, res) => {
  try {
    const { session_id, new_token, error } = verifyToken(
      req.headers.sya_token,
      process.env.JWT_SECRET
    )
    if (error) throw new Error(error.message)
    client.hgetall(session_id, async (err, session) => {
      try {
        res.new_token = new_token
        if (err) throw new Error(err.message)
        const { person_has_role_id: created_by, role_id } = session
        if (role_id === process.env.OWNER_ID) {
          const structures = await Structure.findAll({
            where: { is_deleted: false, is_disabled: false, created_by },
            attributes: {
              exclude: ['is_valid', 'is_deleted', 'created_by', 'is_disabled'],
            },
            include: {
              model: StructureHasCategory,
              where: { is_deleted: false },
              attributes: [
                'structure_has_category_id',
                'structure_category_id',
              ],
              include: {
                model: StructureCategory,
                attributes: ['category_name'],
              },
            },
          })
          const owner_structures = structures.map(
            ({
              name,
              latitude,
              logo_ref,
              longitude,
              description,
              structure_id,
              number_of_votes: rating,
              specific_indications,
              StructureHasCategories: categories,
            }) => {
              return {
                name,
                rating,
                latitude,
                longitude,
                description,
                structure_id,
                specific_indications,
                logo_ref: `${process.env.MULTER_FOLDER}/${logo_ref}`,
                structure_category: categories.reduce(
                  (previous, current) =>
                    `${current.StructureCategory.category_name}, ${previous}`,
                  ''
                ),
                categories: categories.map((_) => _.structure_category_id),
              }
            }
          )
          return res.json({
            owner_structures,
          })
        } else throw new Error(`Access denied for current user`)
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry an unknown error occured',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.getBonusSetting = async (req, res) => {
  try {
    const bonus_setting = await BonusSetting.findOne({
      attributes: { exclude: ['created_at', 'created_by'] },
    })
    return res.json({ bonus_setting })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

/**
 * elasticsearch fetch API
 * @kd mark
 */
exports.searchSercives = async (req, res) => {
  const { service_name, budget, quantity, longitude, latitude } = req.query
  try {
    let services = []
    if (
      !budget &&
      !latitude &&
      !longitude &&
      !service_name &&
      (!budget || !quantity)
    )
      throw new Error(
        'Expecting at least a service_name, unit_price, budget and quantity,  longitude and latitude or even all'
      )
    else if (
      service_name &&
      longitude &&
      latitude &&
      budget &&
      budget &&
      quantity
    ) {
      services = await searchDataIndex('services', {
        bool: {
          should: [
            { match: { service_name } },
            { match: { unit_price: parseInt(budget / quantity) } },
            { match: { longitude } },
            { match: { latitude } },
          ],
        },
      })
    } else {
      services.push(
        ...(!(budget && quantity)
          ? []
          : await searchDataIndex('services', {
              range: {
                unit_price: {
                  gte: budget / quantity - budget / (quantity - 1),
                  lte: budget / quantity + budget / (quantity + 1),
                },
              },
            })),
        ...(!service_name
          ? []
          : await searchDataIndex('services', { match: { service_name } })),
        ...(!longitude
          ? []
          : await searchDataIndex('services', {
              range: {
                longitude: {
                  gte: longitude - 100,
                  lte: longitude + 100,
                },
              },
            })),
        ...(!latitude
          ? []
          : await searchDataIndex('services', {
              range: {
                latitude: {
                  gte: latitude - 100,
                  lte: latitude + 100,
                },
              },
            }))
      )
    }
    return res.json({
      services,
    })
  } catch (error) {
    res.status(500).send({
      message: error.message || 'Sorry, search did not finish well',
    })
  }
}

exports.searchStructures = async (req, res) => {
  const { longitude, latitude, structure_name } = req.query
  try {
    let structures = []
    if (!longitude && !latitude && !structure_name)
      throw new Error(
        'longitude, latitude or structure_name  or all is required'
      )
    else if (longitude && latitude && structure_name) {
      structures = await searchDataIndex('structures', {
        bool: {
          should: [
            { match: { structure_name } },
            { match: { longitude } },
            { match: { latitude } },
          ],
        },
      })
    } else {
      structures.push(
        ...(!structure_name
          ? []
          : await searchDataIndex('structures', { match: { structure_name } })),
        ...(!longitude
          ? []
          : await searchDataIndex('structures', {
              range: {
                longitude: {
                  gte: longitude - 100,
                  lte: longitude + 100,
                },
              },
            })),
        ...(!latitude
          ? []
          : await searchDataIndex('structures', {
              range: {
                latitude: {
                  gte: latitude - 100,
                  lte: latitude + 100,
                },
              },
            }))
      )
    }
    return res.json({
      structures,
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, an error occured',
    })
  }
}
