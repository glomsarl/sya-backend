const {
  Order,
  Service,
  Structure,
  Promotion,
  OrderContent,
  Subscription,
  OrderConfirmation,
  SubscriptionPayment,
  PromotionHasService,
  StructureHasSubscription,
  Sequelize,
  Sequelize: { Op },
} = require('../models/index')

exports.structureTotalSales = async (structure_id, res, use_system) => {
  try {
    const {
      dataValues: { expired_at },
    } = use_system
      ? {
          dataValues: { expired_at: null },
        }
      : await SubscriptionPayment.findOne({
          attributes: [
            [Sequelize.fn('MAX', Sequelize.col('expired_at')), 'expired_at'],
          ],
          include: {
            model: StructureHasSubscription,
            where: structure_id ? { structure_id } : {},
            attributes: ['structure_has_subscription_id'],
          },
        })
    const OrderContents = await OrderContent.findAll({
      attributes: ['quantity', 'unit_price'],
      where: expired_at ? { ordered_at: { [Op.gte]: expired_at } } : {},
      include: [
        {
          model: Order,
          attributes: ['order_id'],
          include: {
            model: OrderConfirmation,
            attributes: ['order_confirmation_id'],
            where: { status: 'SERVED' },
          },
        },
        {
          model: Service,
          attributes: ['service_id'],
          where: structure_id ? { structure_id } : {},
        },
      ],
    })
    let total_sales = OrderContents.reduce(
      (total, { quantity, unit_price, Order }) =>
        Order ? total + quantity * unit_price : total,
      0
    )

    return total_sales
  } catch (error) {
    if (res)
      return res.status(500).send({
        message: error.message || 'Sorry, Something went wrong',
      })
  }
}

exports.unPaidSubscriptionHandler = async (structure_id, res) => {
  try {
    const total_period_revenue = await this.structureTotalSales(
      structure_id,
      res
    )
    const {
      started_at,
      Subscription: { commission_rate },
      Structure: { name: structure_name },
    } = await StructureHasSubscription.findOne({
      attributes: ['started_at'],
      include: [
        {
          model: Subscription,
          attributes: ['commission_rate'],
        },
        {
          model: Structure,
          attributes: ['name'],
          where: { structure_id },
        },
      ],
    })
    const date = new Date(started_at).setDate(
      new Date(started_at).getDate() + 30
    )
    return {
      started_at,
      structure_id,
      is_paid: false,
      structure_name,
      commission_rate,
      total_period_revenue,
      expired_at: new Date(date).toUTCString(),
      commission: (total_period_revenue * commission_rate) / 100,
    }
  } catch (error) {
    if (res)
      return res.status(500).send({
        message: error.message || 'Sorry, Something went wrong',
      })
  }
}

exports.structuringServiceData = async (services) => {
  let rating = 0
  let average_price = 0
  let promotion_has_services = []
  let catalogs = []
  for (let i = 0; i < services.length; i++) {
    const { CatalogDetails, rating: rate, unit_price, service_id } = services[i]
    catalogs.push(...CatalogDetails)
    rating += rate / services.length
    average_price += unit_price / services.length
    const promotions = await Promotion.findAll({
      attributes: ['value', 'promotion_type'],
      where: { ends_at: { [Op.gte]: new Date() } },
      include: {
        model: PromotionHasService,
        attributes: ['promotion_has_service_id'],
        where: { is_deleted: false, service_id },
      },
    })
    promotion_has_services.push(...promotions)
  }

  let is_structure_sy = false
  let promotion_percentage = 0
  if (promotion_has_services && promotion_has_services.length > 0) {
    promotion_has_services.forEach(({ value, promotion_type }) => {
      if (promotion_type === 'Bonus') is_structure_sy = true
      else if (promotion_percentage < value && promotion_type === 'Reduction')
        promotion_percentage = value
    })
  }

  let catalog_option = {
    is_reservable: false,
    is_take_away: false,
    is_home_delivery: false,
    is_payable: false,
  }
  catalogs.forEach((catalog) => {
    const { is_reservable, is_take_away, is_home_delivery, is_payable } =
      catalog
    catalog_option = {
      is_reservable: is_reservable
        ? is_reservable
        : catalog_option.is_reservable,
      is_take_away: is_take_away ? is_take_away : catalog_option.is_take_away,
      is_home_delivery: is_home_delivery
        ? is_home_delivery
        : catalog_option.is_home_delivery,
      is_payable: is_payable ? is_payable : catalog_option.is_payable,
    }
  })
  return {
    is_structure_sy,
    ...catalog_option,
    promotion_percentage,
    rating: parseFloat(rating.toPrecision(3)),
    average_price: Math.round(average_price),
    is_on_promotion: promotion_percentage !== 0,
  }
}

exports.generateStatistics = (ordered_data_array, interval, usage) => {
  let current_date =
    usage === 'client'
      ? ordered_data_array[0].created_at
      : ordered_data_array[0].ordered_at
  let last_date = new Date().setDate(new Date().getDate() - 1)

  let stat_data = []
  do {
    let total_price = 0
    let last_current_date = current_date
    current_date = new Date(current_date).setDate(
      new Date(current_date).getDate() + interval
    )
    let filter_order = ordered_data_array.filter(
      ({ created_at, ordered_at }) => {
        const made_at = usage === 'client' ? created_at : ordered_at
        return last_current_date <= made_at && made_at < current_date
      }
    )
    if (usage === 'finance') {
      total_price = filter_order.reduce((som_price, { OrderContents }) => {
        som_price += OrderContents.reduce(
          (acc_price, { dataValues: { price } }) => {
            return acc_price + price
          },
          0
        )
        return som_price
      }, 0)
    }
    stat_data.push({
      date: current_date,
      quantity: usage === 'finance' ? total_price : filter_order.length,
    })
  } while (current_date <= last_date)
  return stat_data
}
