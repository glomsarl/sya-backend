const {
  Log,
  Order,
  Person,
  Service,
  Catalog,
  Payment,
  Schedule,
  Structure,
  Promotion,
  OrderBonus,
  PersonBonus,
  PersonAudit,
  Publication,
  OrderContent,
  ServiceAudit,
  CatalogAudit,
  Subscription,
  ServiceImage,
  CatalogDetail,
  ScheduleAudit,
  PersonHasRole,
  PromotionAudit,
  StructureAudit,
  BonusSubscription,
  OrderConfirmation,
  SubscriptionAudit,
  StructureCategory,
  CatalogDetailAudit,
  SubscriptionPayment,
  PromotionHasService,
  StructureHasCategory,
  BonusSubscriptionAudit,
  StructureHasSubscription,
  PromotionHasServiceAudit,
  sequelize,
  Sequelize,
  Sequelize: { Op },
} = require('../models')
const { generateStatistics } = require('./common')

exports.findAllPromotions = async (req, res) => {
  const { structure_id } = req.query
  const { structure_id: selected_structure_id } = res.session
  try {
    const promotions = await Promotion.findAll({
      where: {
        structure_id: structure_id ?? selected_structure_id,
        is_deleted: false,
      },
      attributes: { exclude: ['created_by'] },
    })
    return res.json({ promotions })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.findPromotionServices = async (req, res) => {
  const { promotion_id } = req.query

  try {
    if (!promotion_id) throw new Error('promotion_id is required')
    const promotion_services = await PromotionHasService.findAll({
      attributes: ['consequent_price'],
      where: { is_deleted: false, promotion_id },
      include: {
        model: Service,
        where: { is_deleted: false },
        attributes: ['service_id', 'name', 'unit_price'],
      },
    })
    const services = promotion_services.map((service) => {
      const {
        consequent_price,
        Service: { service_id, name: service_name, unit_price },
      } = service
      return { service_id, service_name, unit_price, consequent_price }
    })
    return res.json({ services })
  } catch (error) {
    return res.json({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.createPromotion = async (req, res) => {
  const {
    value,
    ends_at,
    starts_at,
    promotion_name,
    promotion_type,
    purchasing_bonus,
  } = req.body
  const { person_has_role_id: created_by, structure_id } = res.session

  try {
    return sequelize.transaction(async (transaction) => {
      if (
        !value ||
        !ends_at ||
        !starts_at ||
        !structure_id ||
        !promotion_name ||
        !promotion_type
      )
        throw new Error(
          'starts_at, ends_at, promotion_type, promotion_name, value, purchasing_bonus and structure_id is required'
        )
      const promotion = await Promotion.create(
        {
          value,
          ends_at,
          starts_at,
          created_by,
          structure_id,
          promotion_type,
          promotion_name,
          purchasing_bonus: purchasing_bonus ?? 2,
        },
        { transaction }
      )
      if (promotion_type === 'Bonus')
        await BonusSubscription.create(
          {
            promotion_id: promotion.promotion_id,
            quantity_remaining: value,
            created_by,
          },
          { transaction }
        )
      return res.json({ promotion })
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.deletePromotion = (req, res) => {
  const { promotion_id } = req.params
  const { user_id: audited_by } = res.session

  try {
    return sequelize.transaction(async (transaction) => {
      try {
        if (!promotion_id) throw new Error('promotion_id is required')
        const promotion = await Promotion.findOne({
          where: { is_deleted: false, promotion_id },
          attributes: { exclude: ['created_by', 'created_at'] },
        })
        if (promotion) {
          const promotion_audit = await PromotionAudit.create(
            {
              ...promotion.dataValues,
              audited_by,
            },
            { transaction }
          )
          if (promotion_audit) {
            await Promotion.update(
              {
                is_deleted: true,
              },
              {
                where: { is_deleted: false, promotion_id },
                transaction,
              }
            )
            return res.json({ is_promotion_deleted: true })
          } else throw new Error('Promotion deletion failed')
        } else throw new Error('Cannot delete update unknwon record')
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry something went wrong',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.updatePromotion = (req, res) => {
  const { promotion_id } = req.params
  const { user_id: audited_by, person_has_role_id } = res.session
  const { ends_at, value, promotion_name, purchasing_bonus } = req.body

  return sequelize.transaction(async (transaction) => {
    try {
      if (!promotion_id) throw new Error('promotion_id is required')
      const promotion = await Promotion.findOne({
        where: { is_deleted: false, promotion_id },
        attributes: { exclude: ['created_by', 'created_at'] },
      })
      if (promotion) {
        const promotion_audit = await PromotionAudit.create(
          {
            ...promotion.dataValues,
            audited_by,
          },
          { transaction }
        )
        if (promotion_audit) {
          await Promotion.update(
            {
              value: value ?? promotion.value,
              ends_at: ends_at ?? promotion.ends_at,
              promotion_name: promotion_name ?? promotion.promotion_name,
              purchasing_bonus: purchasing_bonus ?? promotion.purchasing_bonus,
            },
            {
              where: { is_deleted: false, promotion_id },
              transaction,
            }
          )
          if (promotion.promotion_type === 'Bonus') {
            const subscription_audit = await Subscription.findOne({
              attributes: { exclude: ['created_at', 'created_by'] },
            })
            await SubscriptionAudit.create(
              {
                ...subscription_audit.dataValues,
                audited_by: person_has_role_id,
              },
              { transaction }
            )
            await Subscription.update(
              {
                quantity_remaining:
                  value - subscription_audit.quantity_cash_out,
              },
              { where: { promotion_id }, transaction }
            )
          }
          return res.json({ is_promotion_deleted: true })
        } else throw new Error('Promotion deletion failed')
      } else throw new Error('Cannot delete update unknwon record')
    } catch (error) {
      return res.status(500).send({
        message: error?.message || 'Sorry something went wrong',
      })
    }
  })
}

exports.addServicesToPromotion = async (req, res) => {
  const { service_to_add, promotion_id } = req.body
  const { user_id: created_by } = res.session

  try {
    if (!service_to_add || !promotion_id)
      throw new Error('service_to_add and promotion_id is required')
    const promotion = await Promotion.findOne({
      attributes: ['value', 'promotion_type'],
      where: { promotion_id, ends_at: { [Op.gte]: new Date() } },
    })
    if (promotion) {
      const { promotion_type, value } = promotion
      const promotion_services = await Service.findAll({
        attributes: ['unit_price', 'service_id', 'name'],
        where: { service_id: service_to_add },
      })
      let new_services = []
      const service_to_add_bulk = promotion_services.map(
        ({ service_id, unit_price, name: service_name }) => {
          const consequent_price =
            promotion_type === 'Reduction'
              ? unit_price - unit_price * value * 0.01
              : unit_price
          new_services.push({
            service_id,
            service_name,
            unit_price,
            consequent_price,
          })
          return {
            service_id,
            created_by,
            promotion_id,
            consequent_price,
          }
        }
      )
      await PromotionHasService.bulkCreate(service_to_add_bulk)
      return res.json({ services: new_services })
    } else throw new Error('Cannot add services to unknwon promotion')
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.removeServiceFromPromotion = async (req, res) => {
  const { service_id, promotion_id } = req.params
  const { user_id: audited_by } = res.session

  try {
    if (!service_id || !promotion_id)
      throw new Error('service_id and promotion_id are required')
    const service = await PromotionHasService.findOne({
      attributes: [
        'promotion_has_service_id',
        'consequent_price',
        'is_deleted',
      ],
      where: { is_deleted: false, promotion_id, service_id },
    })
    if (service) {
      return sequelize.transaction(async (transaction) => {
        const promotion_service_audit = await PromotionHasServiceAudit.create(
          {
            ...service.dataValues,
            audited_by,
          },
          { transaction }
        )
        if (promotion_service_audit) {
          await PromotionHasService.update(
            { is_deleted: true },
            {
              where: { is_deleted: false, promotion_id, service_id },
              transaction,
            }
          )
          return res.json({ is_service_removed: true })
        } else throw new Error('Failed to remove service from promotion')
      })
    } else throw new Error('Cannot find service with such promotion')
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.updateStructure = async (req, res) => {
  const { specific_indications, description, category_id } = JSON.parse(
    req.body.data
  )
  const { user_id: audited_by } = res.session
  const { structure_id } = req.params
  const logo_ref = req.file

  return sequelize.transaction(async (transaction) => {
    try {
      if (!specific_indications || !description)
        throw new Error(
          'logo_ref, specific_indications and description are required'
        )
      const structure = await Structure.findOne({
        where: { is_deleted: false, structure_id },
        attributes: { exclude: ['created_at', 'created_by', 'is_valid'] },
      })
      if (structure) {
        const structure_audit = await StructureAudit.create(
          {
            ...structure.dataValues,
            audited_by,
          },
          { transaction }
        )
        if (structure_audit) {
          await Structure.update(
            {
              logo_ref: logo_ref?.filename,
              specific_indications,
              description,
            },
            {
              where: { is_deleted: false, structure_id },
              transaction,
            }
          )
          await StructureHasSubscription.create({
            structure_id,
            category_id,
          })
          return res.json({ is_structure_updated: true })
        } else throw new Error('Update Failed')
      } else throw new Error('Update Failed')
    } catch (error) {
      return res.status(500).send({
        message: error.message || 'Sorry an unknown error occured',
      })
    }
  })
}

exports.findAllServices = async (req, res) => {
  const { structure_id } = req.query
  const { structure_id: selected_structure_id } = res.session

  try {
    if (!structure_id && !selected_structure_id)
      throw new Error('structure_id is required')
    const services = await Service.findAll({
      where: {
        is_deleted: false,
        structure_id: structure_id ?? selected_structure_id,
      },
      attributes: [
        ['name', 'service_name'],
        'service_id',
        'unit_price',
        'rating',
        ['votes', 'number_of_votes'],
      ],
    })
    return res.json({ services })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.findService = async (req, res) => {
  const { service_id } = req.params

  try {
    if (!service_id) throw new Error('service_id is required')
    const service = await Service.findOne({
      where: { is_deleted: false, service_id },
      attributes: [
        ['name', 'service_name'],
        'service_id',
        'unit_price',
        'rating',
        ['votes', 'number_of_votes'],
        'cancellation_delay',
        ['main_image_ref', 'main_image_ref'],
        ['description', 'service_description'],
      ],
    })
    return res.json({
      service: {
        ...service.dataValues,
        main_image_ref: `${process.env.MULTER_FOLDER}/${service.dataValues.main_image_ref}`,
      },
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

const verifyOverlap = async (structure_id, starts_at, ends_at) => {
  try {
    const overlapCatalog = await Catalog.findOne({
      attributes: ['catalog_id'],
      where: {
        structure_id,
        is_deleted: false,
        [Op.or]: {
          [Op.and]: {
            starts_at: { [Op.lte]: ends_at },
            ends_at: { [Op.gte]: ends_at },
          },
          [Op.and]: {
            starts_at: { [Op.lte]: starts_at },
            ends_at: { [Op.gte]: starts_at },
          },
        },
      },
    })
    return { is_catalog_overlap: Boolean(overlapCatalog) }
  } catch (error) {
    return { error }
  }
}

exports.createCatalog = async (req, res) => {
  const { ends_at, starts_at } = req.body
  const { user_id: created_by, structure_id } = res.session

  try {
    if (!ends_at || !starts_at || !structure_id)
      throw new Error('ends_at, starts_at and structure_id are required')
    const { is_catalog_overlap, error } = await verifyOverlap(
      structure_id,
      starts_at,
      ends_at
    )
    if (error) throw new Error(error.message)
    if (!is_catalog_overlap) {
      const catalog = await Catalog.create({
        ends_at,
        starts_at,
        structure_id,
        created_by,
      })
      return res.json({ catalog })
    } else throw new Error('Failed to creation for overlap reason')
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.editCatalog = (req, res) => {
  const { starts_at, ends_at } = req.body
  const { user_id: audited_by } = res.session
  const { catalog_id } = req.params

  try {
    return sequelize.transaction(async (transaction) => {
      try {
        if (!ends_at || !starts_at || !catalog_id)
          throw new Error('ends_at, starts_at and catalog_id are required')
        const catalog = await Catalog.findOne({
          where: { catalog_id },
          attributes: ['ends_at', 'starts_at', 'catalog_id', 'structure_id'],
        })
        if (catalog) {
          const is_overlap = await verifyOverlap(
            catalog.structure_id,
            starts_at,
            ends_at
          )
          if (is_overlap)
            throw new Error('Failed to creation for overlap reason')
          const catalog_audit = await CatalogAudit.create(
            { ...catalog.dataValues, audited_by },
            { transaction }
          )
          if (catalog_audit) {
            await Catalog.update(
              {
                starts_at,
                ends_at,
              },
              {
                where: { catalog_id, is_deleted: false },
                transaction,
              }
            )
            return res.json({ is_catalog_updated: true })
          } else throw new Error('Update failed')
        } else throw new Error('Cannot update unknwon record')
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry something went wrong',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.editService = (req, res) => {
  const {
    service_name: name,
    service_description: description,
    unit_price,
  } = JSON.parse(req.body.data)
  const main_image_ref = req.file
  const { user_id: audited_by } = res.session
  const { service_id } = req.params

  return sequelize.transaction(async (transaction) => {
    try {
      if (!name || !description || !unit_price || !service_id)
        throw new Error('Incomplety data supply')
      const service = await Service.findOne({
        where: { is_deleted: false, service_id },
        attributes: { exclude: ['created_at', 'created_by'] },
      })
      if (service) {
        await ServiceAudit.create(
          {
            ...service.dataValues,
            audited_by,
          },
          { transaction }
        )
        const new_values = main_image_ref
          ? {
              name,
              description,
              unit_price,
              main_image_ref: main_image_ref.filename,
            }
          : { name, description, unit_price }
        await Service.update(new_values, {
          where: { is_deleted: false, service_id },
          transaction,
        })
        return res.json({ is_service_updated: true })
      } else throw new Error('Cannot update unknwon record')
    } catch (error) {
      return res.status(500).send({
        message: error.message || 'Sorry something went wrong',
      })
    }
  })
}

exports.addServiceImages = async (req, res) => {
  const service_images = req.files
  const { service_id } = req.params
  const { user_id: added_by } = res.session
  try {
    if (!service_images || service_images.length === 0 || !service_id)
      throw new Error(
        'service_id, service_images(array of at least 1) are required'
      )
    const images = await ServiceImage.bulkCreate(
      service_images.map(({ filename: service_image_ref }) => ({
        service_id,
        service_image_ref,
        added_by,
      }))
    )
    return res.json({
      service_images: images.map(({ service_image_id, service_image_ref }) => {
        return {
          service_image_id,
          service_image_ref: `${process.env.MULTER_FOLDER}/${service_image_ref}`,
        }
      }),
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.getServiceImages = async (req, res) => {
  const { service_id } = req.params
  try {
    if (!service_id) throw new Error('service_id is required')
    const service_images = await ServiceImage.findAll({
      where: { is_deleted: false, service_id },
      attributes: ['service_image_id', 'service_image_ref'],
    })
    return res.json({
      service_images: service_images.map(
        ({ service_image_id, service_image_ref }) => {
          return {
            service_image_id,
            service_image_ref: `${process.env.MULTER_FOLDER}/${service_image_ref}`,
          }
        }
      ),
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.deleteCatalog = (req, res) => {
  const { user_id: audited_by } = res.session
  const { catalog_id } = req.params

  try {
    return sequelize.transaction(async (transaction) => {
      try {
        if (!catalog_id)
          throw new Error('ends_at, starts_at and catalog_id are required')
        const catalogs = await Catalog.findOne({
          where: { catalog_id },
          attributes: ['ends_at', 'starts_at', 'catalog_id'],
        })
        if (catalogs) {
          const catalog_audit = await CatalogAudit.create(
            { ...catalogs.dataValues, audited_by },
            { transaction }
          )
          if (catalog_audit) {
            const is_catalog_updated = await Catalog.update(
              {
                is_deleted: true,
              },
              {
                where: { catalog_id, is_deleted: false },
                transaction,
              }
            )
            return res.json({ is_catalog_updated })
          } else throw new Error('Update failed')
        } else throw new Error('Cannot update unknwon record')
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry something went wrong',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.updateStructureSchedule = (req, res) => {
  const { schedule_id } = req.params
  const { user_id: audited_by } = res.session
  const { open_at, close_at, date, day } = req.body

  try {
    return sequelize.transaction(async (transaction) => {
      try {
        if (!schedule_id || !open_at || !close_at || !day)
          throw new Error(
            'schedule_id, open_at, day and close_at most be provided'
          )
        const schedule = await Schedule.findOne(
          {
            where: { is_deleted: false, schedule_id },
            attributes: [
              'date',
              'open_at',
              'close_at',
              'schedule_id',
              'structure_id',
            ],
          },
          { transaction }
        )
        if (schedule) {
          const is_existing = await validateSchedule(
            schedule.structure_id,
            day,
            date
          )
          if (is_existing) throw new Error('Schedule in conflig')
          const schedule_audit = await ScheduleAudit.create(
            {
              ...schedule.dataValues,
              audited_by,
            },
            { transaction }
          )
          if (schedule_audit) {
            const is_schedule_updated = await Schedule.update(
              {
                open_at,
                close_at,
                date: date ?? schedule.dataValues.date,
                day: day ?? schedule.dataValues.day,
              },
              {
                where: { is_deleted: false, schedule_id },
                transaction,
              }
            )
            return res.json({ is_schedule_updated })
          } else throw new Error('Update Failed')
        } else throw new Error('Cannot update unknwon record')
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry an unknown error occured',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.deleteService = (req, res) => {
  const { service_id } = req.params
  const { user_id: audited_by } = res.session
  return sequelize.transaction(async (transaction) => {
    try {
      if (!service_id) throw new Error('service_id is required')
      const service = await Service.findOne({
        where: { is_deleted: false, service_id },
        attributes: { exclude: ['created_at', 'created_by'] },
      })
      if (service) {
        await ServiceAudit.create(
          {
            ...service.dataValues,
            audited_by,
          },
          { transaction }
        )
        await Service.update(
          { is_deleted: true },
          {
            where: { is_deleted: false, service_id },
            transaction,
          }
        )
        return res.json({ is_service_updated: true })
      } else throw new Error('Cannot update unknwon record')
    } catch (error) {
      return res.status(500).send({
        message: error.message || 'Sorry something went wrong',
      })
    }
  })
}

exports.deleteStructureSchedule = (req, res) => {
  const { schedule_id } = req.params
  const { user_id: audited_by } = res.session

  try {
    return sequelize.transaction(async (transaction) => {
      try {
        if (!schedule_id) throw new Error('schedule_id most be provided')
        const schedule = await Schedule.findOne({
          where: { is_deleted: false, schedule_id },
          transaction,
        })
        if (schedule) {
          const schedule_audit = await ScheduleAudit.create(
            {
              ...schedule.dataValues,

              audited_by,
            },
            { transaction }
          )
          if (schedule_audit) {
            const is_schedule_deleted = await Schedule.update(
              {
                is_deleted: true,
              },
              {
                where: { is_deleted: false, schedule_id },
                transaction,
              }
            )
            return res.json({ is_schedule_deleted })
          } else throw new Error('Deletion Failed')
        } else throw new Error('Cannot deleted unknwon record')
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry an unknown error occured',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.addServicesToCatalog = async (req, res) => {
  const { services_to_add, catalog_id } = req.body
  const { user_id: created_by } = res.session

  try {
    if (!services_to_add || services_to_add.length <= 0 || !catalog_id)
      throw new Error('service_to_add and catalog_id most be provided')

    const services_bulk = services_to_add.map((service) => {
      return { ...service, catalog_id, created_by, amount: service.unit_price }
    })
    await CatalogDetail.bulkCreate(services_bulk)
    return res.json({ are_services_added: true })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, something went wrong',
    })
  }
}

exports.removeServicesFromCatalog = async (req, res) => {
  const { remove_services: remove_services_ids, catalog_id } = req.body
  const { user_id: audited_by } = res.session

  try {
    if (!remove_services_ids || !catalog_id)
      throw new Error(
        'remove_services and concerned catalog catalog_id is required'
      )
    const catalog_services = await CatalogDetail.findAll({
      attributes: { exclude: ['created_at', 'created_by'] },
      where: {
        catalog_id,
        is_deleted: false,
        service_id: [...remove_services_ids],
      },
    })
    if (catalog_services) {
      const catalog_services_bulk = catalog_services.map((service) => {
        return { ...service.dataValues, audited_by }
      })
      return sequelize.transaction(async (transaction) => {
        try {
          await CatalogDetailAudit.bulkCreate(catalog_services_bulk, {
            transaction,
          })
          await CatalogDetail.update(
            {
              is_deleted: true,
            },
            {
              where: {
                catalog_id,
                is_deleted: false,
                service_id: [...remove_services_ids],
              },
              transaction,
            }
          )
          return res.json({ are_service_removed: false })
        } catch (error) {
          return res.status(500).send({
            message: error.message || 'Sorry something went wrong',
          })
        }
      })
    }
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

exports.createService = async (req, res) => {
  const {
    unit_price,
    cancellation_delay,
    service_name: name,
    service_description: description,
  } = JSON.parse(req.body.data)
  const { user_id: created_by, structure_id } = res.session
  const main_image_ref = req.file
  try {
    if (!name || !description || !unit_price || !main_image_ref)
      throw new Error('Incomplete data supply')
    const service = await Service.create({
      name,
      unit_price,
      created_by,
      description,
      structure_id,
      cancellation_delay,
      main_image_ref: main_image_ref.filename,
    })
    const {
      name: service_name,
      votes: number_of_votes,
      rating,
      service_id,
      main_image_ref: image_ref,
    } = service
    return res.json({
      service: {
        rating,
        unit_price,
        service_id,
        service_name,
        number_of_votes,
        main_image_ref: `${process.env.MULTER_FOLDER}/${image_ref}`,
      },
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry something went wrong',
    })
  }
}

const validateSchedule = async (structure_id, day, date) => {
  try {
    const condition = date
      ? { [Op.or]: { day, date }, is_deleted: false }
      : { day, is_deleted: false }
    const schedule = await Schedule.findOne({
      attributes: ['schedule_id'],
      where: { ...condition, structure_id },
    })
    return { is_schedule_existing: Boolean(schedule) }
  } catch (error) {
    return { error }
  }
}

exports.addStructureSchedule = async (req, res) => {
  const { open_at, close_at, days, date } = req.body
  const { user_id: created_by, structure_id } = res.session

  try {
    const thedate = date !== '' ? date : null
    if (!open_at || !close_at || !days || days.length === 0 || !structure_id)
      throw new Error(
        'structure_id, open_at, close_at and days(array of at leat one element) are required'
      )
    const schedules = []
    for (let i = 0; i < days.length; i++) {
      const day = days[i]
      const { is_schedule_existing, error } = await validateSchedule(
        structure_id,
        day,
        thedate
      )
      if (error) throw new Error(error.message)
      if (!is_schedule_existing) {
        schedules.push(
          await Schedule.create({
            day,
            open_at,
            close_at,
            created_by,
            structure_id,
            date: thedate,
          })
        )
      } else
        throw new Error('A schedule already exist with the given date or day')
    }
    return res.json({ schedules })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry an unknown error occured',
    })
  }
}

exports.createPublications = async (req, res) => {
  const { structure_id: send_structure_id } = req.body
  const { user_id: created_by, structure_id: selected_structure_id } =
    res.session
  const publication_files = req.files
  const structure_id = selected_structure_id ?? send_structure_id
  try {
    if (!structure_id || !publication_files || publication_files.length <= 0)
      throw new Error('Imcomplete data supply')
    const publications_bulk_data = publication_files.map((file) => {
      return { publication_ref: file.filename, structure_id, created_by }
    })
    const publications = await Publication.bulkCreate(publications_bulk_data)
    return res.json({
      publications: publications.map(({ publication_ref, publication_id }) => ({
        publication_id,
        publication_ref: `${process.env.MULTER_FOLDER}/${publication_ref}`,
      })),
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.deletePublication = async (req, res) => {
  const { publication_id } = req.params

  try {
    if (!publication_id) throw new Error('promotion id is required')
    await Publication.update(
      { is_deleted: true, deleted_at: new Date() },
      { where: { publication_id } }
    )
    return res.json({ is_publication_deleted: true })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

const generateOverview = async (structure_id, period) => {
  const condition = period ? { ordered_at: { [Op.gte]: period } } : {}
  try {
    let stats = [
      { variable: 'unconfirmed', status: 'UNCONFIRMED', value: 0 },
      { variable: 'confirmed', status: 'CONFIRMED', value: 0 },
      { variable: 'cancelled', status: 'CANCELLED', value: 0 },
      { variable: 'served', status: 'SERVED', value: 0 },
    ]
    let generatedStats = {
      confirmed: 0,
      unconfirmed: 0,
      cancelled: 0,
      served: 0,
    }
    for (let i = 0; i < stats.length; i++) {
      stats[i].value = await Order.findOne({
        attributes: [
          [
            Sequelize.fn('COUNT', Sequelize.col(`Order.order_id`)),
            stats[i].variable,
          ],
        ],
        where: condition,
        include: [
          {
            model: OrderContent,
            attributes: ['order_content_id'],
            include: {
              model: Service,
              where: { structure_id },
            },
          },
          {
            model: OrderConfirmation,
            where: { status: stats[i].status },
          },
        ],
      })
      generatedStats = stats[i].value
        ? { ...generatedStats, ...stats[i].value.dataValues }
        : generatedStats
    }
    const service_stats = await OrderContent.findAll({
      group: ['Service.service_id'],
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col('Service.service_id')),
          'total_services',
        ],
      ],
      where: condition,
      include: {
        model: Service,
        attributes: ['name'],
        where: { structure_id },
      },
    })
    let group_services = service_stats
      .map(
        ({
          Service: { name: service_name },
          dataValues: { total_services },
        }) => {
          return { total_services, service_name }
        }
      )
      .sort()
    const group_service = group_services[group_services.length - 1]
    if (group_service) {
      const { total_services, service_name } = group_service
      return {
        ...generatedStats,
        most_ordered_service: `${service_name} (${total_services})`,
      }
    } else return generatedStats
  } catch (error) {
    throw new Error(error.message)
  }
}

exports.getOrderStatistics = async (req, res) => {
  const { structure_id } = req.query
  const { selected_structure_id } = res.session

  try {
    if (!structure_id && !selected_structure_id)
      throw new Error('structure id is required')

    const overall_statistics = await generateOverview(
      structure_id ?? selected_structure_id
    )

    const aDayAgo = new Date(new Date().setDate(new Date().getDate() - 1))
    const daily_statistics = await generateOverview(
      structure_id ?? selected_structure_id,
      aDayAgo
    )

    const sevenDaysAgo = new Date(new Date().setDate(new Date().getDate() - 7))
    const weekly_statistics = await generateOverview(
      structure_id ?? selected_structure_id,
      sevenDaysAgo
    )

    const thirtyDaysAgo = new Date(
      new Date().setDate(new Date().getDate() - 30)
    )
    const monthly_statistics = await generateOverview(
      structure_id ?? selected_structure_id,
      thirtyDaysAgo
    )

    const aYearAgo = new Date(new Date().setDate(new Date().getDate() - 360))
    const year_statistics = await generateOverview(
      structure_id ?? selected_structure_id,
      aYearAgo
    )

    return res.json({
      statistics: [
        { ...daily_statistics, period: 1 },
        { ...weekly_statistics, period: 7 },
        { ...monthly_statistics, period: 30 },
        { ...year_statistics, period: 360 },
      ],
      overall: { ...overall_statistics, period: 'all' },
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.getServicesOrderStatistics = async (req, res) => {
  const { structure_id } = req.query

  try {
    if (!structure_id) throw new Error('structure_id is required')
    let services = []
    const status = [
      'CONFIRMED',
      'UNCONFIRMED',
      ['CANCELLED', 'SYSTEM-CANCELLED'],
      'SERVED',
    ]
    for (let i = 0; i < status.length; i++) {
      const service_data = await Service.findAll({
        attributes: ['service_id', 'name', 'main_image_ref'],
        where: {
          is_deleted: false,
          structure_id: structure_id,
        },
        include: {
          model: OrderContent,
          attributes: ['order_content_id'],
          include: {
            model: Order,
            attributes: ['order_id'],
            include: {
              model: OrderConfirmation,
              where: { status: status[i] },
              attributes: ['order_confirmation_id'],
            },
          },
        },
      })
      service_data.forEach((service) => {
        const {
          service_id,
          name: service_name,
          OrderContents,
          main_image_ref,
        } = service
        let counted_orders = 0
        let is_service_new = true
        let order_confirmation_id = null
        OrderContents.forEach(({ Order }) => {
          if (Order) {
            counted_orders++
            order_confirmation_id =
              Order.OrderConfirmations[0].order_confirmation_id
          }
        })
        services = services.map((service) => {
          if (service.service_id === service_id) {
            is_service_new = false
            const value = {
              0: {
                confirmed: service.confirmed
                  ? service.confirmed
                  : 0 + counted_orders,
              },
              1: {
                unconfirmed: service.unconfirmed
                  ? service.unconfirmed
                  : 0 + counted_orders,
              },
              2: {
                cancelled: service.cancelled
                  ? service.cancelled
                  : 0 + counted_orders,
              },
              3: {
                served: service.served ? service.served : 0 + counted_orders,
              },
            }
            return {
              ...service,
              ...value[i],
              order_confirmation_id,
            }
          } else return service
        })
        if (is_service_new || services.length === 0) {
          const value = {
            0: { confirmed: counted_orders },
            1: { unconfirmed: counted_orders },
            2: { cancelled: counted_orders },
            3: { served: counted_orders },
          }
          services.push({
            service_id,
            ...value[i],
            service_name,
            order_confirmation_id,
            main_image_ref: `${process.env.MULTER_FOLDER}/${main_image_ref}`,
          })
        }
      })
    }
    return res.json({ services })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.getAllServiceOrders = async (req, res) => {
  const { service_id, filter_category } = req.query

  try {
    if (!service_id) throw new Error('service id is required')
    const order_data = await Order.findAll({
      attributes: ['ordered_at', 'selected_date', 'order_id'],
      include: [
        {
          model: PersonHasRole,
          where: { is_deleted: false },
          attributes: ['person_has_role_id'],
          include: {
            model: Person,
            attributes: ['first_name', 'last_name'],
          },
        },
        {
          model: OrderContent,
          where: { service_id },
          attributes: ['quantity', 'unit_price'],
        },
        {
          model: OrderConfirmation,
          attributes: ['status'],
          where: filter_category
            ? { status: `${filter_category}`.toUpperCase() }
            : {},
        },
        {
          model: Payment,
          attributes: ['is_paid', 'payment_method'],
        },
      ],
    })
    const orders = order_data.map((order) => {
      const {
        Payments,
        order_id,
        OrderConfirmations,
        ordered_at: ordered_on,
        selected_date,
        OrderContents,
        PersonHasRole: {
          Person: { first_name, last_name },
        },
      } = order
      const { quantity, unit_price } = OrderContents[0]
      const served_order = OrderConfirmations.find(
        ({ status }) => status === 'SERVED'
      )
      const unpaid_order = Payments.find(({ is_paid }) => is_paid === false)
      return {
        quantity,
        order_id,
        ordered_on,
        unit_price,
        selected_date,
        is_served: Boolean(served_order),
        ordered_by: `${first_name} ${last_name}`,
        order_status: OrderConfirmations[0].status,
        payment_status: unpaid_order ? 'unpaid' : 'cash',
      }
    })
    return res.json({ orders })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.confirmServiceOrder = async (req, res) => {
  const { order_id } = req.params

  try {
    if (!order_id) throw new Error('order id is required')
    const order = await Order.findOne({
      attributes: ['selected_date', 'ordered_by'],
      where: { order_id },
      include: [
        {
          model: PersonHasRole,
          attributes: ['person_has_role_id', 'person_id'],
        },
        {
          model: OrderConfirmation,
          attributes: ['order_confirmation_id'],
          where: { status: { [Op.ne]: 'CANCELLED' } },
        },
        {
          model: OrderBonus,
          attributes: ['created_by', 'bonus_acquired', 'promotion_id'],
        },
        {
          model: OrderContent,
          attributes: ['service_id'],
          include: {
            model: Service,
            attributes: ['structure_id'],
          },
        },
      ],
    })
    if (order) {
      let {
        ordered_by,
        selected_date,
        PersonHasRole: { person_has_role_id, person_id },
        OrderContents,
        OrderConfirmations,
        OrderBonus,
      } = order
      if (new Date() < new Date(selected_date))
        throw new Error(
          'Cannot serve order before serving time selected by order'
        )
      const {
        Service: { structure_id },
      } = OrderContents[0]
      const { order_confirmation_id } = OrderConfirmations[0]
      let promotion_id = null
      let bonus = 0
      if (OrderBonus.length > 0) {
        const { created_by, bonus_acquired, promotion_id: promotion } = OrderBonus[0]
        person_has_role_id = created_by
        bonus = bonus_acquired
        promotion_id = promotion
      }

      const {
        StructureCategory: { structure_category_id },
      } = await StructureHasCategory.findOne({
        where: { structure_id },
        attributes: ['structure_has_category_id'],
        include: {
          model: StructureCategory,
          attributes: ['structure_category_id'],
        },
      })
      const person_bonus = await PersonBonus.findOne({
        attributes: ['person_id'],
        where: { person_has_role_id },
      })
      const person = await Person.findOne({
        include: {
          model: PersonHasRole,
          attributes: ['person_id'],
          where: { person_has_role_id, is_deleted: false },
        },
      })
      const bonus_subscription = await BonusSubscription.findOne({
        where: { promotion_id },
        attributes: { exclude: ['created_at', 'created_by'] },
      })

      return sequelize.transaction(async (transaction) => {
        try {
          await OrderConfirmation.update(
            { status: 'SERVED', served_at: new Date() },
            { where: { order_confirmation_id }, transaction }
          )
          if (person_bonus) {
            await PersonBonus.increment(
              {
                value: bonus,
              },
              {
                where: { person_has_role_id, structure_category_id },
                transaction,
              }
            )
          } else {
            await PersonBonus.create(
              {
                person_has_role_id,
                structure_category_id,
                value: bonus,
              },
              { transaction }
            )
          }
          await PersonAudit.create(
            {
              ...person.dataValues,
            },
            { transaction }
          )
          await Person.increment(
            {
              bonus_acquired: bonus,
            },
            { where: { person_id }, transaction }
          )
          if(bonus_subscription) {
            await BonusSubscriptionAudit.create(
              {
                ...bonus_subscription.dataValues,
                audited_by: ordered_by,
              },
              { transaction }
            )
            await BonusSubscription.increment(
              {
                quantity_cashed_out: bonus,
              },
              {
                where: { promotion_id },
                transaction,
              }
            )
          }
          return res.json({ is_order_confirmed: true })
        } catch (error) {
          return res.status(500).send({
            message: error.message || 'Sorry, Something went wrong',
          })
        }
      })
    } else throw new Error('Sorry, This order was cancelled')
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.getOwnerOverviews = async (req, res) => {
  const { structure_id } = req.query
  try {
    if (!structure_id) throw new Error('structure_id is required')
    const {
      dataValues: { number_of_catalogs },
    } = await Catalog.findOne({
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col(`Catalog.catalog_id`)),
          'number_of_catalogs',
        ],
      ],
      where: { is_deleted: false, structure_id },
    })
    const {
      dataValues: { number_of_services },
    } = await Service.findOne({
      attributes: [
        [
          Sequelize.fn('COUNT', Sequelize.col(`Service.service_id`)),
          'number_of_services',
        ],
      ],
      where: { is_deleted: false, structure_id },
    })
    const {
      dataValues: { number_of_reductions },
    } = await PromotionHasService.findOne({
      attributes: [
        [
          Sequelize.fn(
            'COUNT',
            Sequelize.col(`PromotionHasService.promotion_has_service_id`)
          ),
          'number_of_reductions',
        ],
      ],
      where: { is_deleted: false },
      include: [
        {
          model: Service,
          where: { structure_id },
        },
        {
          model: Promotion,
          where: { promotion_type: 'Reduction' },
        },
      ],
    })
    const {
      dataValues: { number_of_bonuses },
    } = await PromotionHasService.findOne({
      attributes: [
        [
          Sequelize.fn(
            'COUNT',
            Sequelize.col(`PromotionHasService.promotion_has_service_id`)
          ),
          'number_of_bonuses',
        ],
      ],
      where: { is_deleted: false },
      include: [
        {
          model: Service,
          where: { structure_id },
        },
        {
          model: Promotion,
          where: { promotion_type: 'Bonus' },
        },
      ],
    })

    return res.json({
      overviews: {
        number_of_catalogs,
        number_of_services,
        number_of_reductions,
        number_of_bonuses,
      },
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.getFinanceStatistics = async (req, res) => {
  const { interval, starts_at, ends_at, structure_id } = req.query
  try {
    if (!interval) throw new Error('interval_duration is required')
    const orders_entries = await Order.findAll({
      attributes: ['order_id', 'ordered_at'],
      order: [['ordered_at', 'ASC']],
      where:
        starts_at && ends_at && starts_at !== '' && ends_at !== ''
          ? {
              ordered_at: {
                [Op.and]: {
                  [Op.gte]: parseInt(starts_at),
                  [Op.lte]: parseInt(ends_at),
                },
              },
            }
          : starts_at && starts_at !== ''
          ? { ordered_at: { [Op.gte]: parseInt(starts_at) } }
          : ends_at !== ''
          ? { ordered_at: { [Op.lte]: parseInt(ends_at) } }
          : {},
      include: [
        {
          model: OrderContent,
          attributes: [
            [
              Sequelize.literal(
                `(OrderContents.quantity * OrderContents.unit_price)`
              ),
              'price',
            ],
          ],
          include: {
            model: Service,
            where: { structure_id },
          },
        },
        {
          model: OrderConfirmation,
          where: { status: 'SERVED' },
        },
      ],
    })
    return res.json({
      entries:
        orders_entries.length > 0
          ? generateStatistics(orders_entries, parseInt(interval), 'finance')
          : [],
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}

exports.structureSubscription = async (req, res) => {
  const { structure_id, payment_method, subscription_payment_id } = req.body
  try {
    if (!payment_method || !structure_id)
      throw new Error('structure_id and payment_method are required')
    const {
      dataValues: { total_sales },
    } = await OrderContent.findOne({
      attributes: [
        [Sequelize.fn('SUM', Sequelize.col('unit_price')), 'total_sales'],
      ],
      include: [
        {
          model: Order,
          attributes: ['order_id'],
          include: {
            model: OrderConfirmation,
            attributes: ['order_confirmation_id'],
            where: { status: 'CONFIRMED' },
          },
        },
        {
          model: Service,
          attributes: ['service_id'],
          include: {
            model: Structure,
            where: { structure_id },
          },
        },
      ],
    })
    const setting = await Subscription.findOne({
      attributes: ['commission_rate'],
    })
    if (!setting) throw new Error('Bonus setting not yet set')
    const { commission_rate, subscription_type } = setting
    const intervals = {
      MONTHLY: 30,
      TERMLY: 180,
      YEARLY: 360,
    }
    return sequelize.transaction(async (transaction) => {
      try {
        const { structure_subscription_id } =
          await StructureHasSubscription.create(
            {
              structure_id,
              subscription_id: process.env.SUBSCRIPTION_1,
              expired_at: new Date(
                new Date().setDate(
                  new Date().getDate() + intervals[subscription_type]
                )
              ),
            },
            {
              onDuplicateUpdated: subscription_payment_id
                ? ['subscription_id', 'structure_id']
                : [],
              transaction,
            }
          )
        const subscription_payment = await SubscriptionPayment.create(
          {
            is_paid: true,
            payment_method,
            commission_rate,
            subscription_payment_id,
            structure_subscription_id,
            reference: Date.now().toString(),
            total_period_revenue: total_sales,
            commission: total_sales * commission_rate,
          },
          {
            onDuplicateUpdated: subscription_payment_id
              ? ['subscription_payment_id']
              : [],
            transaction,
          }
        )
        return res.json({ subscription_payment })
      } catch (error) {
        return res.status(500).send({
          message: error.message || 'Sorry, Something went wrong',
        })
      }
    })
  } catch (error) {
    return res.status(500).send({
      message: error.message || 'Sorry, Something went wrong',
    })
  }
}
