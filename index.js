const express = require('express')
const app = express()
require('dotenv').config()
const cors = require('cors')
const db = require('./models')
const shell = require('shelljs')
const { responseInterceptor } = require('./utils/token-insertion')
const {
  orderRemainder,
  specificDateJob,
  orderCancellationJob,
} = require('./jobs/order-reminder')
const {
  subscriptionReminder,
  verifyStructuresSubscription,
} = require('./jobs/subscription-remider')

var corsOptions = {
  origin: '*',
}

app.use(cors(corsOptions))

// parse requests of content-type - application/json
app.use(express.json())
app.use('/uploads', express.static('uploads'))

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }))

db.sequelize.sync()

// db.sequelize.sync({ force: true }).then(() => {
//   shell.exec('npx sequelize-cli db:seed:all')
//   console.log('$ sequelize sync successfully')
// })
// simple route
app.get('/', async (req, res) => {
  res.json({
    message: 'Welcome to See you Again REST API.',
  })
})

app.use(responseInterceptor)
require('./routes/index')(app)
require('./jobs/subscription-remider')

// set port, listen for requests
const PORT = process.env.PORT || 4000
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`)
})
