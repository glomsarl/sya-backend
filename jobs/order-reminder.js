const {
  Payment,
  OrderConfirmation,
  Sequelize: { Op },
} = require('../models/index')
const CronJob = require('cron').CronJob
const { mailer } = require('../utils/mailer')

//1- daily order reminder
exports.orderRemainder = (
  time_interval,
  { structure_name, quantity, service_name, serving_date, order_id, email }
) => {
  const new_job = new CronJob({
    cronTime: `0 00 */${time_interval} * * *`,
    onTick: async () => {
      try {
        console.log({ time_interval, order_id })
        const next_date_interval = new Date(
          new Date().setHours(new Date().getHours() + 1)
        )
        const payment = await Payment.findOne({
          where: { order_id, is_paid: true },
        })
        const order = await OrderConfirmation.findOne({
          where: { order_id },
          attributes: ['status'],
        })
        mailer([email], 'Order Reminder', '../views/OrderReminder.hbs', {
          quantity,
          service_name,
          serving_date,
          structure_name,
          status: order?.status,
          payment_status: payment ? 'PAID' : 'NOT PAID',
        })
          .then((info) => console.log(info))
          .catch((error) => console.log(error))
        const last_date = new Date(
          new Date(serving_date).setHours(new Date(serving_date).getHours() - 2)
        )
        if (last_date - next_date_interval <= 0) {
          new_job.stop()
          this.specificDateJob(last_date, {
            structure_name,
            service_name,
            serving_date,
            quantity,
            order_id,
            email,
          })
        }
      } catch (error) {
        console.error(error)
      }
    },
  })
  return new_job
}

//2-  last reminder 2 hours before the ordered time
exports.specificDateJob = (
  specific_date,
  { structure_name, quantity, service_name, serving_date, order_id, email }
) => {
  return new CronJob({
    cronTime: new Date(specific_date),
    onTick: async () => {
      try {
        const payment = await Payment.findOne({
          where: { order_id, is_paid: true },
        })
        const order = await OrderConfirmation.findOne({
          where: { order_id },
          attributes: ['status'],
        })
        mailer([email], 'Order Reminder', '../views/OrderReminder.hbs', {
          quantity,
          serving_date,
          service_name,
          structure_name,
          status: order?.status,
          payment_status: payment ? 'PAID' : 'NOT PAID',
        })
          .then((info) => console.log(info))
          .catch((error) => console.log(error))
      } catch (error) {
        console.error(error)
      }
    },
  })
}

exports.orderCancellationJob = (
  specific_date,
  {
    structure_name,
    quantity,
    service_name,
    serving_date,
    order_id,
    unit_price,
    email,
  }
) => {
  return new CronJob({
    cronTime: new Date(specific_date),
    onTick: async () => {
      try {
        await OrderConfirmation.update(
          {
            status: 'SYSTEM-CANCELLED',
            cancelled_at: new Date(),
          },
          { where: { order_id, status: 'UNCONFIRMED' } }
        )
        const payment = await Payment.findOne({
          where: { order_id, is_paid: true },
        })
        mailer([email], 'Order Notice', '../views/OrderNotice.hbs', {
          quantity,
          unit_price,
          service_name,
          serving_date,
          structure_name,
          status: 'UNCONFIRMED',
          payment_status: payment ? 'PAID' : 'NOT PAID',
        })
          .then((info) => console.log(info))
          .catch((error) => console.log(error))
      } catch (error) {
        console.error(error)
      }
    },
  })
}
