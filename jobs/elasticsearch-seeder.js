const {
  Service,
  Structure,
  Sequelize: { Op },
} = require('../models/index')
const CronJob = require('cron').CronJob
const {
  createDataIndex,
  searchAllIndex,
} = require('../utils/elasticsearch')

exports.serviceIndexing = () => {
  const cron_job = new CronJob({
    cronTime: '0 00 00 * * *',
    onTick: async () => {
      try {
        const all_services = await Service.findAll({
          attributes: ['service_id', 'name', 'unit_price', 'rating'],
          where: { is_deleted: false },
          include: {
            model: Structure,
            attributes: ['structure_id', 'name', 'longitude', 'latitude'],
            where: { is_disabled: false, is_deleted: false },
          },
        })
        const indexed_services = await searchAllIndex('services')
        if (indexed_services && indexed_services.length > 0) {
          all_services.forEach(
            ({
              rating,
              service_id,
              unit_price,
              name: service_name,
              Structure: {
                latitude,
                longitude,
                structure_id,
                name: structure_name,
              },
            }) => {
              if (
                !indexed_services.find(
                  ({ service_id: id }) => id === service_id,
                )
              )
                console.log({
                  ...createDataIndex('services', {
                    rating,
                    latitude,
                    longitude,
                    unit_price,
                    service_id,
                    service_name,
                    structure_id,
                    structure_name,
                  }),
                })
            },
          )
        } else {
          all_services.forEach(
            ({
              rating,
              unit_price,
              service_id,
              name: service_name,
              Structure: { structure_id, name: structure_name },
            }) => {
              console.log({
                ...createDataIndex('services', {
                  rating,
                  latitude,
                  longitude,
                  unit_price,
                  service_id,
                  service_name,
                  structure_id,
                  structure_name,
                }),
              })
            },
          )
        }
      } catch (error) {
        console.error({
          message: error.message || 'Sorry something went wrong',
        })
      }
    },
  })
  return cron_job
}

exports.structureIndexing = () => {
  const cron_job = new CronJob({
    cronTime: '0 00 00 * * *',
    onTick: async () => {
      try {
        const all_structures = await Structure.findAll({
          attributes: [
            'name',
            'latitude',
            'longitude',
            'description',
            'structure_id',
            'number_of_votes',
            'specific_indications',
          ],
          where: { is_deleted: false, is_disabled: false },
        })
        const indexed_structures = await searchAllIndex('structures')
        if (indexed_structures && indexed_structures.length > 0) {
          all_structures.forEach(
            ({
              name: structure_name,
              latitude,
              longitude,
              description,
              structure_id,
              number_of_votes: rating,
              specific_indications,
            }) => {
              if (
                !indexed_structures.find(
                  ({ structure_id: id }) => id === structure_id,
                )
              )
                console.log({
                  ...createDataIndex('structures', {
                    rating,
                    latitude,
                    longitude,
                    description,
                    structure_id,
                    structure_name,
                    specific_indications,
                  }),
                })
            },
          )
        } else {
          all_structures.forEach(
            ({
              name: structure_name,
              latitude,
              longitude,
              description,
              structure_id,
              number_of_votes: rating,
              specific_indications,
            }) => {
              console.log({
                ...createDataIndex('structures', {
                  rating,
                  latitude,
                  longitude,
                  description,
                  structure_id,
                  structure_name,
                  specific_indications,
                }),
              })
            },
          )
        }
      } catch (error) {
        console.error({
          message: error.message || 'Sorry something went wrong',
        })
      }
    },
  })
  return cron_job
}
