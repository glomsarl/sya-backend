//sending an email to all structure owners after the given parameter(time_interval = 20/25/28) time,
const { structureTotalSales } = require('../controllers/common')
const {
  Structure,
  Sequelize: { Op },
  StructureHasSubscription,
  sequelize,
} = require('../models/index')
const CronJob = require('cron').CronJob
const { mailer } = require('../utils/mailer')

exports.subscriptionReminder = (
  specific_date,
  { email, month, structure_name, commission_rate, structure_id, lastest_date },
  number_of_times
) => {
  const newJob = new CronJob({
    cronTime: new Date(specific_date),
    onTick: async () => {
      try {
        const total_sales = await structureTotalSales(structure_id)
        mailer(
          [email],
          'SYA Business Invoice',
          '../views/BusinessInvoiceTemplate.hbs',
          {
            month,
            lastest_date,
            structure_name,
            commission_rate,
            total_sales: total_sales,
            total_due: (total_sales * commission_rate) / 100,
          }
        )
          .then((info) => console.log(info))
          .catch((error) => console.log(error))
        if (number_of_times === 1) {
          const next_date = new Date(
            new Date().setDate(new Date().getDate() + 5)
          )
          this.subscriptionReminder(next_date, {
            month,
            email,
            lastest_date,
            structure_id,
            structure_name,
            commission_rate,
          }).start()
        } else if (number_of_times === 2) {
          const next_date = new Date(
            new Date().setDate(new Date().getDate() + 3)
          )
          this.subscriptionReminder(next_date, {
            month,
            email,
            lastest_date,
            structure_id,
            structure_name,
            commission_rate,
          }).start()
        }
        newJob.stop()
      } catch (error) {
        console.error(error)
      }
    },
  })
  return newJob
}

//verify after 30 days that a structure has paid is subscription
exports.verifyStructuresSubscription = (duration, structure_id) => {
  const ending_date = new Date(
    new Date().setDate(new Date().getDate() + duration)
  )
  return new CronJob({
    cronTime: new Date(ending_date),
    onTick: async () => {
      try {
        const starting_date = new Date(
          new Date().setDate(new Date().getDate() - duration)
        )
        return sequelize.transaction(async (transaction) => {
          await StructureHasSubscription.update(
            { expired_at: new Date() },
            {
              transaction,
              where: { structure_id, started_at: { [Op.lte]: starting_date } },
            }
          )
          await Structure.update(
            { is_disabled: true },
            {
              transaction,
              where: { structure_id },
            }
          )
        })
      } catch (error) {
        console.error(error)
      }
    },
  })
}
