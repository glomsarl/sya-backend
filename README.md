# Getting Started with SYA-Backend

## Execute Project

To run the project, follow the steps below:

1. Clone the project
### `git clone https://gitlab.com/glomsarl/sya-backend.git`

2. Install project dependencies
### `npm install`

3. Update env file to match env.skeleton in base directory

4. Download, install and launch Redis server on your local computer

5. Update env file by changing variable `REDIS_HOST` value to match (3) above execution port (may work by default without configurations)

6. Bootstrap required database data through seed file
### `npx sequelize-cli db:seed:all`

7. Launch application
### `npm start`

8. HAPPY HACKING
