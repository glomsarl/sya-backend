'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("StructureHasCategory", {
      structure_has_category_id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      is_category_active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      structure_id: {
        type: Sequelize.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "Structure",
          key: "structure_id",
          as: "structure_id",
        },
      },
      structure_category_id: {
        type: Sequelize.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "StructureCategory",
          key: "structure_category_id",
          as: "structure_category_id",
        },
      },
      added_by: {
        type: Sequelize.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "added_by",
        },
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('StructureHasCategory');
  }
};