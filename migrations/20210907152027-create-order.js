'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Order', {
      order_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      ordered_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      selected_date: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      ordered_by: {
        type: Sequelize.UUID,
        references: {
          model: "PersonHasRole",
          key: "person_role_id",
          as: "ordered_by"
        }
      },
      order_type: {
        type: Sequelize.ENUM('R', 'T', 'H'),
        allowNull: false,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Order');
  }
};