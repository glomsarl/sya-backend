'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('PersonHasRoleAudit', {
      admin_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      audited_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      role_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Role",
          key: "role_id",
          as: "role_id"
        }
      },
      person_has_role_id: {
        allowNull: false,
        type: DataTypes.UUID,
        references: {
          model: "PersonHasRole",
          key: "person_has_role_id",
          as: "person_has_role_id"
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('PersonHasRoleAudit');
  }
};
