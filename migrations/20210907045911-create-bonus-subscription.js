'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('BonusSubscription', {
      bonus_subscription_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      promotion_id: {
        type: Sequelize.UUID,
        allowNull: false,   
        references: {
          model: "Promotion",
          key: "promotion_id",
          as: "promotion_id",
        }
      },
      quantity_cashed_out: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      quantity_cashed_in: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      created_by: {
        type: Sequelize.UUID,
        allowNull: false,   
        references: {
          model: "PersonHasRole",
          key: "person_has_role_id",
          as: "created_by",
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('BonusSubscription');
  }
};