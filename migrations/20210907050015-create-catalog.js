'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Catalog', {
      catalog_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      structure_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Structure",
          key: "structure_id",
          as: "structure_id"
        }
      },
      ends_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      starts_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        allowNull: false,
      },
      created_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "created_by",
        }
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        allowNull: false, 
        defaultValue: false
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Catalog');
  }
};