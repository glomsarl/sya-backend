'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('PersonAudit', {
      person_audit_id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      first_name: {
        type: Sequelize.STRING,
      },
      last_name: {
        type: Sequelize.STRING,
      },
      national_id_number: {
        type: Sequelize.STRING,
        unique: true,
      },
      date_of_birth: {
        type: Sequelize.DATEONLY,
      },
      gender: {
        type: Sequelize.ENUM('M', 'F'),
      },
      phone: {
        type: Sequelize.INTEGER,
      },
      email: {
        type: Sequelize.STRING,
      },
      person_image_ref: {
        type: Sequelize.STRING,
      },
      password: {
        type: Sequelize.STRING,
      },
      bonus_acquired: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      person_id: {
        type: Sequelize.UUID,
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'Person',
          key: 'person_id',
          as: 'person_id',
        },
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('PersonAudit')
  },
}
