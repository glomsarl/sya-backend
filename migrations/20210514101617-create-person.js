'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Person", {
      person_id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      first_name: {
        type: Sequelize.STRING,
      },
      last_name: {
        type: Sequelize.STRING,
      },
      national_id_number: {
        type: Sequelize.STRING,
        unique: true,
      },
      date_of_birth: {
        type: Sequelize.DATEONLY,
      },
      gender: {
        type: Sequelize.ENUM("M", "F"),
      },
      phone: {
        type: Sequelize.INTEGER,
        unique: true,
      },
      email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      bonus_acquired: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      referral_id: {
        type: Sequelize.UUID,
        references: {
          model: "Person",
          key: "person_id",
          as: "referral_id"
        }
      },
      person_image_ref: {
        type: Sequelize.STRING,
        unique: true,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Person');
  }
};
