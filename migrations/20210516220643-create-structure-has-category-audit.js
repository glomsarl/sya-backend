'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("StructureHasCategoryAudit", {
      structure_has_category_audit_id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      is_category_active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      structure_has_category_id: {
        type: Sequelize.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "StructureHasCategory",
          key: "structure_has_category_id",
          as: "structure_has_category_id",
        },
      },
      audited_by: {
        type: Sequelize.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "audited_by",
        },
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('StructureHasCategoryAudit');
  }
};