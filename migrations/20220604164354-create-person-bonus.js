'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('PersonBonus', {
      person_bonus_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      value: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      person_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Person",
          key: "person_id",
          as: "person_id",
        }
      },
      structure_category_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "StructureCategory",
          key: "structure_category_id",
          as: "structure_category_id",
        }
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW,
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('PersonBonus')
  },
}
