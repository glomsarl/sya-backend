'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('CatalogDetail', {
      catalog_detail_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      is_reservable: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      is_take_away: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      is_home_delivery: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      is_payable: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false
      },
      amount: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      quantity: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      catalog_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        refenrences: {
          model: 'Catalog',
          key: 'catalog_id',
          as: 'catalog_id',
        },
      },
      promotion_has_service_id: {
        type: Sequelize.UUID,
        allowNull: false,
        refenrences: {
          model: 'PromotionHasService',
          key: 'promotion_has_service_id',
          as: 'promotion_has_service_id',
        },
      },
      service_id: {
        type: Sequelize.UUID,
        allowNull: false,
        refenrences: {
          model: 'Service',
          key: 'service_id',
          as: 'service_id',
        },
      },
      bonus_subscription_has_service_id: {
        type: Sequelize.UUID,
        refenrences: {
          model: 'BonusSubscriptionHasService',
          key: 'bonus_subscription_has_service_id',
          as: 'bonus_subscription_has_service_id',
        },
      },
      cancellation_delay: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        allowNull: false,
      },
      created_by: {
        type: Sequelize.UUID,
        allowNull: false,
        refenrences: {
          model: 'StructureHasPersonnel',
          key: 'Structure_has_personnel_id',
          as: 'created_by',
        },
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('CatalogDetail')
  },
}
