'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('PromotionAudit', {
      promotion_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      promotion_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Promotion",
          key: "promotion_id",
          as: "promotion_by"
        }
      },
      audited_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      audited_by: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: "StructionHasPersonnel",
          key: "structure_has_personnel_id",
          as: "audited_by",
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('PromotionAudit');
  }
};