'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ServiceAudit', {
      service_audit_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      service_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Service",
          key: "service_id",
          as: "service_id",
        }
      },
      audited_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "audited_by",
        }
      },
      audited_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ServiceAudit');
  }
};