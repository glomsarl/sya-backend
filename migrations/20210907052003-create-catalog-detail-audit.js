'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('CatalogDetailAudit', {
      catalog_detail_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      catalog_detail_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "CatalogDetail",
          key: "catalog_detail_id",
          as: "catalog_detail_id",
        }
      },
      is_reservable: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      is_take_away: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      is_home_delivery: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      is_payable: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      amount: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      quantity: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      cancellation_delay: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false
      },
      audited_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        allowNull: false,
      },
      audited_by: {
        type: Sequelize.UUID,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "audited_by"
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('CatalogDetailAudit');
  }
};