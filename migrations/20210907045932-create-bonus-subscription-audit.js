'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('BonusSubscriptionAudit', {
      bonus_subscription_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      quantity_cashed_out: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      quantity_cashed_in: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      quantity_remaining: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      bonus_subscription_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "BonusSubscription",
          key: "bonus_subscription_id",
          as: "bonus_subscription_id",
        }
      },
      audited_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "audited_by",
        }
      },
      audited_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        allowNull: false,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('BonusSubscriptionAudit');
  }
};