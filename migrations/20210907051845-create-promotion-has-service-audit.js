'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('PromotionHasServiceAudit', {
      promotion_has_services_audit_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      promotion_has_service_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'ReductioinHasService',
          key: 'promotion_has_service_id',
          as: 'promotion_has_service_id',
        },
      },
      consequent_price: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      is_deleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
      },
      audited_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      audited_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'StructureHasPersonnel',
          key: 'struction_has_personnel_id',
          as: 'audited_by',
        },
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('PromotionHasServiceAudit')
  },
}
