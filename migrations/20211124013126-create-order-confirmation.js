'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('OrderConfirmation', {
      order_confirmation_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      order_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Order',
          key: 'order_id',
          as: 'order_id',
        },
      },
      status: {
        type: Sequelize.ENUM('CONFIRMED', 'UNCONFIRMED', 'CANCELLED', 'SERVED'),
        allowNull: false,
        defaultValue: 'UNCONFIRMED',
      },
      comfirmed_at: {
        type: Sequelize.DATE,
      },
      cancelled_at: {
        type: Sequelize.DATE,
      },
      served_at: {
        type: Sequelize.DATE,
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('OrderConfirmation')
  },
}
