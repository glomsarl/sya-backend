'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('FeedbackAudit', {
      feedback_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      feedback_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Feedback",
          key: "feedback_id",
          as: "feedback_id",
        }
      },
      audited_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        allowNull: false,
      },
      audited_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "PersonHasRole",
          key: "person_role_id",
          as: "audited_by"
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('FeedbackAudit');
  }
};