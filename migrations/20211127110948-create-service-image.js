'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ServiceImage', {
      service_image_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      service_id: {
        type: Sequelize.UUID,
        allowNull: false, 
        references: {
          model: "Service",
          key: "service_id",
          as: "service_id"
        }
      },
      service_image_ref: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      added_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      added_by: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "added_by"
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ServiceImage');
  }
};