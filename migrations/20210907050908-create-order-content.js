'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('OrderContent', {
      order_content_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      service_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Service",
          key: "service_id",
          as: "service_id",
        }
      },
      order_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Order",
          key: "order_id",
          as: "order_id",
        }
      },
      quantity: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      unit_price: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      ordered_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      ordered_by: {
        allowNull: false,
        type: Sequelize.DATE,
        references: {
          model: "PersonHasRole",
          key: "person_has_role_id",
          as: "ordered_by"
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('OrderContent');
  }
};