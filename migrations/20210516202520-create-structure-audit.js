'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("StructureAudit", {
      structure_audit_id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      logo_ref: {
        type: Sequelize.STRING,
      },
      description: {
        type: Sequelize.STRING,
      },
      longitude: {
        type: Sequelize.REAL,
      },
      latitude: {
        type: Sequelize.REAL,
      },
      specific_indications: {
        type: Sequelize.STRING,
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      structure_id: {
        type: Sequelize.UUID,
        onDelete: "CASCADE",
        allowNull: false,
        references: {
          model: "Structure",
          key: "structure_id",
          as: "structure_id",
        },
      },
      audited_by: {
        type: Sequelize.UUID,
        onDelete: "CASCADE",
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnnel_id",
          as: "audited_by",
        },
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('StructureAudit');
  }
};