'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('SubscriptionAudit', {
      subscription_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      subscription_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Subscription',
          key: 'subscription_id',
          as: 'subscription_id',
        },
      },
      subscription_type: {
        type: Sequelize.ENUM('MONTHLY', 'TERMLY', 'YEARLY'),
        defaultValue: 'MONTHLY',
        allowNull: false,
      },
      commission_rate: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      audited_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      audited_by: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'PersonHasRole',
          key: 'person_has_role_id',
          as: 'audited_by',
        },
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('SubscriptionAudit')
  },
}
