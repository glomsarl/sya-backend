'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Subscription', {
      subscription_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      subscription_type: {
        type: Sequelize.ENUM('MONTHLY', 'TERMLY', 'YEARLY'),
        defaultValue: 'MONTHLY',
        allowNull: false,
      },
      commission_rate: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'PersonHasRole',
          key: 'person_has_role_id',
          as: 'created_by',
        },
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Subscription')
  },
}
