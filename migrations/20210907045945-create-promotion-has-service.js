'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('PromotionHasService', {
      promotion_has_service_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      promotion_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Promotion",
          key: "promotion_id",
          as: "promotion_id",
        }
      },
      service_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Service",
          key: "service_id",
          as: "service_id",
        }
      },
      consequent_price: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      created_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "created_by",
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('PromotionHasService');
  }
};