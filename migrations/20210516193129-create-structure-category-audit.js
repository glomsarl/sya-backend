'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("StructureCategoryAudit", {
      structure_category_audit_id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      category_name: {
        type: Sequelize.STRING,
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      structure_category_id: {
        type: Sequelize.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "StructureCategory",
          key: "structure_category_id",
          as: "structure_category_id",
        },
      },
      audited_by: {
        type: Sequelize.UUID,
        onDelete: "CASCADE",
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "audited_by",
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('StructureCategoryAudit');
  }
};