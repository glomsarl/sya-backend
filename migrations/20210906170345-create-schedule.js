'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Schedule', {
      schedule_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      day: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      onpen_at: {
        type: Sequelize.TIME,
        allowNull: false,
      },
      close_at: {
        type: Sequelize.TIME,
        allowNull: false,
      },
      date: {
        type: Sequelize.DATE
      },
      structure_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Struction",
          key: "structure_id",
          as: "structure_id"
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "created_by"
        }
      },
      is_deleted: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Schedule');
  }
};