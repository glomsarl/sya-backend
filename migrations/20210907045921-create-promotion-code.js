'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('PromotionCode', {
      promotion_code_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      structure_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Structure',
          key: 'structure_id',
          as: 'structure_id',
        },
      },
      person_has_role_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'PersonHasRole',
          key: 'person_has_role_id',
          as: 'person_has_role_id',
        },
      },
      code_value: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      is_used: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      percentage: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      created_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'StructureHasPersonnel',
          key: 'structure_has_personnel_id',
          as: 'created_by',
        },
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('PromotionCode')
  },
}
