'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Payment', {
      payment_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      order_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Order",
          key: "order_id",
          as: "order_id",
        }
      },
      is_paid: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      payment_method: {
        type: Sequelize.ENUM('CASH', 'SY'),
        allowNull: false,
      },
      payment_service: {
        type: Sequelize.ENUM('OM', 'MoMo', 'SYA'),
        allowNull: false,
      },
      initial_due: {
        type: Sequelize.FLOAT,
        defaultValue: 0,
        allowNull: false,
      },
      amount_paid: {
        type: Sequelize.FLOAT,
        defaultValue: 0,
        allowNull: false,
      },
      promotion_code_id: {
        type: Sequelize.UUID,
        references: {
          model: "PromotionCode",
          key: "promotion_code_id",
          as: "promotion_code_id",
        }
      },
      paid_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        allowNull: false,
      },
      paid_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Person",
          key: "person_id",
          as: "paid_by"
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Payment');
  }
};