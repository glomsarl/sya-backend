'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ScheduleAudit', {
      schedule_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      audited_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "audited_by",
        }
      },
      schedule_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: "Schedule",
          key: "schedule_id",
          as: "schedule_id",
        }
      },
      audited_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      open_at: {
        type: Sequelize.TIME
      },
      close_at: {
        type: Sequelize.TIME
      },
      date: {
        type: Sequelize.DATE
      },
      is_deleted: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ScheduleAudit');
  }
};