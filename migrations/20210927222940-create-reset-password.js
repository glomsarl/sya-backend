'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ResetPassword', {
      reset_password_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4, 
      },
      link: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      is_used: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      expires_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      person_id: {
        type: Sequelize.UUID,
        references: {
          model: "Person",
          key: "person_id",
          as: "person_id"
        }
      },
      onwer_id: {
        type: Sequelize.UUID,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "onwer_id"
        }
      },
      admin_id: {
        type: Sequelize.UUID,
        references: {
          model: "Admin",
          key: "admin_id",
          as: "admin_id"
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ResetPassword');
  }
};