'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('OrderAudit', {
      order_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      audited_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      audited_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "PersonHasRole",
          key: "person_has_role_id",
          as: "audited_by"
        }
      },
      order_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Order",
          key: "order_id",
          as: "order_id"
        }
      },
      order_type: {
        type: Sequelize.ENUM('R', 'T', 'H'),
        allowNull: false,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('OrderAudit');
  }
};