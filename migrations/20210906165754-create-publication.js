'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Publication', {
      publication_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      publication_ref: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      structure_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: "Structure",
          key: "structure_id",
          as: "structure_id",
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      created_by: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "created_by"
        }
      },
      is_deleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      deleted_at: {
        type: DataTypes.DATE,
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Publication');
  }
};