'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('BonusSettingAudit', {
      bonus_setting_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      sy_value: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      bonus_bank: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      sign_up_bonus: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      structure_validation_bonus: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      is_sign_up_bonus_on: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      audited_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      audited_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "PersonHasRole",
          key: "person_has_role_id",
          as: "audited_by"
        }
      },
      bonus_bank: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      bonus_setting_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "BonusSetting",
          key: "bonus_setting_id",
          as: "bonus_setting_id"
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('BonusSettingAudit');
  }
};