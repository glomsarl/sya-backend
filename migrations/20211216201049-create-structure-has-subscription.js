'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('StructureHasSubscription', {
      structure_has_subscription_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      structure_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Structure',
          key: 'structure_id',
          as: 'structure_id',
        },
      },
      subscription_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Subscription',
          key: 'subscription_id',
          as: 'subscription_id',
        },
      },
      subscribed_at: {
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE,
        allowNull: false,
      },
      expired_at: {
        type: DataTypes.DATE,
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('StructureHasSubscription')
  },
}
