'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Demand", {
      demand_id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      validated_at: {
        type: Sequelize.DATE,
      },
      is_valid: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      rejected_at: {
        type: Sequelize.DATE,
      },
      rejection_reason: {
        type: Sequelize.STRING,
      },
      requested_by: {
        type: Sequelize.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "PersonHasRole",
          key: "person_has_role_id",
          as: "requested_by",
        }
      },
      validated_by: {
        type: Sequelize.UUID,
        onDelete: "CASCADE",
        references: {
          model: "Admin",
          key: "admin_id",
          as: "validated_by",
        }
      },
      structure_id: {
        type: Sequelize.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "Structure",
          key: "structure_id",
          as: "structure_id",
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
      is_viewed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      viewed_at: {
        type: Sequelize.DATE,
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Demand');
  }
};