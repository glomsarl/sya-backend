'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Promotion', {
      promotion_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      structure_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Structure",
          key: "structure_id",
          as: "structure_id",
        }
      },
      starts_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      ends_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      is_expired: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      promotion_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      promotion_type: {
        type: Sequelize.ENUM("Bonus", "Reduction"),
        allowNull: false,
      },
      value: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      purchasing_bonus: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      is_deleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          models: "PersonHasRole",
          key: "person_has_role_id",
          as: "created_by",
        }
      },
      created_at: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.NOW,
        allowNull: false,
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Promotion');
  }
};