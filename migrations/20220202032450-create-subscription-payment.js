'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('SubscriptionPayment', {
      subscription_payment_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      structure_subscription_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "StructureSubscription",
          key: "structure_subscription_id",
          as: "structure_subscription_id"
        } 
      },
      comission: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      commission_rate: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      paid_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW,
      },
      is_paid: {
        type: Sequelize.UUID,
        allowNull: false
      },
      reference: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      payment_method: {
        type: Sequelize.ENUM('CASH', 'MoMo', 'OM'),
        allowNull: false,
      },
      total_period_revenue: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('SubscriptionPayment');
  }
};