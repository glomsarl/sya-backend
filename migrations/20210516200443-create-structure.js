'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Structure", {
      structure_id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      logo_ref: {
        type: Sequelize.STRING,
      },
      description: {
        type: Sequelize.STRING,
      },
      longitude: {
        type: Sequelize.REAL,
        allowNull: false,
      },
      latitude: {
        type: Sequelize.REAL,
        allowNull: false,
      },
      specific_indication: {
        type: Sequelize.STRING,
      },
      number_of_points: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      number_of_votes: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      is_valid: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      is_disabled: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: "Person",
          key: "person_id",
          as: "created_by",
        }
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Structure');
  }
};