'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('CatalogAudit', {
      catalog_audit_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      ends_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      starts_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      audited_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        allowNull: false,
      },
      catalog_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "Catalog",
          key: "catalog_id",
          as: "catalog_id"
        }
      },
      audited_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "StructureHasPersonnel",
          key: "structure_has_personnel_id",
          as: "audited_by"
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('CatalogAudit');
  }
};