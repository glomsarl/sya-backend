'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('BonusSetting', {
      bonus_setting_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      sy_value: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      bonus_bank: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      sign_up_bonus: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      referral_bonus: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        allowNull: false
      },
      structure_validation_bonus: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
      is_sign_up_bonus_on: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        allowNull: false,
      },
      created_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "PersonHasRole",
          key: "person_has_role_id",
          as: "created_by"
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('BonusSetting');
  }
};