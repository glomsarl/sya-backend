const fs = require("fs");
require("dotenv").config()
module.exports = {
  development : {
    username: process.env.USER_NAME,
    password: process.env.PASSWORD,
    database: process.env.DB_DEV_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect:"mysql"
  },
  test: {
    username: process.env.USER_NAME,
    password: process.env.PASSWORD,
    database: process.env.DB_TEST_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "mysql"
  },
  production: {
    username: process.env.USER_NAME,
    password: process.env.PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "mysql"
  },
}
