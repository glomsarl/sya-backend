const shell = require('shelljs')

// shell.exec("npx sequelize-cli model:generate --name BonusSettingAudit --attributes sy_value:integer,audited_at:date,audited_by:uuid,bonus_bank:integer,bonus_setting_id:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name Mail --attributes sent_by:uuid,sent_at:date --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name Promotion --attributes structure_id:uuid,start_at:date,ends_at:date,is_expired:boolean,promotion_name:string --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name BonusSubscription --attributes structure_id:uuid,quantity_cashed_out:integer,quantity_cashed_in:integer,quantity_remaining:integer,created_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name Publication --attributes structure_id:uuid,start_at:date,ends_at:date,is_expired:boolean,promotion_name:string --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name PromotionCode --attributes structure_id:uuid,person_id:uuid,code_value:date,is_used:boolean,amount:integer --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name BonusSubscriptionAudit --attributes quantity_cashed_out:integer,quantity_cashed_in:integer,quantity_remaining:integer,bonus_subscription_id:uuid,audited_by:uuid,audited_at:date --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name PromotionHasService --attributes promotion_id:uuid,service_id:uuid,consequent_price:integer,created_at:date,created_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name ServiceAudit --attributes service_id:uuid,audited_by:uuid,audited_at:date --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name Catalog --attributes structure_id:uuid,ends_at:date,starts_at:date,created_at:date,created_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name CatalogAudit --attributes ends_at:date,starts_at:date,audited_at:date,audited_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name BonusSubscriptionHasServiceAudit --attributes bonus_subscription_has_service_id:uuid,audited_at:date,audited_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name BonusSubscriptionHasService --attributes bonus_subscription_has_service_id:uuid,created_at:date,created_by:uuid --config sequelize-cli.json")

// shell.exec("npx sequelize-cli model:generate --name PromotionHasServiceAudit --attributes promotion_has_service_id:uuid,audited_at:date,audited_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name PromotionAudit --attributes promotion_id:uuid,audited_at:date,audited_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name FeedbackAudit --attributes feedback_id:uuid,audited_at:date,audited_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name Order --attributes ordered_at:date,ordered_by:uuid,order_type:enum:'{R,T,H}' --config sequelize-cli.json")

// shell.exec("npx sequelize-cli model:generate --name OrderBonus --attributes order_id:uuid,bonus_acquired:uuid,is_acquired:boolean,created_at:date,created_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name Service --attributes structure_id:uuid,name:string,description:boolean,unit_price:uuid,rating:integer,votes:integer,cancellation_delay:integer,created_at:date,created_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name ServiceImages --attributes service_id:uuid,service_image_ref:string,is_deleted:boolean,added_at:date,added_by:uuid")

// shell.exec("npx sequelize-cli model:generate --name Feedback --attributes order_content_id:uuid,feedback:string,is_visible:boolean,rating:integer,created_at:date,created_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name Payment --attributes order_id:uuid,is_paid:boolean,payment_method:enum:'{CASH,SY}',payment_service:enum:'{OM,MoMo,EUMO}',initial_due:integer,amount_paid:integer,promotion_code_id:uuid,paid_at:date,paid_by:uuid --config sequelize-cli.json")

// shell.exec("npx sequelize-cli model:generate --name OrderConfirmation --attributes order_id:uuid,status:enum:{CONFIRMED,UNCONFIRMED,CANCELLED,SERVED},comfirmed_at:date,cancelled_at:date,served_at:date")

// shell.exec("npx sequelize-cli model:generate --name CatalogDetail --attributes is_reservable:boolean,is_take_away:boolean,is_home_delivery:boolean,is_payable:boolean,amount:integer,quantity:integer,catalog_id:integer,promotion_has_service_id:uuid,service_id:uuid,bonus_subscription_has_service_id:uuid,cancellation_delay:integer,created_at:date,created_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name CatalogDetailAudit --attributes catalog_detail_id:uuid,is_reservable:boolean,is_take_away:boolean,is_home_delivery:boolean,is_payable:boolean,amount:integer,quantity:integer,catalog_id:integer,promotion_has_service_id:uuid,service_id:uuid,bonus_subscription_has_service_id:uuid,cancellation_delay:integer,audited_at:date,audited_by:uuid --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name OrderContent --attributes service_id:uuid,order_id:uuid,quantity:integer,unit_price:integer --config sequelize-cli.json --force")

// shell.exec("npx sequelize-cli model:generate --name Subscription --attributes subscription_type:enum:{monthly,termly,yearly},commission_rate:integer,created_at:date,created_by:uuid")
// shell.exec("npx sequelize-cli model:generate --name StructureHasSubscription --attributes structure_id:uuid,subscribed_at:date,subscription_id:uuid,subscription_amount:integer,commission_rate:integer")
// shell.exec("npx sequelize-cli model:generate --name SubscriptionAudit --attributes subscription_id:uuid,subscription_type:enum:{monthly,termly,yearly},commission_rate:integer,audited_at:date,audited_by:uuid,is_deleted:boolean")
// shell.exec("npx sequelize-cli model:generate --name SubscriptionPayment --attributes structure_subscription_id:uuid,comission:float,commission_rate:float,paid_at:date,is_paid:boolean,reference:string,paymen_method:enum:{CASH,MoMo,OM},total_period_revenue:float")
// shell.exec("npx sequelize-cli model:generate --name PersonBonus --attributes person_bonus_id:uuid,value:integer,person_id:uuid,structure_category_id:uuid,created_at:date")