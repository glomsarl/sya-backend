const router = require('express').Router()

module.exports = (app) => {
  const uploads = require('../utils/multer')
  const user = require('../controllers/user.controller')

  router.put('/log-out', user.userLogOut)
  router.post('/sign-in', user.signIn)

  router.post('/reset-password', user.sendResetPasswordLink)
  router.post('/new-password/:link/:email', user.setNewPassword)

  router.post(
    '/demand',
    uploads.single('logo_ref'),
    user.demandStructureCreation
  )

  router.get('/structure/all', user.getStructures)
  router.get('/structure/info', user.getStructureInfo)
  router.get('/structures/catalogs', user.findAllCatalogs)
  router.put('/structure/active', user.setActiveStructures)
  router.get('/owner-structures', user.findOnwerStructures)
  router.get('/structure/publications', user.fetchPublications)
  router.get('/structure/schedules', user.findStructureSchedules)
  router.get('/structure/categories', user.getStructureCategories)
  router.get('/structure/catalog-services', user.findCatalogServices)

  router.get('/profile/info', user.findUserProfile)
  router.put(
    '/profile/:person_id/edit',
    uploads.single('person_image_ref'),
    user.updateProfile
  )
  router.get('/feedbacks', user.getFeedbacks)

  router.get('/subscriptions', user.fetchSubscriptions)
  router.get('/subscription/active', user.getActiveSubscription)
  router.get('/subscriptions-tracker', user.getSubscriptionsTracker)

  router.get('/promotion-code', user.getCodeValuePercentage)

  router.get('/search/services', user.searchSercives)
  router.get('/search/structures', user.searchStructures)

  router.get('/statistic/sales', user.getSalesStatistics)
  router.get('/statistic/promotions', user.getBonusStatistics)

  router.get('/bonus-setting', user.getBonusSetting)

  app.use('/user', router)
}
