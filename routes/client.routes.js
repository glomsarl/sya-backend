const router = require('express').Router()

module.exports = (app) => {
  const middleware = require('../middlewares/index')
  const client = require('../controllers/client.controller')
  const { signIn } = require('../controllers/user.controller')

  router.post('/register', client.register)

  app.use(
    ['/client/order', '/client/purchase', '/client/sy-points'],
    middleware.clientAccess
  )

  router.post('/purchase', client.purchaseService)

  router.get('/sy-points', client.findPersonSy)

  router.get('/order/all', client.findAllUserOrders)
  router.post('/order/feedback', client.giveFeedback)
  router.put('/order/status', client.changeOrderStatus)

  router.post(
    '/sign-up/purchase',
    (req, res, next) => {
      client.register(req, res, next)
    },
    client.purchaseService
  )

  router.post(
    '/sign-in/purchase',
    (req, res, next) => {
      signIn(req, res, next)
    },
    client.purchaseService
  )

  app.use('/client', router)
}
