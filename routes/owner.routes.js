const router = require("express").Router();

module.exports = (app) => {
  const uploads = require("../utils/multer");
  const middleware = require("../middlewares/index");
  const owner = require("../controllers/owner.controller");

  app.use(
    [
      "/owner/subscription",
      "/owner/publication",
      "/owner/promotion",
      "/owner/statistic",
      "/owner/schedule",
      "/owner/profile",
      "/owner/catalog",
      "/owner/service",
      "/owner/order",
    ],
    middleware.ownerAccess
  );

  router.put(
    "/profile/:structure_id/edit",
    uploads.single("logo_ref"),
    owner.updateStructure
  );

  router.post("/promotion/new", owner.createPromotion);
  router.get("/promotion/all", owner.findAllPromotions);
  router.put("/promotion/:promotion_id/edit", owner.updatePromotion);
  router.put("/promotion/:promotion_id/delete", owner.deletePromotion);
  router.get("/promotion/all-services", owner.findPromotionServices);
  router.post("/promotion/add-services", owner.addServicesToPromotion);
  router.put(
    "/promotion/:promotion_id/:service_id/remove",
    owner.removeServiceFromPromotion
  );

  router.post("/catalog/new", owner.createCatalog);
  router.put("/catalog/:catalog_id/edit", owner.editCatalog);
  router.put("/catalog/:catalog_id/delete", owner.deleteCatalog);
  router.post("/catalog/add-services", owner.addServicesToCatalog);
  router.post("/catalog/remove-services", owner.removeServicesFromCatalog);

  router.post(
    "/service/new",
    uploads.single("main_image_ref"),
    owner.createService
  );
  router.put(
    "/service/:service_id/edit",
    uploads.single("main_image_ref"),
    owner.editService
  );

  router.put(
    "/service/:service_id/images",
    uploads.array("service_images"),
    owner.addServiceImages
  );
  router.get("/service/all", owner.findAllServices);
  router.get("/service/:service_id/info", owner.findService);
  router.put("/service/:service_id/delete", owner.deleteService);
  router.get("/service/:service_id/images", owner.getServiceImages);

  router.post("/schedule/new", owner.addStructureSchedule);
  router.put("/schedule/:schedule_id/edit", owner.updateStructureSchedule);
  router.put("/schedule/:schedule_id/delete", owner.deleteStructureSchedule);

  router.post(
    "/publication/new",
    uploads.array("publications"),
    owner.createPublications
  );
  router.put("/publication/:publication_id/delete", owner.deletePublication);

  router.get("/order/all", owner.getAllServiceOrders);
  router.put("/order/:order_id/served", owner.confirmServiceOrder);

  router.post("/subscription/new", owner.structureSubscription);

  router.get("/statistic/orders", owner.getOrderStatistics);
  router.get("/statistic/overview", owner.getOwnerOverviews);
  router.get("/statistic/finances", owner.getFinanceStatistics);
  router.get("/statistic/service-orders", owner.getServicesOrderStatistics);

  app.use("/owner", router);
};
