const router = require('express').Router()

module.exports = (app) => {
  const admin = require('../controllers/admin.controller')
  const middleware = require('../middlewares/index')

  app.use(
    [
      '/admin/bonus-setting',
      '/admin/subscription',
      '/admin/structure',
      '/admin/demand',
      '/admin/mail',
    ],
    middleware.adminAccess
  )
  router.post('/mail/sent', admin.sendEmails)
  router.get('/mail/structures', admin.structuresMinorFetch)

  router.put('/demand/view', admin.viewDemands)
  router.get('/demand/all', admin.fetchAllDemands)
  router.put('/demand/delete', admin.deletedDemands)
  router.put('/demand/:demand_id/validate', admin.validateDemand)

  router.get('/structure/all', admin.fetchAllStructures)
  router.put('/structure/ability', admin.setStructureAbility)

  router.post('/bonus-setting/new', admin.writeBonusSetting)
  router.put('/bonus-setting/edit', admin.updateBonusSetting)

  router.post('/subscriprion/new', admin.createSubscription)
  router.put('/subscription/:subscription_id/edit', admin.updateSubscription)
  router.put('/subscription/:subscription_id/delete', admin.deleteSubscription)

  router.get('/statistic/clients', admin.getClientStatistics)
  router.get('/statistic/overview', admin.getAdminOverviews)
  router.get('/statistic/subscriptions', admin.subscriptionStatistics)

  router.put('/feedback/:feedback_id/visible', admin.toggleFeedbackVisibility)

  app.use('/admin', router)
}
