module.exports = app => {
    //user routes
    require('./admin.routes')(app)
    require('./owner.routes')(app)
    require('./client.routes')(app)
    require('./user.routes')(app)
}